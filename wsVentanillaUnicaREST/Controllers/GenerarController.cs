﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using wsVentanillaUnicaREST.Models;
using wsVentanillaUnicaREST.Util;
using libGenerarPDF.clases;
using System.IO;
using libGenerarPDF;
using System.Web;
using wsVentanillaUnicaREST.dao;

namespace wsVentanillaUnicaREST.Controllers{

    [RoutePrefix("generar")]
    public class GenerarController : ApiController{
        //---------------------------------------------------------------------
        [HttpPost]    
        [ActionName("hojaatencion")]
        public IHttpActionResult ObtenerHojaAtencion([FromBody]JObject _PostParams) {

            bool  lObtenerFichaPago = false;
            bool  bUrlEncode = true;
            bool  lExisteErrorPDF = false;
            bool  bAppKeyHabilitada = false;
            int   iCountTramites = 0;
            int   iTotalCerosImporte = 9;
            int    _iIdBitacora = 0;
            string sParamReferencia = string.Empty;
            string sParamAppKey = string.Empty;         
            string sLinealog   = string.Empty;
            string sServerPath = string.Empty;
            string sFileNameConfig = string.Empty;
            string sErrorMetodo = string.Empty;
            string sRutaTempPDF = string.Empty;
            string sFileNameBarCode = string.Empty;
            string sFileNameBarCode30 = string.Empty;
            string sRefPrincipal30 = string.Empty;
            string sBase64PDF = string.Empty;
            string KeyConfig = string.Empty;
            string sParamsRef = string.Empty;
            JObject ParamsRef = new JObject();
            string sBase64HojaPDF = string.Empty;
            string sIdProceso = string.Empty;
            string sFileNameConfigMsg = string.Empty;
            string sFileNamePDFBase64 = string.Empty;

            string sUserAgentHttp = string.Empty;
            string sIpAddress = "0.0.0.0";

            StringBuilder sStringBuildAux = null;
            HttpContextWrapper oHttpContextW = null;

            DTOBitacora  oBitacoraHoja = null;
            DaoConsultas oDaoConsulta = null;
            TJsonRespuesta oJsonResp = null;
            TPostResponse  oPRespAppKey = null;
            TPostResponse oMsgError = null;
            Dictionary<string, string> oDictConfig = null;

            //Objetos para interaccion con el Reporte
            TDataReporteVU oDataReportVU = null;           
            TDataTramite oDataTramite = null;
            TDataConceptoRef oNewDataConcepto = null;
            TConfiguracion oConfigParams = null;
            DTOConfiguracion oConfig = null;
            List<TDataConceptoRef> listConceptosVU = null;
            List<TDataConceptoRef> oListGruposTramites = null;
            List<TDataAtributos> listAtributos = null;           
            GenerarPDF oCrearPDF = null;

            JArray aJListDetalles = null;
            JArray aJListConceptos = null;
            JArray aJListAtributos = null;

            JObject oJsonDocPDF = null;
            List<JObject> listDocsPDF = null;

            HttpResponseMessage oHttpMsgError = null;

            JObject oResponsePost = new JObject();
            
            oResponsePost["iError"] = 0;
            oResponsePost["cError"] = "";
            oResponsePost["aDocumentos"] = JToken.FromObject(new List<JObject>());

            if (_PostParams == null) {
                //TODO : Complementar con mensajes para ser explicito el error             
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_EMPTY);
                throw new HttpResponseException(oHttpMsgError);
            }//fin:iif

            if (_PostParams["cLineaReferencia"] == null || _PostParams["cAppKey"] == null) {
                //TODO : Complementar con mensajes para ser explicito el error
                //throw new HttpResponseException(HttpStatusCode.BadRequest);
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_NO_REQUERIDOS);
                throw new HttpResponseException(oHttpMsgError);
            }//fin:iif

            sParamReferencia = _PostParams.Value<string>("cLineaReferencia").Trim();
            sParamAppKey     = HttpUtility.UrlDecode( _PostParams.Value<string>("cAppKey").Trim());

            if (_PostParams["lEsFichaPago"] != null) {
                lObtenerFichaPago = Convert.ToBoolean(_PostParams.Value<string>("lEsFichaPago").Trim().ToLower());
            }//fin:else
            else { lObtenerFichaPago = false; }

            //DAP 20200820, parámetro para identificar si el PDF Base64 se aplica el UrlEncode            
            bUrlEncode = true;
            if(_PostParams["lUrlEncode"] != null) {
                if (!string.IsNullOrEmpty(_PostParams.Value<string>("lUrlEncode").Trim())) {
                    bUrlEncode = Convert.ToBoolean(_PostParams.Value<string>("lUrlEncode").Trim().ToLower());
                }//fin:if                
            }//fin:else
            else { bUrlEncode = true; }

            sIdProceso = "[" + DateTime.Now.ToString("yyyyMMdd.HHmmss.fff") + "]";

            try {
                oConfigParams = new TConfiguracion();
                sFileNameConfigMsg = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");

                //Obtener la direccion ipcliente
                if (Request.Properties.ContainsKey("MS_HttpContext")) {
                    oHttpContextW = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                    if (oHttpContextW != null) {
                        sIpAddress = oHttpContextW.Request.UserHostAddress;
                    }//fin:if
                }//fin:if

                // Get user agent.
                if (Request.Headers.Contains("User-Agent")) {
                    var headers = Request.Headers.GetValues("User-Agent");
                    sStringBuildAux = new StringBuilder();
                    foreach (var header in headers) {
                        sStringBuildAux.Append(header);
                        sStringBuildAux.Append(" ");
                    }//fin:foreach
                    sUserAgentHttp = sStringBuildAux.ToString().Trim();
                    sStringBuildAux = null;
                }//fin:if
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //Validar si AppKey tiene permisos de ejecucion sobre el servicio
                bAppKeyHabilitada = FuncionesWeb.comprobarAppKeyHabilitada(sParamAppKey, ref sErrorMetodo);
                if (!string.IsNullOrEmpty(sErrorMetodo)) { 
                    Utilerias.guardarLogEventos(sIdProceso + "[ObtenerHojaAtencion]" + sErrorMetodo);
                }//fin:if

                if (bAppKeyHabilitada == false) {

                    //Error, falta de permiso de AppKey
                    sLinealog = sIdProceso + sErrorMetodo;
                    Utilerias.guardarLogEventos(sLinealog);

                    oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_09);
                    oResponsePost["iError"] = oMsgError.iError;
                    oResponsePost["cError"] = oMsgError.cError;
                    oMsgError = null;

                    return Ok(oResponsePost);
                }//fin:if (bAppKeyHabilitada == false)
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //Validar que la linea de Referencia solo contenga caracteres numericos
                if (Regex.IsMatch(sParamReferencia, "^[0-9]*$")) {

                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    //Obtener variables de configuracion
                    oDaoConsulta = new DaoConsultas();

                    KeyConfig = Constantes.VUE_JSON_PAGO_OXXO;

                    oConfig = oDaoConsulta.ObtenerValorConfig(KeyConfig);

                    sParamsRef = oConfig.cValor;

                    ParamsRef = JObject.Parse(sParamsRef);

                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    //Guardar registro de bitacora 
                    oBitacoraHoja = new DTOBitacora();
                    oBitacoraHoja.sFolio = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                    oBitacoraHoja.sEvento = "POST^" + Request.RequestUri.ToString(); ;
                    oBitacoraHoja.sObservacion =
                        "[INFO]{GENERAR_HOJA_ATENCION}Inicio de petición para generar la hoja de atención.";
                    oBitacoraHoja.sHttpUserAgent = sUserAgentHttp;
                    oBitacoraHoja.sIPCliente = sIpAddress;
                    oBitacoraHoja.sInputParams = _PostParams.ToString(Formatting.None);
                    _iIdBitacora = oDaoConsulta.GuardarBitacora(oBitacoraHoja, new List<DTODetalleBitacora>());
                    oBitacoraHoja = null;
                    oDaoConsulta = null;
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    //Validar si el AppKey enviado en válido
                    oPRespAppKey = FuncionesWeb.validarAppKeyAAFY(sParamAppKey);
                    if (oPRespAppKey.lError == false) {
                       
                        //Obtener la configuracion global del reporte a mostrar
                        sFileNameConfig = System.Web.Hosting.HostingEnvironment.MapPath("~/config/config_reportes.json");
                        sServerPath     = System.Web.Hosting.HostingEnvironment.MapPath("~");

                        sErrorMetodo = string.Empty;
                        oDictConfig = Utilerias.obtenerConfigReporte(sFileNameConfig, string.Empty, ref sErrorMetodo);




                        if (string.IsNullOrEmpty(sErrorMetodo)) {

                            sRutaTempPDF = Path.Combine(sServerPath, oDictConfig["ruta_filesystem_pdf"]);
                            iTotalCerosImporte = Convert.ToInt32(oDictConfig["total_ceros_importe"]);
                            
                            oConfigParams.sRutaImagenConvenios = "file:///" + Path.Combine(sServerPath, oDictConfig["ruta_imagen_convenios"]);
                            oConfigParams.sMensajeC1  = oDictConfig["reporte_msg1"];
                            oConfigParams.sMensajeC2  = oDictConfig["reporte_msg2"];
                            oConfigParams.bGuardarPDF = Convert.ToBoolean(oDictConfig["guardar_pdf"]);
                            oConfigParams.sFileSystemPDF   = sRutaTempPDF;
                            oConfigParams.sMascaraLineaRef = oDictConfig["mascara_linearef"];

                            //Invocar servicio para obtener los datos de la linea de Referencia
                            oJsonResp = FuncionesWeb.obtenerDetalleReferenciaWS(
                                sParamReferencia, sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty)
                            );
                            if (oJsonResp.lError) {
                                sLinealog = sIdProceso + "[" + sParamReferencia + "]";
                                sLinealog += oJsonResp.iError;
                                sLinealog += "|" + oJsonResp.cError; 
                                sLinealog += "|" + oJsonResp.cMensaje; 
                                Utilerias.guardarLogEventos(sLinealog);

                                oResponsePost["iError"] = 4;
                                oResponsePost["cError"] = "["+ Convert.ToString(oJsonResp.iError)  + "]" + oJsonResp.cError;

                                FuncionesWeb.enviarEmailNotificacion(
                                    "ERROR al invocar el servicio para obtener las hojas de atención[/generar/hojaatencion].",
                                    "ERROR al invocar el servicio para obtener los detalles de la línea de referencia.",
                                    sLinealog
                                );
                                //FIN:RESPUESTA ERROR EN SERVICIO
                            }//fin: if (oJsonResp.lError)
                            else {                                
                                //Obtener datos del encabezado del Recibo                            
                                oDataReportVU = new TDataReporteVU();
                                oDataReportVU.cFolioSolicitud = oJsonResp.oJsonData.Value<string>("cFolioSolicitud");
                                oDataReportVU.dtFechaSolicitud = oJsonResp.oJsonData.Value<string>("dtFechaSolicitud");
                                oDataReportVU.cRefPrincipal = oJsonResp.oJsonData.Value<string>("cLineaRef");
                                oDataReportVU.dTotal = oJsonResp.oJsonData.Value<double>("dMontoTotal");

                                //Validación de linea de referencia de 30 digitos
                                if (oDataReportVU.dTotal <= Convert.ToDouble(ParamsRef["monto_maximo"]))
                                {
                                    sRefPrincipal30 = ParamsRef["identificador"] + oDataReportVU.cRefPrincipal;
                                    if(sRefPrincipal30.Length == 29)
                                    {
                                        oDataReportVU.cRefPrincipal30 = FuncionesWeb.convertBase10(sRefPrincipal30);
                                        oDataReportVU.cConceptoGlobal = Convert.ToString(ParamsRef["concepto_global"]);
                                    }
                                    else
                                    {
                                        sRefPrincipal30 = string.Empty;
                                    }
                                }

                                oDataReportVU.cAviso = oDictConfig["reporte_aviso"];
                                oDataReportVU.cImageAAFY = "file:///" + Path.Combine(sServerPath, oDictConfig["ruta_imagen_aafy"]);
                                oDataReportVU.cImageYUC  = "file:///" + Path.Combine(sServerPath, oDictConfig["ruta_imagen_yuc"]);
                                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                //Seccion para decidir que tipo de Documento se debera generar
                                if (lObtenerFichaPago) {
                                    oDataReportVU.cTituloReporte = oDictConfig["titulo_ficha_pago"];
                                    
                                    oDataReportVU.cRutaReporte = Path.Combine(sServerPath, oDictConfig["reporte_ficha_pago"]);
                                    oDataReportVU.cTotalLetras = oJsonResp.oJsonData.Value<string>("cTotalLetras");
                                    oDataReportVU.dtVigencia = oJsonResp.oJsonData.Value<string>("dtVigencia");

                                    //Agrupar los tramites en un solo concepto para la ficha de pago
                                    sErrorMetodo = string.Empty;
                                    oListGruposTramites = this.AgruparTramitesFichaPago(
                                        oJsonResp.oJsonData.SelectToken("aDetalles").ToString(),
                                        ref sErrorMetodo
                                    );
                                    aJListDetalles = JArray.Parse(oJsonResp.oJsonData.SelectToken("aDetalles").ToString());

                                    listAtributos = this.AgruparAtributos(
                                        aJListDetalles, ref sErrorMetodo
                                    );

                                    if (string.IsNullOrEmpty(sErrorMetodo)) {
                                        
                                        //Obtener el archivo del codigo de barras
                                        sErrorMetodo = string.Empty;
                                        sFileNameBarCode = Utilerias.generarImageBarcode(
                                            iTotalCerosImporte, oDataReportVU.dTotal, oDataReportVU.cRefPrincipal, sRutaTempPDF, ref sErrorMetodo
                                        );


                                        if (!string.IsNullOrEmpty(sRefPrincipal30))
                                        {
                                            sFileNameBarCode30 = Utilerias.generarImageBarcode(
                                                0, oDataReportVU.dTotal, oDataReportVU.cRefPrincipal30, sRutaTempPDF, ref sErrorMetodo
                                            );
                                        }

                                        if (string.IsNullOrEmpty(sErrorMetodo)) {

                                            oConfigParams.sRutaImagenBarCode = sFileNameBarCode;
                                            oConfigParams.sRutaImagenBarCode30 = sFileNameBarCode30;

                                            sLinealog = sIdProceso + "INICIO invocación DLL <crearFichaPagoPDF>";
                                            Utilerias.guardarLogEventos(sLinealog);

                                            sErrorMetodo = string.Empty;
                                            oCrearPDF = new GenerarPDF();
                                            sBase64PDF = oCrearPDF.crearFichaPagoPDF(
                                                oDataReportVU,
                                                listAtributos,
                                                oListGruposTramites,                                                                                                
                                                oConfigParams,
                                                ref sErrorMetodo
                                            );
                                            oCrearPDF = null;
                                            oListGruposTramites.Clear();
                                            sLinealog = sIdProceso + "INICIO invocación DLL <crearFichaPagoPDF>";
                                            Utilerias.guardarLogEventos(sLinealog);

                                            sFileNamePDFBase64 = oDataReportVU.cRefPrincipal + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.ffff") + ".pdf";

                                            //Comprobar si existe Error antes de asignar valores
                                            if (string.IsNullOrEmpty(sErrorMetodo)) {
                                                
                                                //OK
                                                oJsonDocPDF = new JObject();
                                                oJsonDocPDF["iOrden"] = 1;
                                                oJsonDocPDF["iTipo"]  = (int)Constantes.eTipoDocumentosPDF.FICHA_PAGO;
                                                oJsonDocPDF["cReferencia"]  = oDataReportVU.cRefPrincipal;
                                                oJsonDocPDF["cNombreArchivo"] = sFileNamePDFBase64;
                                                //oJsonDocPDF["cBase64"] = HttpUtility.UrlEncode(sBase64PDF, Encoding.UTF8);
                                                
                                                //DAPC 20200820
                                                sBase64HojaPDF = string.Empty;                                                
                                                if (bUrlEncode) { sBase64HojaPDF = HttpUtility.UrlEncode(sBase64PDF, Encoding.UTF8); }
                                                else { sBase64HojaPDF = sBase64PDF; }                                                
                                                oJsonDocPDF["cBase64"] = sBase64HojaPDF;

                                                listDocsPDF = new List<JObject>();
                                                listDocsPDF.Add(oJsonDocPDF);

                                                oResponsePost["iError"] = 0;
                                                oResponsePost["cError"] = string.Empty;
                                                oResponsePost["aDocumentos"] = JToken.FromObject(listDocsPDF);
                                                //FIN : OK Obtencion Tramite
                                            }//fin: if (string.IsNullOrEmpty(sErrorMetodo)) 
                                            else {
                                                //Ocurrio un error al generar el PDF
                                                sLinealog = sIdProceso + sErrorMetodo;
                                                Utilerias.guardarLogEventos(sLinealog);

                                                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_07);
                                                oResponsePost["iError"] = oMsgError.iError;
                                                oResponsePost["cError"] = oMsgError.cError;
                                                oMsgError = null;

                                                FuncionesWeb.enviarEmailNotificacion(
                                                    "ERROR al invocar el servicio para obtener las hojas de atención[/generar/hojaatencion].",
                                                    "ERROR al generar al archivo PDF.",
                                                    sLinealog
                                                );
                                            }//fin:else if (string.IsNullOrEmpty(sErrorMetodo)) 
                                            
                                            //Eliminar la imagen del barcode
                                            sErrorMetodo = string.Empty;
                                            Utilerias.eliminarArchivoFileSystem(sFileNameBarCode, ref sErrorMetodo);
                                            if (!string.IsNullOrEmpty(sFileNameBarCode30)) {Utilerias.eliminarArchivoFileSystem(sFileNameBarCode30, ref sErrorMetodo);}
                                            if (!string.IsNullOrEmpty(sErrorMetodo)){
                                                sLinealog = sIdProceso + sErrorMetodo;
                                                Utilerias.guardarLogEventos(sLinealog);
                                            }//fin:if
                                        }//fin:if (string.IsNullOrEmpty(sErrorMetodo))
                                        else {
                                            //Notificar que ocurrio un error al generarse el Barcode                                            
                                            sLinealog = sIdProceso + sErrorMetodo;
                                            Utilerias.guardarLogEventos(sLinealog);
                                
                                            oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_06);
                                            oResponsePost["iError"] = oMsgError.iError;
                                            oResponsePost["cError"] = oMsgError.cError;
                                            oMsgError = null;

                                            FuncionesWeb.enviarEmailNotificacion(
                                                "ERROR al invocar el servicio para obtener las hojas de atención[/generar/hojaatencion].",
                                                "ERROR al generar la imagen para BarCode",
                                                sLinealog
                                            );
                                        }//fin:else if (string.IsNullOrEmpty(sErrorMetodo))
                                    }//fin:if (string.IsNullOrEmpty(sErrorMetodo))
                                    else {
                                        //Guardar en el log 
                                        sLinealog = sIdProceso + sErrorMetodo;
                                        Utilerias.guardarLogEventos(sLinealog);
                            
                                        oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_05);
                                        oResponsePost["iError"] = oMsgError.iError;
                                        oResponsePost["cError"] = oMsgError.cError;
                                        oMsgError = null;
                                    }//fin:else if (string.IsNullOrEmpty(sErrorMetodo))
                                }//fin:if if (lObtenerFichaPago)
                                else {
                                    oDataReportVU.cTituloReporte = oDictConfig["titulo_reporte"];
                                    oDataReportVU.cRutaReporte = Path.Combine(sServerPath, oDictConfig["reporte_hojaatencion"]);
                                    
                                    //Lista para las hojas de atencion generadas
                                    listDocsPDF = new List<JObject>();
                                    lExisteErrorPDF = false;

                                    //Iterar los detalles para obtener los recibos correspondientes                    
                                    aJListDetalles = JArray.Parse(oJsonResp.oJsonData.SelectToken("aDetalles").ToString());
                                    foreach (JObject oJsonTramite in aJListDetalles.Children<JObject>()) {
                                        iCountTramites++;
                                        
                                        oDataTramite = new TDataTramite();
                                        oDataTramite.dTotal = oJsonTramite.Value<double>("dImporte");

                                        oDataTramite.iAnioFiscal   = oJsonTramite.Value<int>("iAnioFiscal");
                                        oDataTramite.iOrdenTramite = oJsonTramite.Value<int>("iOrden");
                                        oDataTramite.cFolioTramite = oJsonTramite.Value<string>("cIdSolTramite");
                                        oDataTramite.cLineaReferencia = oJsonTramite.Value<string>("cReferencia");
                                        oDataTramite.cNombreTramite = oJsonTramite.Value<string>("cNombreTramite");
                                        oDataTramite.dtVigencia = oJsonTramite.Value<string>("dtVigencia");

                                        //Asignar la fecha de vigencia del trámite
                                        oDataReportVU.dtVigencia = oDataTramite.dtVigencia;

                                        //Obtener los conceptos
                                        aJListConceptos = JArray.Parse(oJsonTramite.SelectToken("aConceptos").ToString());

                                        if (listConceptosVU == null) listConceptosVU = new List<TDataConceptoRef>();
                                        listConceptosVU.Clear();

                                        foreach (JObject oJsonConcepto in aJListConceptos.Children<JObject>()) {
                                            oNewDataConcepto = new TDataConceptoRef();
                                            oNewDataConcepto.Clear();
                                            oNewDataConcepto.iOrden = oJsonConcepto.Value<int>("iNumOrden");
                                            oNewDataConcepto.iCantidad = oJsonConcepto.Value<double>("dCantidad");
                                            oNewDataConcepto.cConcepto = oJsonConcepto.Value<string>("cDescripcion");
                                            oNewDataConcepto.dImporteUnitario = oJsonConcepto.Value<double>("dImporteUnitario");
                                            oNewDataConcepto.dtotal = oJsonConcepto.Value<double>("dTotal");
                                            listConceptosVU.Add(oNewDataConcepto);
                                        }//fin:foreach (JObject oJsonConcepto in aJListConceptos.Children<JObject>())                       

                                        //Listar los atributos
                                        if (listAtributos == null) listAtributos = new List<TDataAtributos>();
                                        listAtributos.Clear();
                                        aJListAtributos = JArray.Parse(oJsonTramite.SelectToken("aAtributos").ToString());
                                        foreach (JObject oJsonAtributo in aJListAtributos.Children<JObject>()) {
                                            listAtributos.Add(new TDataAtributos() {
                                                iOrden = oJsonAtributo.Value<int>("iOrden"),
                                                cAtributo = oJsonAtributo.Value<string>("cAtributo"),
                                                cValor = oJsonAtributo.Value<string>("cValor")
                                            });
                                        }//fin: foreach (JObject oJsonAtributo in aJListAtributos.Children<JObject>())

                                        //Invocar la generacion del hoja de atencion a traves de la DLL
                                        if (listConceptosVU.Count > 0 ) {
                                            
                                            //Obtener el archivo del codigo de barras
                                            sErrorMetodo = string.Empty;
                                            sFileNameBarCode = Utilerias.generarImageBarcode(
                                                iTotalCerosImporte, oDataReportVU.dTotal, oDataReportVU.cRefPrincipal, sRutaTempPDF, ref sErrorMetodo
                                            );

                                            if (!string.IsNullOrEmpty(sRefPrincipal30))
                                            {
                                                sFileNameBarCode30 = Utilerias.generarImageBarcode(
                                                    0, oDataReportVU.dTotal, oDataReportVU.cRefPrincipal30, sRutaTempPDF, ref sErrorMetodo
                                                );
                                            }

                                            if (string.IsNullOrEmpty(sErrorMetodo)) {
                                                
                                                //OK, solicitar la generacion del PDF
                                                oConfigParams.sRutaImagenBarCode = sFileNameBarCode;
                                                oConfigParams.sRutaImagenBarCode30 = sFileNameBarCode30;

                                                sLinealog = sIdProceso + "INICIO invocación DLL <crearPDFTramiteVU>" + listAtributos;
                                                Utilerias.guardarLogEventos(sLinealog);
                                                sErrorMetodo = string.Empty;
                                                oCrearPDF = new GenerarPDF();
                                                sBase64PDF = oCrearPDF.crearPDFTramiteVU(
                                                    oDataReportVU,
                                                    oDataTramite,
                                                    listAtributos,
                                                    listConceptosVU,
                                                    oConfigParams,
                                                    ref sErrorMetodo
                                                );
                                                oCrearPDF = null;
                                                listAtributos.Clear(); listConceptosVU.Clear();

                                                sLinealog = sIdProceso + "FIN invocación DLL <crearPDFTramiteVU>";
                                                Utilerias.guardarLogEventos(sLinealog);
                                                
                                                sFileNamePDFBase64 = oDataTramite.cLineaReferencia + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.ffff") + ".pdf";                                                
                                                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                                //Generar arreglo para usuario final
                                                if (string.IsNullOrEmpty(sErrorMetodo)) {
                                                    oJsonDocPDF = new JObject();
                                                    oJsonDocPDF["iOrden"] = iCountTramites;
                                                    oJsonDocPDF["iTipo"]  = (int)Constantes.eTipoDocumentosPDF.HOJA_ATENCION;
                                                    oJsonDocPDF["cReferencia"]    = oDataTramite.cLineaReferencia;
                                                    oJsonDocPDF["cNombreArchivo"] = sFileNamePDFBase64;
                                                    //oJsonDocPDF["cBase64"] = HttpUtility.UrlEncode(sBase64PDF, Encoding.UTF8);

                                                    //DAPC 20200820
                                                    sBase64HojaPDF = string.Empty;
                                                    if (bUrlEncode) { sBase64HojaPDF = HttpUtility.UrlEncode(sBase64PDF, Encoding.UTF8); }
                                                    else { sBase64HojaPDF = sBase64PDF; }
                                                    oJsonDocPDF["cBase64"] = sBase64HojaPDF;

                                                    listDocsPDF.Add(oJsonDocPDF);
                                                }//fin:if (string.IsNullOrEmpty(sErrorMetodo))
                                                else {                                                
                                                    //Enviar correo de error
                                                    lExisteErrorPDF = true;

                                                    sLinealog = sIdProceso + sErrorMetodo;
                                                    Utilerias.guardarLogEventos(sLinealog);

                                                    oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_07);
                                                    oResponsePost["iError"] = oMsgError.iError;
                                                    oResponsePost["cError"] = oMsgError.cError;
                                                    oMsgError = null;

                                                    FuncionesWeb.enviarEmailNotificacion(
                                                        "ERROR al invocar el servicio para obtener las hojas de atención[/generar/hojaatencion].",
                                                        "ERROR al generar al archivo PDF.",
                                                        sLinealog
                                                    );
                                                }//fin:else   

                                                //Eliminar la imagen del barcode
                                                sErrorMetodo = string.Empty;
                                                Utilerias.eliminarArchivoFileSystem(sFileNameBarCode, ref sErrorMetodo);
                                                if (!string.IsNullOrEmpty(sFileNameBarCode30)) { Utilerias.eliminarArchivoFileSystem(sFileNameBarCode30, ref sErrorMetodo); }
                                                if (!string.IsNullOrEmpty(sErrorMetodo)){
                                                    sLinealog = sIdProceso + sErrorMetodo;
                                                    Utilerias.guardarLogEventos(sLinealog);
                                                }//fin:if
                                            }//fin:if (string.IsNullOrEmpty(sErrorMetodo))
                                            else {
                                                //Notificar del Error
                                                sLinealog = sIdProceso + sErrorMetodo;
                                                Utilerias.guardarLogEventos(sLinealog);
                                
                                                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_06);
                                                oResponsePost["iError"] = oMsgError.iError;
                                                oResponsePost["cError"] = oMsgError.cError;
                                                oMsgError = null;

                                                lExisteErrorPDF = true;

                                                FuncionesWeb.enviarEmailNotificacion(
                                                    "ERROR al invocar el servicio para obtener las hojas de atención[/generar/hojaatencion].",
                                                    "ERROR al generar la imagen para BarCode.",
                                                    sLinealog
                                                );
                                            }//fin: if (string.IsNullOrEmpty(sErrorMetodo))
                                        }//fin:if (listConceptosVU.Count > 0 )
                                        else {
                                            //Notificar que no existen conceptos a pagar
                                            oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_08);
                                            oResponsePost["iError"] = oMsgError.iError;
                                            oResponsePost["cError"] = oMsgError.cError;

                                            sLinealog = sIdProceso + oMsgError.cError;
                                            Utilerias.guardarLogEventos(sLinealog);

                                            oMsgError = null;  

                                            lExisteErrorPDF = true;
                                        }//fin:if if (listConceptosVU.Count > 0 )

                                        if (lExisteErrorPDF) { break; }
                                    }//fin: foreach (JObject oJsonTramite)
                                    if(listDocsPDF.Count > 0){
                                        oResponsePost["aDocumentos"] = JToken.FromObject(listDocsPDF);
                                    }//fin:if
                                }//fin:else if (lObtenerFichaPago)
                            }//fin:else if (oJsonResp.lError)
                        }//fin:else
                        else {
                            //Notificar sobre error al intentar obtener los parametros de configuracion                          
                            sLinealog = sIdProceso + sErrorMetodo;
                            Utilerias.guardarLogEventos(sLinealog);

                            sFileNameConfig = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");
                
                            oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfig, Constantes.KEY_MSG_ERROR_03);
                            oResponsePost["iError"] = oMsgError.iError;
                            oResponsePost["cError"] = oMsgError.cError;
                            oMsgError = null;  
                            
                            FuncionesWeb.enviarEmailNotificacion(
                                "ERROR al invocar el servicio para obtener las hojas de atención[/generar/hojaatencion].",
                                "No fue posible obtener los paremtros de configuración del archivo [config_reportes.json].",
                                sLinealog
                            );
                        }//fin:else                        
                    }//if:if (oPRespAppKey.lError == false)
                    else {
                        //Notificar que el AppKey no tiene permiso para ejecutar el servicio
                        sLinealog = sIdProceso + "EL AppKey " + sParamAppKey ;
                        sLinealog += " no tiene permiso para consultar el servicio.";                        
                        Utilerias.guardarLogEventos(sLinealog);
                
                        oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_02);
                        oResponsePost["iError"] = oMsgError.iError;
                        oResponsePost["cError"] = oMsgError.cError;
                        oMsgError = null;                          
                    }//fin:else                    
                }//fin:if (Regex.IsMatch(sParamReferencia, "^[0-9]*$"))
                else {
                    //Notificar que la referencia no tiene un formato valido(Numeros)
                    oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_01);
                    oResponsePost["iError"] = oMsgError.iError;
                    oResponsePost["cError"] = oMsgError.cError;
                    oMsgError = null;                   
                }//fin:else             
            }//fin:try
            catch (Exception oEx) {
                sLinealog = sIdProceso + "Error en [ObtenerHojaAtencion]: ";
                sLinealog += "Message^" + oEx.Message;
                sLinealog += "~Source^" + oEx.Source;
                sLinealog += "~Target^" + oEx.TargetSite.ToString();
                sLinealog += "~StackTrace^" + oEx.StackTrace;
                Utilerias.guardarLogEventos(sLinealog);
             
                //Obtener la configuracion global del reporte a mostrar
                sFileNameConfig = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");
                
                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfig, Constantes.KEY_MSG_ERROR_99);
                oResponsePost["iError"] = oMsgError.iError;
                oResponsePost["cError"] = oMsgError.cError;
                oMsgError = null;
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //Guardar registro de bitacora 
                if (oDaoConsulta != null) oDaoConsulta = null;
                oDaoConsulta = new DaoConsultas();
                oBitacoraHoja = new DTOBitacora();
                oBitacoraHoja.sFolio  = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                oBitacoraHoja.sEvento = "EXCEPCION en [/generar/hojaatencion]";
                oBitacoraHoja.sObservacion =
                   "[ERROR]{GENERAR_HOJA_ATENCION} Ocurrió una excepción en la ejecución del servicio.";
                oBitacoraHoja.sHttpUserAgent = sUserAgentHttp;
                oBitacoraHoja.sIPCliente = sIpAddress;
                oBitacoraHoja.sInputParams = string.Empty;
                _iIdBitacora = oDaoConsulta.GuardarBitacora(oBitacoraHoja, new List<DTODetalleBitacora>());
                oBitacoraHoja = null;
                oDaoConsulta = null;
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                FuncionesWeb.enviarEmailNotificacion(
                    "ERROR al invocar el servicio para obtener las hojas de atención.",
                    "ERROR, ocurrió una excepción al ejecutar el servicio [/generar/hojaatencion].",
                    sLinealog
                );
            }//fin: catch

            return Ok(oResponsePost);
        }//fin:if
        //---------------------------------------------------------------------       
        [HttpPost]
        [ActionName("referenciagn")]
        public IHttpActionResult GenerarReferenciaVUGN([FromBody]JObject _ParamsJson) {

            int _iIdBitacora = 0;
            bool bAppKeyHabilitada = false;
            double dImporteTramite = 0.00;
            string sIdProceso = string.Empty;
            string sLinealog  = string.Empty;
            string sFileNameConfig = string.Empty;
            
            string sUserAgentHttp = string.Empty;
            string sIpAddress = "0.0.0.0";
            string sAttrValue   = string.Empty;
            string sErrorMetodo = string.Empty;

            StringBuilder sStringBuildAux = null;
            HttpContextWrapper oHttpContextW = null;

            TJsonRespuesta oJsonRespWS  = null;
            TInputGenerarLR oJsonInputs = null;
            TPostResponse oMsgError = null;
            DTOBitacora oBitGenerarLR = null;
            DaoConsultas oDaoconsulta = null;
            HttpResponseMessage oHttpMsgError = null;

            JObject oJsonResponse = new JObject();

            oJsonResponse["iError"] = 0;
            oJsonResponse["cError"] = "";

            if (_ParamsJson == null) {
                //TODO : Complementar con mensajes para ser explicito el error               
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_EMPTY);
                throw new HttpResponseException(oHttpMsgError);                
            }//fin:iif

            if (_ParamsJson["cAppKey"] == null || _ParamsJson["iIdTramite"] == null ||
                _ParamsJson["iIdDependencia"] == null || _ParamsJson["iIdSubDependencia"] == null ||
                _ParamsJson["cIdentificadorUnico"] == null || _ParamsJson["oRecibo"] == null) {
                //TODO : Complementar con mensajes para ser explicito el error
                //throw new HttpResponseException(HttpStatusCode.BadRequest);
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_NO_REQUERIDOS);
                throw new HttpResponseException(oHttpMsgError);
            }//fin:iif
          
            try {
                //Obtener la direccion ipcliente
                if (Request.Properties.ContainsKey("MS_HttpContext")) {
                    oHttpContextW = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                    if (oHttpContextW != null) {
                        sIpAddress = oHttpContextW.Request.UserHostAddress;
                    }//fin:if
                }//fin:if

                // Get user agent.
                if (Request.Headers.Contains("User-Agent")) {
                    var headers = Request.Headers.GetValues("User-Agent");
                    sStringBuildAux = new StringBuilder();
                    foreach (var header in headers) {
                        sStringBuildAux.Append(header);
                        sStringBuildAux.Append(" ");
                    }//fin:foreach
                    sUserAgentHttp = sStringBuildAux.ToString().Trim();
                    sStringBuildAux = null;
                }//fin:if

                sIdProceso = "[" + DateTime.Now.ToString("yyyyMMdd.HHmmss.fff") + "]";

                sFileNameConfig = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");

                //Validar la integridad de los datos                
                if (string.IsNullOrEmpty(_ParamsJson.Value<string>("cAppKey")) ||
                    string.IsNullOrEmpty(_ParamsJson.Value<string>("iIdTramite")) ||
                    string.IsNullOrEmpty(_ParamsJson.Value<string>("iIdDependencia")) ||
                    string.IsNullOrEmpty(_ParamsJson.Value<string>("iIdSubDependencia")) ||
                    string.IsNullOrEmpty(_ParamsJson.Value<string>("cIdentificadorUnico")) ||
                    _ParamsJson.Value<JObject>("oRecibo")["cNombreRazonSocial"] == null) {

                    oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfig, Constantes.KEY_MSG_ERROR_WSGENERARREF_02);
                    oJsonResponse["iError"] = oMsgError.iError;
                    oJsonResponse["cError"] = oMsgError.cError;
                    oMsgError = null;
                    // FIN APLICACION CON ERROR
                }//fin:if
                else {
                    //Evaluar el AppKey tiene permiso de ejecucion
                    bAppKeyHabilitada = FuncionesWeb.comprobarAppKeyHabilitada(
                        _ParamsJson.Value<string>("cAppKey"), ref sErrorMetodo
                    );

                    if (bAppKeyHabilitada) {
                        oJsonInputs = new TInputGenerarLR();
                        oJsonInputs.Clear();

                        //Obtener el valor de la vigencia de la Linea de Referencia
                        if (_ParamsJson["dtFechaVigencia"] != null) {
                            sAttrValue = _ParamsJson.Value<string>("dtFechaVigencia");
                            if (!string.IsNullOrEmpty(sAttrValue)) {
                                oJsonInputs.sFechaVigenciaLR = sAttrValue;
                            }//fin:if
                        }//fin:if

                        //Obtener el valor [dImporteTramite] para aquellos tramites que reciban importe
                        dImporteTramite = 0.00;
                        if (_ParamsJson["dImporteTramite"] != null) {
                            sAttrValue = _ParamsJson.Value<string>("dImporteTramite");
                            if (!string.IsNullOrEmpty(sAttrValue)) {
                                if(double.TryParse(sAttrValue, out dImporteTramite)){
                                    //Utilerias.guardarLogEventos("dImporteTramite=>" + sAttrValue);                                   
                                    oJsonInputs.sImporteTramite = (Math.Round(dImporteTramite, 2)).ToString();
                                }//fin:if
                            }//fin:if
                        }//fin: if (_ParamsJson["dImporteTramite"] != null)

                        oJsonInputs.sAppKey = _ParamsJson.Value<string>("cAppKey");
                        oJsonInputs.iIdTramite = _ParamsJson.Value<int>("iIdTramite");
                        oJsonInputs.iIdDependencia = _ParamsJson.Value<int>("iIdDependencia");
                        oJsonInputs.iIdSubDependencia = _ParamsJson.Value<int>("iIdSubDependencia");

                        JObject oJRecibo = _ParamsJson.Value<JObject>("oRecibo");
                        int iCodigoPostalRec = 0;

                        if (!string.IsNullOrEmpty(oJRecibo.Value<string>("iCodigoPostal"))) {
                            if (!Int32.TryParse(oJRecibo.Value<string>("iCodigoPostal").Trim(), out iCodigoPostalRec)) {
                                iCodigoPostalRec = 0;
                            }//fin:if
                        }//fin:if

                        oJsonInputs.sDatosRecibos = String.Format(Constantes.CONFIG_FORMAT_RECIBO,
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cRFC")) ? "" : oJRecibo.Value<string>("cRFC").Trim(), // 0
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cNombreRazonSocial")) ? "" : oJRecibo.Value<string>("cNombreRazonSocial").Trim(), // 1
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cDomicilio")) ? "" : oJRecibo.Value<string>("cDomicilio").Trim(), // 2
                            iCodigoPostalRec > 0 ? iCodigoPostalRec.ToString() : "", // 3
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cMunicipio")) ? "" : oJRecibo.Value<string>("cMunicipio").Trim(), // 4
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cLocalidad")) ? "" : oJRecibo.Value<string>("cLocalidad").Trim(), // 5
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cColonia")) ? "" : oJRecibo.Value<string>("cColonia").Trim(), // 6
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cEmail")) ? "" : oJRecibo.Value<string>("cEmail").Trim(), // 7
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cTelefono")) ? "" : oJRecibo.Value<string>("cTelefono").Trim()  //8
                        );

                        if (!string.IsNullOrEmpty(_ParamsJson.Value<string>("cIdentificadorUnico"))) {
                            oJsonInputs.sAtributos = string.Format(
                                "{0}^{1}", Constantes.ATRIBUTO_UNICO_ETIQ,
                                _ParamsJson.Value<string>("cIdentificadorUnico")
                            );
                        }//fin:                    
                         //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                         //Guardar registro de bitacora 
                        oDaoconsulta = new DaoConsultas();
                        oBitGenerarLR = new DTOBitacora();
                        oBitGenerarLR.sFolio = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                        oBitGenerarLR.sEvento = "POST^" + Request.RequestUri.ToString(); ;
                        oBitGenerarLR.sObservacion =
                            "[INFO]{GENERAR_REFERENCIA}Inicio de petición para generar una línea de referencia.";
                        oBitGenerarLR.sHttpUserAgent = sUserAgentHttp;
                        oBitGenerarLR.sIPCliente = sIpAddress;
                        oBitGenerarLR.sInputParams = _ParamsJson.ToString(Formatting.None);
                        _iIdBitacora = oDaoconsulta.GuardarBitacora(oBitGenerarLR, new List<DTODetalleBitacora>());
                        oBitGenerarLR = null;
                        oDaoconsulta = null;
                        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                        //Invocar servicio para obtener los datos de la linea de Referencia
                        oJsonInputs.sFolioExecute = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                        oJsonRespWS = FuncionesWeb.generarReferenciaGNWS(oJsonInputs);
                        oJsonInputs = null;
                        if (oJsonRespWS.lError) {
                            sLinealog = sIdProceso + oJsonRespWS.iError;
                            sLinealog += "|" + oJsonRespWS.cError;
                            Utilerias.guardarLogEventos(sLinealog);

                            oJsonResponse["iError"] = oJsonRespWS.iError;
                            oJsonResponse["cError"] = oJsonRespWS.cError;

                            FuncionesWeb.enviarEmailNotificacion(
                                "ERROR al invocar el servicio para generar una línea de referencia[/generar/referenciagn].",
                                "ERROR al invocar el servicio para generar una línea de referencia.",
                                sLinealog
                            );
                            //FIN:RESPUESTA ERROR EN SERVICIO
                        }//fin: if (oJsonResp.lError)
                        else {
                            if (oJsonRespWS.oJsonData != null) {
                                oJsonResponse.Remove("iError");
                                oJsonResponse.Remove("cError");

                                oJsonResponse = (JObject)oJsonRespWS.oJsonData.DeepClone();
                            }//fin:if
                            else {
                                //Error
                                sLinealog = sIdProceso + "La respuesta Json del WebSpeed no es correcta.";
                                Utilerias.guardarLogEventos(sLinealog);

                                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfig, Constantes.KEY_MSG_ERROR_WSGENERARREF_01);
                                oJsonResponse["iError"] = oMsgError.iError;
                                oJsonResponse["cError"] = oMsgError.cError;
                                oMsgError = null;
                            }//fin:else                  
                        }//fin:else                               
                        oJsonRespWS.oJsonData = null;
                        oJsonRespWS = null;
                    }//fin:if (bAppKeyHabilitada)
                    else {
                        //Error en el permiso del appkey para utilizar el servicio
                        sLinealog = sIdProceso + "El AppKey no está autorizado para invocar al servicio.";
                        Utilerias.guardarLogEventos(sLinealog);

                        oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfig, Constantes.KEY_MSG_ERROR_WSGENERARREF_03);
                        oJsonResponse["iError"] = oMsgError.iError;
                        oJsonResponse["cError"] = oMsgError.cError;
                        oMsgError = null;
                    }//fin:else if (bAppKeyHabilitada)                    
                }//fin:else if (string.IsNullOrEmpty(_ParamsJson.Value<string>("cAppKey"))
            }//fin:try
            catch (Exception oEx) {
                sLinealog = sIdProceso + "Error en [ObtenerHojaAtencion]: ";
                sLinealog += "Message^" + oEx.Message;
                sLinealog += "~Source^" + oEx.Source;
                sLinealog += "~Target^" + oEx.TargetSite.ToString();
                sLinealog += "~StackTrace^" + oEx.StackTrace;
                Utilerias.guardarLogEventos(sLinealog);

                //Obtener la configuracion global del reporte a mostrar
                sFileNameConfig = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");

                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfig, Constantes.KEY_MSG_ERROR_EXCEPCION);
                oJsonResponse["iError"] = oMsgError.iError;
                oJsonResponse["cError"] = oMsgError.cError;
                oMsgError = null;
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //Guardar registro de bitacora 
                if (oDaoconsulta != null) oDaoconsulta = null;
                oDaoconsulta = new DaoConsultas();
                oBitGenerarLR = new DTOBitacora();
                oBitGenerarLR.sFolio = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                oBitGenerarLR.sEvento = "EXCEPCION en [/generar/referenciagn]";
                oBitGenerarLR.sObservacion =
                   "[ERROR]{GENERAR_REFERENCIA} Ocurrió una excepcion en la ejecución del servicio.";
                oBitGenerarLR.sHttpUserAgent = sUserAgentHttp;
                oBitGenerarLR.sIPCliente = sIpAddress;
                oBitGenerarLR.sInputParams = string.Empty;
                _iIdBitacora = oDaoconsulta.GuardarBitacora(oBitGenerarLR, new List<DTODetalleBitacora>());
                oBitGenerarLR = null;
                oDaoconsulta = null;
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
                FuncionesWeb.enviarEmailNotificacion(
                    "ERROR al invocar el servicio para generar una línea de referencia.",
                    "ERROR, ocurrió una excepción al ejecutar el servicio [/generar/referenciagn].",
                    sLinealog
                );
            }//fin: catch
           
            return Ok(oJsonResponse);

        }//fin:GenerarReferenciaVUGN
        //---------------------------------------------------------------------
        [HttpPost]
        [ActionName("referencia")]
        public IHttpActionResult GenerarReferenciaVUE([FromBody]JObject _ParamsJson) {

            int    _iIdBitacora = 0;
            bool   bAppKeyHabilitada = false;
            double dImporteTramite = 0.00;
            string sIdProceso = string.Empty;
            string sLinealog = string.Empty;
            string sFileNameConfig = string.Empty;

            string sUserAgentHttp = string.Empty;
            string sIpAddress = "0.0.0.0";
            string sAttrValue   = string.Empty;
            string sErrorMetodo = string.Empty;

            StringBuilder sStringBuildAux = null;
            HttpContextWrapper oHttpContextW = null;

            TJsonRespuesta oJsonRespWS = null;
            TInputGenerarLR oJsonInputs = null;
            TPostResponse oMsgError = null;
            DTOBitacora oBitGenerarLR = null;
            DaoConsultas oDaoconsulta = null;
            HttpResponseMessage oHttpMsgError = null;

            JObject oJsonResponse = new JObject();

            oJsonResponse["iError"] = 0;
            oJsonResponse["cError"] = "";

            if (_ParamsJson == null) {
                //TODO : Complementar con mensajes para ser explicito el error               
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_EMPTY);
                throw new HttpResponseException(oHttpMsgError);
            }//fin:iif

            if (_ParamsJson["cAppKey"] == null || _ParamsJson["iIdTramite"] == null ||
                _ParamsJson["iIdDependencia"] == null || _ParamsJson["iIdSubDependencia"] == null ||
                _ParamsJson["cIdentificadorUnico"] == null || _ParamsJson["oRecibo"] == null) {        
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_NO_REQUERIDOS);
                throw new HttpResponseException(oHttpMsgError);
            }//fin:iif

            sIdProceso = "[" + DateTime.Now.ToString("yyyyMMdd.HHmmss.fff") + "]";

            try {
                //Obtener la direccion ipcliente
                if (Request.Properties.ContainsKey("MS_HttpContext")) {
                    oHttpContextW = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                    if (oHttpContextW != null) {
                        sIpAddress = oHttpContextW.Request.UserHostAddress;
                    }//fin:if
                }//fin:if

                // Get user agent.
                if (Request.Headers.Contains("User-Agent")) {
                    var headers = Request.Headers.GetValues("User-Agent");
                    sStringBuildAux = new StringBuilder();
                    foreach (var header in headers) {
                        sStringBuildAux.Append(header);
                        sStringBuildAux.Append(" ");
                    }//fin:foreach
                    sUserAgentHttp = sStringBuildAux.ToString().Trim();
                    sStringBuildAux = null;
                }//fin:if
                
                sFileNameConfig = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");

                //Validar la integridad de los datos                
                if (string.IsNullOrEmpty(_ParamsJson.Value<string>("cAppKey")) ||
                    string.IsNullOrEmpty(_ParamsJson.Value<string>("iIdTramite")) ||
                    string.IsNullOrEmpty(_ParamsJson.Value<string>("iIdDependencia")) ||
                    string.IsNullOrEmpty(_ParamsJson.Value<string>("iIdSubDependencia")) ||
                    string.IsNullOrEmpty(_ParamsJson.Value<string>("cIdentificadorUnico")) ||
                    _ParamsJson.Value<JObject>("oRecibo")["cNombreRazonSocial"] == null) {

                    oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfig, Constantes.KEY_MSG_ERROR_WSGENERARREF_02);
                    oJsonResponse["iError"] = oMsgError.iError;
                    oJsonResponse["cError"] = oMsgError.cError;
                    oMsgError = null;
                    // FIN APLICACION CON ERROR
                }//fin:if (string.IsNullOrEmpty(_ParamsJson.Value<string>("cAppKey"))
                else {
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    //Evaluar el AppKey tiene permiso de ejecucion
                    bAppKeyHabilitada = FuncionesWeb.comprobarAppKeyHabilitada(
                        _ParamsJson.Value<string>("cAppKey"), ref sErrorMetodo
                    );

                    if (bAppKeyHabilitada) {
                        oJsonInputs = new TInputGenerarLR();
                        oJsonInputs.Clear();

                        //Obtener el valor de la vigencia de la Linea de Referencia
                        if (_ParamsJson["dtFechaVigencia"] != null) {
                            sAttrValue = _ParamsJson.Value<string>("dtFechaVigencia");
                            if (!string.IsNullOrEmpty(sAttrValue)) {
                                oJsonInputs.sFechaVigenciaLR = sAttrValue;
                            }//fin:if
                        }//fin:if

                        //Obtener el valor [dImporteTramite] para aquellos tramites que reciban importe
                        dImporteTramite = 0.00;
                        if (_ParamsJson["dImporteTramite"] != null) {
                            sAttrValue = _ParamsJson.Value<string>("dImporteTramite");
                            if (!string.IsNullOrEmpty(sAttrValue)) {
                                if (double.TryParse(sAttrValue, out dImporteTramite)) {
                                    oJsonInputs.sImporteTramite = (Math.Round(dImporteTramite, 2)).ToString();
                                }//fin:if
                            }//fin:if
                        }//fin: if (_ParamsJson["dImporteTramite"] != null)

                        oJsonInputs.sAppKey = _ParamsJson.Value<string>("cAppKey");
                        oJsonInputs.iIdTramite = _ParamsJson.Value<int>("iIdTramite");
                        oJsonInputs.iIdDependencia = _ParamsJson.Value<int>("iIdDependencia");
                        oJsonInputs.iIdSubDependencia = _ParamsJson.Value<int>("iIdSubDependencia");

                        JObject oJRecibo = _ParamsJson.Value<JObject>("oRecibo");
                        int iCodigoPostalRec = 0;

                        if (!string.IsNullOrEmpty(oJRecibo.Value<string>("iCodigoPostal"))) {
                            if (!Int32.TryParse(oJRecibo.Value<string>("iCodigoPostal").Trim(), out iCodigoPostalRec)) {
                                iCodigoPostalRec = 0;
                            }//fin:if
                        }//fin:if

                        oJsonInputs.sDatosRecibos = String.Format(Constantes.CONFIG_FORMAT_RECIBO,
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cRFC")) ? "" : oJRecibo.Value<string>("cRFC").Trim(), // 0
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cNombreRazonSocial")) ? "" : oJRecibo.Value<string>("cNombreRazonSocial").Trim(), // 1
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cDomicilio")) ? "" : oJRecibo.Value<string>("cDomicilio").Trim(), // 2
                            iCodigoPostalRec > 0 ? iCodigoPostalRec.ToString() : "", // 3
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cMunicipio")) ? "" : oJRecibo.Value<string>("cMunicipio").Trim(), // 4
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cLocalidad")) ? "" : oJRecibo.Value<string>("cLocalidad").Trim(), // 5
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cColonia")) ? "" : oJRecibo.Value<string>("cColonia").Trim(), // 6
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cEmail")) ? "" : oJRecibo.Value<string>("cEmail").Trim(), // 7
                            string.IsNullOrEmpty(oJRecibo.Value<string>("cTelefono")) ? "" : oJRecibo.Value<string>("cTelefono").Trim()  //8
                        );

                        if (!string.IsNullOrEmpty(_ParamsJson.Value<string>("cIdentificadorUnico"))) {
                            oJsonInputs.sAtributos = string.Format(
                                "{0}^{1}", Constantes.ATRIBUTO_UNICO_ETIQ,
                                _ParamsJson.Value<string>("cIdentificadorUnico")
                            );
                        }//fin:                    
                         //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                         //Guardar registro de bitacora 
                        oDaoconsulta = new DaoConsultas();
                        oBitGenerarLR = new DTOBitacora();
                        oBitGenerarLR.sFolio = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                        oBitGenerarLR.sEvento = "POST^" + Request.RequestUri.ToString(); ;
                        oBitGenerarLR.sObservacion =
                            "[INFO]{GENERAR_REFERENCIA}Inicio de petición para generar una línea de referencia.";
                        oBitGenerarLR.sHttpUserAgent = sUserAgentHttp;
                        oBitGenerarLR.sIPCliente = sIpAddress;
                        oBitGenerarLR.sInputParams = _ParamsJson.ToString(Formatting.None);
                        _iIdBitacora = oDaoconsulta.GuardarBitacora(oBitGenerarLR, new List<DTODetalleBitacora>());
                        oBitGenerarLR = null;
                        oDaoconsulta = null;
                        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                        //Invocar servicio para obtener los datos de la linea de Referencia
                        oJsonInputs.sFolioExecute = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                        oJsonRespWS = FuncionesWeb.generarReferenciaWS(oJsonInputs);
                        oJsonInputs = null;
                        if (oJsonRespWS.lError) {
                            sLinealog = sIdProceso + oJsonRespWS.iError;
                            sLinealog += "|" + oJsonRespWS.cError;
                            Utilerias.guardarLogEventos(sLinealog);

                            oJsonResponse["iError"] = oJsonRespWS.iError;
                            oJsonResponse["cError"] = oJsonRespWS.cError;

                            FuncionesWeb.enviarEmailNotificacion(
                                "ERROR al invocar el servicio para generar una línea de referencia[/generar/referencia].",
                                "ERROR al invocar el servicio para generar una línea de referencia.",
                                sLinealog
                            );
                            //FIN:RESPUESTA ERROR EN SERVICIO
                        }//fin: if (oJsonResp.lError)
                        else {
                            if (oJsonRespWS.oJsonData != null) {
                                oJsonResponse.Remove("iError");
                                oJsonResponse.Remove("cError");

                                oJsonResponse = (JObject)oJsonRespWS.oJsonData.DeepClone();
                            }//fin:if
                            else {
                                //Error
                                sLinealog = sIdProceso + "La respuesta Json del WebSpeed no es correcta.";
                                Utilerias.guardarLogEventos(sLinealog);

                                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfig, Constantes.KEY_MSG_ERROR_WSGENERARREF_01);
                                oJsonResponse["iError"] = oMsgError.iError;
                                oJsonResponse["cError"] = oMsgError.cError;
                                oMsgError = null;
                            }//fin:else                  
                        }//fin:else                               
                        oJsonRespWS.oJsonData = null;
                        oJsonRespWS = null;
                    }//fin:if (bAppKeyHabilitada)
                    else {
                        //Error en el permiso del appkey para utilizar el servicio
                        sLinealog = sIdProceso + "El AppKey no está autorizado para invocar al servicio.";
                        Utilerias.guardarLogEventos(sLinealog);

                        oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfig, Constantes.KEY_MSG_ERROR_WSGENERARREF_03);
                        oJsonResponse["iError"] = oMsgError.iError;
                        oJsonResponse["cError"] = oMsgError.cError;
                        oMsgError = null;
                    }//fin:else if (bAppKeyHabilitada)
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                    
                }//fin:else if (string.IsNullOrEmpty(_ParamsJson.Value<string>("cAppKey"))
            }//fin:try
            catch (Exception oEx) {
                sLinealog = sIdProceso + "Error en [GenerarReferenciaVUE]: ";
                sLinealog += "Message^" + oEx.Message;
                sLinealog += "~Source^" + oEx.Source;
                sLinealog += "~Target^" + oEx.TargetSite.ToString();
                sLinealog += "~StackTrace^" + oEx.StackTrace;
                Utilerias.guardarLogEventos(sLinealog);

                //Obtener la configuracion global del reporte a mostrar
                sFileNameConfig = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");

                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfig, Constantes.KEY_MSG_ERROR_EXCEPCION);
                oJsonResponse["iError"] = oMsgError.iError;
                oJsonResponse["cError"] = oMsgError.cError;
                oMsgError = null;
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //Guardar registro de bitacora 
                if (oDaoconsulta != null) oDaoconsulta = null;
                oDaoconsulta = new DaoConsultas();
                oBitGenerarLR = new DTOBitacora();
                oBitGenerarLR.sFolio = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                oBitGenerarLR.sEvento = "EXCEPCION en [/generar/referencia]";
                oBitGenerarLR.sObservacion =
                   "[ERROR]{GENERAR_REFERENCIA} Ocurrió una excepcion en la ejecución del servicio.";
                oBitGenerarLR.sHttpUserAgent = sUserAgentHttp;
                oBitGenerarLR.sIPCliente = sIpAddress;
                oBitGenerarLR.sInputParams = string.Empty;
                _iIdBitacora = oDaoconsulta.GuardarBitacora(oBitGenerarLR, new List<DTODetalleBitacora>());
                oBitGenerarLR = null;
                oDaoconsulta = null;
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
                FuncionesWeb.enviarEmailNotificacion(
                    "ERROR al invocar el servicio para generar una línea de referencia.",
                    "ERROR, ocurrió una excepción al ejecutar el servicio [/generar/referencia].",
                    sLinealog
                );
            }//fin: catch

            return Ok(oJsonResponse);

        }//fin:GenerarReferenciaVUE
        //---------------------------------------------------------------------
        //---------------------------------------------------------------------
        public List<TDataConceptoRef> AgruparTramitesFichaPago(string _sJlistaDetalles, ref string _sOutError) {

            double dSumaTotal = 0.00;
            List<TDataConceptoRef> listGrupos = new List<TDataConceptoRef>();

            List<TDataTramite> listTramites = null;

            TDataTramite oDataTramite = null;
            TDataConceptoRef oConceptoTmp = null;
            JArray aJListDetalles = null;
            JArray aJListConceptos = null;
            Dictionary<string, TDataConceptoRef> dictGrupos = null;

            try {

                dictGrupos = new Dictionary<string, TDataConceptoRef>();
                listTramites = new List<TDataTramite>();
                aJListDetalles = JArray.Parse(_sJlistaDetalles);
                foreach (JObject oJDetTramite in aJListDetalles) {
                    oDataTramite = new TDataTramite();
                    oDataTramite.dTotal = oJDetTramite.Value<double>("dImporte");
                    oDataTramite.iOrdenTramite = oJDetTramite.Value<int>("iOrden"); 
                    oDataTramite.cNombreTramite = oJDetTramite.Value<string>("cNombreTramite");                    
                    oDataTramite.iIdTramite = oJDetTramite.Value<int>("iIdTramite");

                    //Sumarizar los importes unitarios de los conceptos                    
                    aJListConceptos = JArray.Parse(oJDetTramite.SelectToken("aConceptos").ToString());
                    dSumaTotal = 0.00;
                    foreach (JObject oJsonConcepto in aJListConceptos.Children<JObject>()) {
                        dSumaTotal += oJsonConcepto.Value<double>("dImporteUnitario");
                    }//fin:foreach  
                    oDataTramite.dImporteUnitTotal = dSumaTotal;

                    listTramites.Add(oDataTramite);
                }//fin:foreach

                //Iteracion para generar los grupos, emulando los conceptos
                foreach (TDataTramite oTTramite in listTramites) {
                    if (dictGrupos.ContainsKey(oTTramite.iIdTramite.ToString())) {
                        oConceptoTmp = dictGrupos[oTTramite.iIdTramite.ToString()];
                        oConceptoTmp.iCantidad++;
                        oConceptoTmp.cConcepto = oTTramite.cNombreTramite;
                        oConceptoTmp.dImporteUnitario += oTTramite.dImporteUnitTotal;
                        oConceptoTmp.dtotal += oTTramite.dTotal;
                    }//fin:if
                    else {
                        oConceptoTmp = new TDataConceptoRef() {
                            iOrden = 0,
                            iCantidad = 1,
                            cConcepto = oTTramite.cNombreTramite,
                            dImporteUnitario = oTTramite.dImporteUnitTotal,
                            dtotal = oTTramite.dTotal,
                        };
                        dictGrupos.Add(oTTramite.iIdTramite.ToString(), oConceptoTmp);
                    }//else                   
                }//fin:foreach
                listTramites = null; aJListConceptos = null;

                foreach (KeyValuePair<string, TDataConceptoRef> oElement in dictGrupos) {
                    listGrupos.Add(new TDataConceptoRef() {
                        iOrden = 0,
                        iCantidad = oElement.Value.iCantidad,
                        cConcepto = oElement.Value.cConcepto,
                        dImporteUnitario = oElement.Value.dImporteUnitario,
                        dtotal = oElement.Value.dtotal
                    });
                }//fin:
                dictGrupos.Clear(); dictGrupos = null;
            }//fin:try
            catch (Exception oEx) {
                _sOutError = "Error en [AgruparTramitesFichaPago]: ";
                _sOutError += "Message^" + oEx.Message;
                _sOutError += "~Source^" + oEx.Source;
                _sOutError += "~Target^" + oEx.TargetSite.ToString();
                _sOutError += "~StackTrace^" + oEx.StackTrace;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                                                                
            }//fin:catch

            return listGrupos;

        }//fin:metod
         //---------------------------------------------------------------------
        public List<TDataAtributos> AgruparAtributos(JArray _sJlistaDetalles, ref string _sOutError)
        {

            List<TDataConceptoRef> listGrupos = new List<TDataConceptoRef>();

            List<TDataTramite> listTramites = null;
            JArray aJListDetalles = null;
            bool bAgregar = true;
            JArray aJListAtributos = null;
            Dictionary<string, TDataConceptoRef> dictGrupos = null;
            List<TDataAtributos> listAtributos = new List<TDataAtributos>();
            try
            {

                dictGrupos = new Dictionary<string, TDataConceptoRef>();
                listTramites = new List<TDataTramite>();
                aJListDetalles = _sJlistaDetalles;

                foreach (JObject oJDetTramite in aJListDetalles)
                {
                    aJListAtributos = JArray.Parse(oJDetTramite["aAtributos"].ToString());
    
                    foreach (JObject oJsonAtributo in aJListAtributos)
                    {
                        for (int i = 0; i < listAtributos.Count(); i++)
                        {
                            if (listAtributos[i].cAtributo == oJsonAtributo.Value<string>("cAtributo"))
                            {
                                listAtributos[i].cValor = listAtributos[i].cValor + "," + oJsonAtributo.Value<string>("cValor");

                                bAgregar = false;

                                break;
                            }

                            bAgregar = true;
                            
                        }

                        if (bAgregar)
                        {
                            listAtributos.Add(new TDataAtributos()
                            {
                                iOrden = oJsonAtributo.Value<int>("iOrden"),
                                cAtributo = oJsonAtributo.Value<string>("cAtributo"),
                                cValor = oJsonAtributo.Value<string>("cValor")
                            });
                        }

                    }//fin: foreach (JObject oJsonAtributo in aJListAtributos.Children<JObject>())

                }//fin:foreach

            }//fin:try
            catch (Exception oEx)
            {
                _sOutError = "Error en [AgruparAtributos]: ";
                _sOutError += "Message^" + oEx.Message;
                _sOutError += "~Source^" + oEx.Source;
                _sOutError += "~Target^" + oEx.TargetSite.ToString();
                _sOutError += "~StackTrace^" + oEx.StackTrace;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                                                                
            }//fin:catch

            return listAtributos;

        }//fin:metod
        //---------------------------------------------------------------------
    }//fin:class
}
