﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json;
using wsVentanillaUnicaREST.Util;
using wsVentanillaUnicaREST.Models;
using wsVentanillaUnicaREST.dao;
using System.IO;
using System.Configuration;

namespace wsVentanillaUnicaREST.Controllers{

    [RoutePrefix("consultar")]
    public class ConsultarController : ApiController{
        //---------------------------------------------------------------------
        [HttpPost]
        [ActionName("referencia")]
        public IHttpActionResult ConsultarReferenciaVU([FromBody]JObject _PostParams) {

            int _iIdBitacora = 0;
            bool bAppKeyHabilitada = false;
            string sIdProceso = string.Empty;
            string sLinealog = string.Empty;
            string urlBase = string.Empty;
            string sErrorMetodo = string.Empty;
            Dictionary<string, string> oDictConfig = null;
            string KeyConfig = string.Empty;
            string stAppAmbiente = string.Empty;

            string sFileNameConfigMsg = string.Empty;
            string sFileNameConfig = string.Empty;
            string sParamLineaRef = string.Empty;
            String claveCaja = string.Empty;
            bool bCaja = false;
            string sParamAppKey   = string.Empty;
            string[] sCajas = new string[] { };
            string stPermitidosR = string.Empty;

            string sUserAgentHttp = string.Empty;
            string sIpAddress     = "0.0.0.0";

            StringBuilder sStringBuildAux = null;

            TPostResponse oMsgError = null;
            JArray jaDatos = new JArray();
            DTOConfiguracion oConfig = null;
            TJsonRespuesta oJsonRespWS = null;
            HttpContextWrapper oHttpContextW = null;

            DTOBitacora oBitConsulta = null;
            DaoConsultas oDaoDatos = null;
            HttpResponseMessage oHttpMsgError = null;

            JObject oJsonResponse = new JObject();

            oJsonResponse["iError"] = false;
            oJsonResponse["cError"] = "";
            oJsonResponse["aDatos"] = JToken.FromObject(new List<JObject>());

            if (_PostParams == null) {
                //TODO : Complementar con mensajes para ser explicito el error               
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_EMPTY);
                throw new HttpResponseException(oHttpMsgError);
            }//fin:iif

            if (_PostParams["cAppKey"] == null || _PostParams["cLineaReferencia"] == null) {
                //TODO : Complementar con mensajes para ser explicito el error
                //throw new HttpResponseException(HttpStatusCode.BadRequest);
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_NO_REQUERIDOS);
                throw new HttpResponseException(oHttpMsgError);
            }//fin:iif

            try {

                //Obtener la direccion ipcliente
                if (Request.Properties.ContainsKey("MS_HttpContext")) {
                    oHttpContextW = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                    if (oHttpContextW != null) {
                        sIpAddress = oHttpContextW.Request.UserHostAddress;
                    }//fin:if
                }//fin:if

                // Get user agent.
                if (Request.Headers.Contains("User-Agent")) {
                    var headers = Request.Headers.GetValues("User-Agent");
                    sStringBuildAux = new StringBuilder();
                    foreach (var header in headers) {
                        sStringBuildAux.Append(header);
                        sStringBuildAux.Append(" ");
                    }//fin:foreach
                    sUserAgentHttp = sStringBuildAux.ToString().Trim();
                    sStringBuildAux = null;
                }//fin:if

                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Obtener variables de configuracion
                oDaoDatos = new DaoConsultas();

                if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null)
                {
                    stAppAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                }//fin:if    

                switch (stAppAmbiente)
                {
                    case Constantes._DESARROLLO:
                        KeyConfig = Constantes.DES_URL_RECIBO;
                        break;
                    case Constantes._PRODUCCION:
                    case Constantes._TEST_PRODUCCION:
                        KeyConfig = Constantes.PROD_URL_RECIBO;
                        break;
                }//fin:switch (stAppAmbiente)

                oConfig = oDaoDatos.ObtenerValorConfig(KeyConfig);
                stPermitidosR = oDaoDatos.ObtenerValorConfig(Constantes.VUE_CAJAS_DIGITALES).cValor;

                sFileNameConfigMsg = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");
                sFileNameConfig = System.Web.Hosting.HostingEnvironment.MapPath("~/config/config_reportes.json");

                sParamAppKey   = HttpUtility.UrlDecode(_PostParams.Value<string>("cAppKey").Trim());
                sParamLineaRef = HttpUtility.UrlDecode(_PostParams.Value<string>("cLineaReferencia").Trim());
                
                //validar que el parametro referencia solo contenga numeros                              
                sIdProceso = "[" + DateTime.Now.ToString("yyyyMMdd.HHmmss.fff") + "]";
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //Guardar registro de bitacora 
                oBitConsulta = new DTOBitacora();                              
                oBitConsulta.sFolio       = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                oBitConsulta.sEvento      = "POST^" + Request.RequestUri.ToString();
                oBitConsulta.sObservacion = 
                    "[INFO]{CONSULTAR_REFERENCIA}Inicio de petición información para la referencia:" + sParamLineaRef;
                oBitConsulta.sHttpUserAgent = sUserAgentHttp;
                oBitConsulta.sIPCliente = sIpAddress;
                oBitConsulta.sInputParams = _PostParams.ToString(Formatting.None);
                _iIdBitacora = oDaoDatos.GuardarBitacora(oBitConsulta, new List<DTODetalleBitacora>());               
                oBitConsulta = null;
                oDaoDatos = null;
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                
                if (Regex.IsMatch(sParamLineaRef, "^[0-9]*$")) {

                    bAppKeyHabilitada = FuncionesWeb.comprobarAppKeyHabilitada(sParamAppKey, ref sErrorMetodo);
                    if (!string.IsNullOrEmpty(sErrorMetodo)) Utilerias.guardarLogEventos(sErrorMetodo);

                    //AppKey Habilitada para hacer uso de los servicos para VUE
                    if (bAppKeyHabilitada) {
                        
                        //Invocar servicio para obtener los datos de la linea de Referencia
                        oJsonRespWS = FuncionesWeb.consultarReferenciaVU(
                            sParamLineaRef, sParamAppKey, sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty)
                        );
                        if (oJsonRespWS.lError) {

                            sLinealog  = sIdProceso + "[" + sParamLineaRef + "]";                            
                            sLinealog += oJsonRespWS.iError.ToString() + "|" + oJsonRespWS.cError;
                            Utilerias.guardarLogEventos(sLinealog);

                            oJsonResponse["iError"] = oJsonRespWS.iError;
                            oJsonResponse["cError"] = oJsonRespWS.cError;

                            FuncionesWeb.enviarEmailNotificacion(
                                "ERROR al invocar el servicio para obtener las Detalle de Referencias[/consultar/referencia].",
                                "ERROR al invocar el servicio para consultar una línea de referencia.",
                                sLinealog
                            );
                            //FIN:RESPUESTA ERROR EN SERVICIO
                        }//fin: if (oJsonResp.lError)
                        else {
                            //OK, Retornar estructura de datos para consulta del cliente                        
                            //Comprobar el Nodo [aDatos]

                            if (oJsonRespWS.oJsonData["aDatos"] != null) {
                                oJsonResponse["iError"] = 0;
                                oJsonResponse["cError"] = "";

                                //Obtener la configuracion global
                                sErrorMetodo = string.Empty;
                                oDictConfig = Utilerias.obtenerConfigReporte(sFileNameConfig, string.Empty, ref sErrorMetodo);

                                if (!string.IsNullOrEmpty(sErrorMetodo)) {
                                    sLinealog = sIdProceso + "";
                                    Utilerias.guardarLogEventos(sLinealog);

                                    oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_13);
                                    oJsonResponse["iError"] = oMsgError.iError;
                                    oJsonResponse["cError"] = oMsgError.cError;
                                    oMsgError = null;
                                }
                                else
                                {
                                    urlBase = oConfig.cValor;
                                    sCajas = stPermitidosR.Split(',');
                                    jaDatos = oJsonRespWS.oJsonData.Value<JArray>("aDatos");

                                    for (int i = 0; i < jaDatos.Count(); i++)
                                    {
                                        //Validar cajas
                                        foreach (var sCaja in sCajas)
                                        {
                                            claveCaja = string.Empty;
                                            claveCaja = int.Parse(sCaja).ToString("0000");
                                            if (claveCaja == jaDatos[i].Value<string>("cIDCaja"))
                                            {
                                                bCaja = true;
                                                break;
                                            }
                                        }

                                        if (bCaja && jaDatos[i].Value<string>("cEstatusPago") == oDictConfig["cEstatusPago"])
                                        {
                                            jaDatos[i]["cUrl"] = urlBase.Replace("@1", jaDatos[i].Value<string>("cLineaReferencia"));
                                        }

                                        bCaja = false;
                                    }
                                }
                                oJsonResponse["aDatos"] = jaDatos;
                            }//fin:if
                            else {
                                sLinealog = sIdProceso + "La respuesta Json no el nodo [aDatos]";
                                Utilerias.guardarLogEventos(sLinealog);

                                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_WSCONSULTAR_REF_02);
                                oJsonResponse["iError"] = oMsgError.iError;
                                oJsonResponse["cError"] = oMsgError.cError;
                                oMsgError = null;
                            }//fin:else
                        }//fin:else
                        oJsonRespWS.oJsonData = null;
                        oJsonRespWS = null;
                    }//fin:if (bAppKeyHabilitada)
                    else {                       
                        //Error, falta de permiso de la AppKey
                        sLinealog = sIdProceso + sErrorMetodo;                   
                        Utilerias.guardarLogEventos(sLinealog);

                        oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_WSCONSULTAR_REF_03);
                        oJsonResponse["iError"] = oMsgError.iError;
                        oJsonResponse["cError"] = oMsgError.cError;
                        oMsgError = null;
                    }//fin:else if (bAppKeyHabilitada)                    
                }//fin:if (Regex.IsMatch(sParamLineaRef, "^[0-9]*$"))
                else {
                    //Error en el formato de la referencia
                    sLinealog = sIdProceso + "La línea de referencia [" + sParamLineaRef + "]";                       
                    sLinealog += " no tiene un formato correcto.";
                    Utilerias.guardarLogEventos(sLinealog);

                    oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_WSCONSULTAR_REF_01);
                    oJsonResponse["iError"] = oMsgError.iError;
                    oJsonResponse["cError"] = oMsgError.cError;
                    oMsgError = null;                                     
                }//fin:else                                           
            }//fin:try
            catch (Exception oEx) {
                sLinealog = sIdProceso + "Error en [ConsultarReferenciaVU]: ";
                sLinealog += "Message^" + oEx.Message;
                sLinealog += "~Source^" + oEx.Source;
                sLinealog += "~Target^" + oEx.TargetSite.ToString();
                sLinealog += "~StackTrace^" + oEx.StackTrace;
                Utilerias.guardarLogEventos(sLinealog);

                //Obtener la configuracion global del reporte a mostrar
                sFileNameConfigMsg = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");

                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_99);
                oJsonResponse["iError"] = oMsgError.iError;
                oJsonResponse["cError"] = oMsgError.cError;
                oMsgError = null;                
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //Obtener la direccion ipcliente
                if (Request.Properties.ContainsKey("MS_HttpContext")) {
                   oHttpContextW = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                   if (oHttpContextW != null) {
                       sIpAddress = oHttpContextW.Request.UserHostAddress;
                   }//fin:if
                }//fin:if                
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //Guardar registro de bitacora 
                if (oDaoDatos != null) oDaoDatos = null;
                oDaoDatos = new DaoConsultas();
                oBitConsulta = new DTOBitacora();                              
                oBitConsulta.sFolio       = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                oBitConsulta.sEvento      = "EXCEPCION en [consultar/referencia]";
                oBitConsulta.sObservacion = 
                   "[ERROR]{CONSULTAR_REFERENCIA} Ocurrió una excepcion en la ejecución del servicio.";
                oBitConsulta.sHttpUserAgent = sUserAgentHttp;
                oBitConsulta.sIPCliente = sIpAddress;
                oBitConsulta.sInputParams = string.Empty;
                _iIdBitacora = oDaoDatos.GuardarBitacora(oBitConsulta, new List<DTODetalleBitacora>());               
                oBitConsulta = null;
                oDaoDatos = null;
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                   
                FuncionesWeb.enviarEmailNotificacion(
                    "ERROR al invocar el servicio para consultar referencia.",
                    "ERROR, ocurrió una excepción al ejecutar el servicio [/consultar/referencia].",
                    sLinealog
                );
            }//fin: catch

            return Ok(oJsonResponse);

        }//fin:ConsultarReferenciaVU
         //---------------------------------------------------------------------
         //---------------------------------------------------------------------
        [HttpPost]
        [ActionName("recibosOLD")]
        public IHttpActionResult ConsultarRecibosOLD([FromBody]JObject _PostParams)
        {

            int _iIdBitacora = 0;
            bool bAppKeyHabilitada = false;
            string sUrlRecibo = string.Empty;
            bool bPdf = true;
            bool bPDFimprime = false;
            bool bUnDocumento = false;
            string sIdProceso = string.Empty;
            bool bUrlEncode = true;
            bool bPrefijo = false;
            string urlBase = string.Empty;
            string stAppAmbiente = string.Empty;
            string sBase64HojaPDF = string.Empty;
            string sFileNameConfig = string.Empty;
            string file = string.Empty;
            string sLinealog = string.Empty;
            string KeyConfig = string.Empty;
            string sErrorMetodo = string.Empty;
            string sRutaTempPDF = string.Empty;
            string sServerPath = string.Empty;
            string sFileNameConfigMsg = string.Empty;
            string sParamLineaRef = string.Empty;
            string sParamAppKey = string.Empty;
            JObject ObjItem = null;
            Dictionary<string, string> oDictConfig = null;
            JArray jListAuxiliar = null;
            string[] sPrefijos = new string[] { };
            List<String> sListAuxiliar = null;
            TJsonRespuesta jsonItem = null;
            JArray aJsonTramites = null;

            string sUserAgentHttp = string.Empty;
            string sIpAddress = "0.0.0.0";

            StringBuilder sStringBuildAux = null;

            TPostResponse oMsgError = null;
            TJsonRespuesta oJsonRespWS = null;
            HttpContextWrapper oHttpContextW = null;

            DTOBitacora oBitConsulta = null;
            DTOConfiguracion oConfig = null;
            DTOConfiguracion stPermitidosR = null;
            DaoConsultas oDaoDatos = null;
            HttpResponseMessage oHttpMsgError = null;

            JObject oJsonResponse = new JObject();

            oJsonResponse["iError"] = 0;
            oJsonResponse["cError"] = "";
            

            if (_PostParams == null)
            {
                //TODO : Complementar con mensajes para ser explicito el error               
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_EMPTY);
                throw new HttpResponseException(oHttpMsgError);
            }//fin:iif

            if (_PostParams["cAppKey"] == null || _PostParams["cLineaReferencia"] == null)
            {
                //TODO : Complementar con mensajes para ser explicito el error
                //throw new HttpResponseException(HttpStatusCode.BadRequest);
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_NO_REQUERIDOS);
                throw new HttpResponseException(oHttpMsgError);
            }//fin:iif

            //DAP 20200820, parámetro para identificar si el PDF Base64 se aplica el UrlEncode            
            bUrlEncode = true;
            if (_PostParams["lUrlEncode"] != null)
            {
                if (!string.IsNullOrEmpty(_PostParams.Value<string>("lUrlEncode").Trim()))
                {
                    bUrlEncode = Convert.ToBoolean(_PostParams.Value<string>("lUrlEncode").Trim().ToLower());
                }//fin:if                
            }//fin:else
            else { bUrlEncode = true; }

            //Parámetro para identificar si se generan los base 64           
            bPdf = true;
            if (_PostParams["lPdf"] != null)
            {
                if (!string.IsNullOrEmpty(_PostParams.Value<string>("lPdf").Trim()))
                {
                    bPdf = Convert.ToBoolean(_PostParams.Value<string>("lPdf").Trim().ToLower());
                }//fin:if                
            }//fin:else
            else { bPdf = true; }

            //Parámetro para identificar si se generan los base 64           
            bPDFimprime = false;
            if (_PostParams["pdfImprime_"] != null)
            {
                if (!string.IsNullOrEmpty(_PostParams.Value<string>("pdfImprime_").Trim()))
                {
                    bPDFimprime = Convert.ToBoolean(_PostParams.Value<string>("pdfImprime_").Trim().ToLower());
                }//fin:if                
            }//fin:else
            else { bPDFimprime = false; }

            //Parámetro para identificar si se generan un solo PDF     
            bUnDocumento = false;
            if (_PostParams["lUnDocumento"] != null)
            {
                if (!string.IsNullOrEmpty(_PostParams.Value<string>("lUnDocumento").Trim()))
                {
                    bUnDocumento = Convert.ToBoolean(_PostParams.Value<string>("lUnDocumento").Trim().ToLower());
                }//fin:if                
            }//fin:else
            else { bUnDocumento = false; }

            //Obtener la configuracion global
            sFileNameConfig = System.Web.Hosting.HostingEnvironment.MapPath("~/config/config_reportes.json");
            sServerPath     = System.Web.Hosting.HostingEnvironment.MapPath("~");

            sErrorMetodo = string.Empty;
            oDictConfig = Utilerias.obtenerConfigReporte(sFileNameConfig, string.Empty, ref sErrorMetodo);

            if(string.IsNullOrEmpty(sErrorMetodo)){

                sErrorMetodo = string.Empty;
                sRutaTempPDF = Path.Combine(sServerPath, oDictConfig["ruta_filesystem_pdf"]);

                try
                {

                    //Obtener la direccion ipcliente
                    if (Request.Properties.ContainsKey("MS_HttpContext"))
                    {
                        oHttpContextW = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                        if (oHttpContextW != null)
                        {
                            sIpAddress = oHttpContextW.Request.UserHostAddress;
                        }//fin:if
                    }//fin:if

                    // Get user agent.
                    if (Request.Headers.Contains("User-Agent"))
                    {
                        var headers = Request.Headers.GetValues("User-Agent");
                        sStringBuildAux = new StringBuilder();
                        foreach (var header in headers)
                        {
                            sStringBuildAux.Append(header);
                            sStringBuildAux.Append(" ");
                        }//fin:foreach
                        sUserAgentHttp = sStringBuildAux.ToString().Trim();
                        sStringBuildAux = null;
                    }//fin:if
                    
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    //Obtener variables de configuracion
                    oDaoDatos = new DaoConsultas();

                    if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null)
                    {
                        stAppAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                    }//fin:if    

                    switch (stAppAmbiente)
                    {
                        case Constantes._DESARROLLO:
                            KeyConfig = Constantes.DES_URL_RECIBO;
                        break;
                        case Constantes._PRODUCCION:
                        case Constantes._TEST_PRODUCCION:
                            KeyConfig = Constantes.PROD_URL_RECIBO;
                        break;
                    }//fin:switch (stAppAmbiente)
 
                    oConfig = oDaoDatos.ObtenerValorConfig(KeyConfig);

                    //Obtener prefijos permitidos
                    stPermitidosR = oDaoDatos.ObtenerValorConfig(Constantes.VUE_PREFIJOS_PERMITIDOS);

                    sPrefijos = stPermitidosR.cValor.Split('@');

                    sFileNameConfigMsg = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");

                    sParamAppKey = HttpUtility.UrlDecode(_PostParams.Value<string>("cAppKey").Trim());
                    sParamLineaRef = HttpUtility.UrlDecode(_PostParams.Value<string>("cLineaReferencia").Trim());

                    //Validar prefijos permitidos
                    foreach (var sPrefijo in sPrefijos)
                    {
                        if (sPrefijo == sParamLineaRef.Substring(0, 3))
                        {
                            bPrefijo = true;
                            break;
                        }
                    }

                    //validar que el parametro referencia solo contenga numeros                              
                    sIdProceso = "[" + DateTime.Now.ToString("yyyyMMdd.HHmmss.fff") + "]";

                   
                    if (!Directory.Exists(sRutaTempPDF))
                    {
                        oJsonResponse["iError"] = 99;
                        oJsonResponse["cError"] = "La configuración del servicio no esta disponible";
                        return Ok(oJsonResponse);
                    }
                    
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    //Guardar registro de bitacora 
                    oBitConsulta = new DTOBitacora();
                    oBitConsulta.sFolio = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                    oBitConsulta.sEvento = "POST^" + Request.RequestUri.ToString();
                    oBitConsulta.sObservacion =
                        "[INFO]{CONSULTAR_RECIBOS}Inicio de petición información para los RECIBOS:" + sParamLineaRef;
                    oBitConsulta.sHttpUserAgent = sUserAgentHttp;
                    oBitConsulta.sIPCliente = sIpAddress;
                    oBitConsulta.sInputParams = _PostParams.ToString(Formatting.None);
                    _iIdBitacora = oDaoDatos.GuardarBitacora(oBitConsulta, new List<DTODetalleBitacora>());
                    oBitConsulta = null;
                    oDaoDatos = null;
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                
                    if (Regex.IsMatch(sParamLineaRef, "^[0-9]*$"))
                    {
                        if (bPrefijo == true)
                        {
                            bAppKeyHabilitada = FuncionesWeb.comprobarAppKeyHabilitada(sParamAppKey, ref sErrorMetodo);
                            if (!string.IsNullOrEmpty(sErrorMetodo)) Utilerias.guardarLogEventos(sErrorMetodo);

                            //AppKey Habilitada para hacer uso de los servicos para VUE
                            if (bAppKeyHabilitada)
                            {
                                urlBase = oConfig.cValor;
                                if (bPDFimprime)
                                {
                                    urlBase = "http://192.168.90.83/cgi-bin/wspd_cgi.sh/WService=wsingresos/internet/compartido/pde_imprime_pdf_.r?iID-frm=0&cRef-Frm=@1";
                                }

                                //Invocar servicio para obtener los tramites
                                oJsonRespWS = FuncionesWeb.obtenerRecibos(
                                    sParamLineaRef
                                );

                                if (oJsonRespWS.lError)
                                {

                                    sLinealog = sIdProceso + "[" + sParamLineaRef + "]";
                                    sLinealog += oJsonRespWS.iError.ToString() + "|" + oJsonRespWS.cError;
                                    Utilerias.guardarLogEventos(sLinealog);

                                    oJsonResponse["iError"] = oJsonRespWS.iError;
                                    oJsonResponse["cError"] = oJsonRespWS.cError;

                                    FuncionesWeb.enviarEmailNotificacion(
                                        "ERROR al invocar el servicio para obtener los tramites[/consultar/recibos].",
                                        "ERROR al invocar el servicio para obtener los tramites",
                                        sLinealog
                                    );
                                    //FIN:RESPUESTA ERROR EN SERVICIO
                                }//fin: if (oJsonResp.lError)
                                else
                                {
                                    aJsonTramites = JArray.Parse(oJsonRespWS.oJsonData.SelectToken("aRecibos").ToString());

                                    switch (sParamLineaRef.Substring(0, 3))
                                    {
                                        case "121":
                                                oJsonResponse = FuncionesWeb.RespReferenciaPrincipal(
                                                    aJsonTramites, urlBase, bPdf, sRutaTempPDF,  bUrlEncode, bUnDocumento
                                                );
                                            break;
                                        case "122":
                                            oJsonResponse = FuncionesWeb.RespReferenciaPrincipal(
                                                    aJsonTramites, urlBase, bPdf, sRutaTempPDF, bUrlEncode
                                            );
                                            break;
                                    }
                                }//fin:else de if(oJsonRespWS.lError)

                            }//fin:if (bAppKeyHabilitada)
                            else
                            {
                                //Error, falta de permiso de la AppKey
                                sLinealog = sIdProceso + sErrorMetodo;
                                Utilerias.guardarLogEventos(sLinealog);

                                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_WSCONSULTAR_REF_03);
                                oJsonResponse["iError"] = oMsgError.iError;
                                oJsonResponse["cError"] = oMsgError.cError;
                                oMsgError = null;
                            }//fin:else if (bAppKeyHabilitada) 

                        }//fin:if(bPrefijo == true)
                        else
                        {
                            //Error en el formato de la referencia o prefijo no permitido
                            sLinealog = sIdProceso + "La línea de referencia [" + sParamLineaRef + "]";
                            sLinealog += " no esta habilitada para pago";
                            Utilerias.guardarLogEventos(sLinealog);

                            oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_11);
                            oJsonResponse["iError"] = oMsgError.iError;
                            oJsonResponse["cError"] = oMsgError.cError;

                            oMsgError = null;
                        }//fin:else de if(bPrefijo == true)

                    }//fin:if (Regex.IsMatch(sParamLineaRef, "^[0-9]*$"))
                    else
                    {
                        //Error en el formato de la referencia o prefijo no permitido
                        sLinealog = sIdProceso + "La línea de referencia [" + sParamLineaRef + "]";
                        sLinealog += " no tiene un formato correcto o no es valido";
                        Utilerias.guardarLogEventos(sLinealog);

                        oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_WSCONSULTAR_REF_01);
                        oJsonResponse["iError"] = oMsgError.iError;
                        oJsonResponse["cError"] = oMsgError.cError;
                        oMsgError = null;
                    }//fin:else  
                                                             
                }//fin:try
                catch (Exception oEx)
                {
                    sLinealog = sIdProceso + "Error en [ConsultarRecibos]: ";
                    sLinealog += "Message^" + oEx.Message;
                    sLinealog += "~Source^" + oEx.Source;
                    sLinealog += "~Target^" + oEx.TargetSite.ToString();
                    sLinealog += "~StackTrace^" + oEx.StackTrace;
                    Utilerias.guardarLogEventos(sLinealog);

                    ////Obtener la configuracion global del reporte a mostrar
                    //sFileNameConfigMsg = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");

                    //oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_99);
                    oJsonResponse["iError"] = 1;
                    oJsonResponse["cError"] = "ERROR al invocar el servicio para consultar recibos: " + oEx.Message;
                    oMsgError = null;
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    //Obtener la direccion ipcliente
                    if (Request.Properties.ContainsKey("MS_HttpContext"))
                    {
                        oHttpContextW = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                        if (oHttpContextW != null)
                        {
                            sIpAddress = oHttpContextW.Request.UserHostAddress;
                        }//fin:if
                    }//fin:if                
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    //Guardar registro de bitacora 
                    if (oDaoDatos != null) oDaoDatos = null;
                    oDaoDatos = new DaoConsultas();
                    oBitConsulta = new DTOBitacora();
                    oBitConsulta.sFolio = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                    oBitConsulta.sEvento = "EXCEPCION en [consultar/recibos]";
                    oBitConsulta.sObservacion =
                    "[ERROR]{CONSULTAR_RECIBOS} Ocurrió una excepcion en la ejecución del servicio.";
                    oBitConsulta.sHttpUserAgent = sUserAgentHttp;
                    oBitConsulta.sIPCliente = sIpAddress;
                    oBitConsulta.sInputParams = string.Empty;
                    _iIdBitacora = oDaoDatos.GuardarBitacora(oBitConsulta, new List<DTODetalleBitacora>());
                    oBitConsulta = null;
                    oDaoDatos = null;
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                   
                    FuncionesWeb.enviarEmailNotificacion(
                        "ERROR al invocar el servicio para consultar recibos.",
                        "ERROR, ocurrió una excepción al ejecutar el servicio [/consultar/recibos].",
                        sLinealog 
                    );
                }//fin: catch
            }else{

                //Obtener la configuracion global del reporte a mostrar
                sFileNameConfigMsg = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");
                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_99);
                oJsonResponse["iError"] = oMsgError.iError;
                oJsonResponse["cError"] = oMsgError.cError;
            }
            return Ok(oJsonResponse);

        }//fin:ConsultarReferenciaVU
         //---------------------------------------------------------------------
         //---------------------------------------------------------------------
        [HttpPost]
        [ActionName("recibos")]
        public IHttpActionResult ConsultarRecibos([FromBody]JObject _PostParams)
        {

            int _iIdBitacora = 0;
            bool bAppKeyHabilitada = false;
            string sUrlRecibo = string.Empty;
            bool bPdf = true;
            bool bUnDocumento = false;
            JArray jListAuxiliar = new JArray();
            string sIdProceso = string.Empty;
            bool bUrlEncode = true;
            bool bPrefijo = false;
            string urlBase = string.Empty;
            string stAppAmbiente = string.Empty;
            string sBase64HojaPDF = string.Empty;
            string sFileNameConfig = string.Empty;
            string file = string.Empty;
            string sLinealog = string.Empty;
            string KeyConfig = string.Empty;
            string sErrorMetodo = string.Empty;
            string sRutaTempPDF = string.Empty;
            string sServerPath = string.Empty;
            string sFileNameConfigMsg = string.Empty;
            string sParamLineaRef = string.Empty;
            string sParamAppKey = string.Empty;
            Dictionary<string, string> oDictConfig = null;
            string[] sPrefijos = new string[] { };
            JArray aJsonTramites = null;

            string sUserAgentHttp = string.Empty;
            string sIpAddress = "0.0.0.0";

            StringBuilder sStringBuildAux = null;

            TPostResponse oMsgError = null;
            TJsonRespuesta oJsonRespWS = null;
            HttpContextWrapper oHttpContextW = null;

            DTOBitacora oBitConsulta = null;
            DTOConfiguracion stPermitidosR = null;
            DaoConsultas oDaoDatos = null;
            DTOConfiguracion oConfig = null;
            HttpResponseMessage oHttpMsgError = null;

            JObject oJsonResponse = new JObject();

            oJsonResponse["iError"] = 0;
            oJsonResponse["cError"] = "";


            if (_PostParams == null)
            {
                //TODO : Complementar con mensajes para ser explicito el error               
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_EMPTY);
                throw new HttpResponseException(oHttpMsgError);
            }//fin:iif

            if (_PostParams["cAppKey"] == null || _PostParams["cLineaReferencia"] == null)
            {
                //TODO : Complementar con mensajes para ser explicito el error
                //throw new HttpResponseException(HttpStatusCode.BadRequest);
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_NO_REQUERIDOS);
                throw new HttpResponseException(oHttpMsgError);
            }//fin:iif

            //DAP 20200820, parámetro para identificar si el PDF Base64 se aplica el UrlEncode            
            bUrlEncode = true;
            if (_PostParams["lUrlEncode"] != null)
            {
                if (!string.IsNullOrEmpty(_PostParams.Value<string>("lUrlEncode").Trim()))
                {
                    bUrlEncode = Convert.ToBoolean(_PostParams.Value<string>("lUrlEncode").Trim().ToLower());
                }//fin:if                
            }//fin:else
            else { bUrlEncode = true; }

            //Parámetro para identificar si se generan los base 64           
            bPdf = true;
            if (_PostParams["lPdf"] != null)
            {
                if (!string.IsNullOrEmpty(_PostParams.Value<string>("lPdf").Trim()))
                {
                    bPdf = Convert.ToBoolean(_PostParams.Value<string>("lPdf").Trim().ToLower());
                }//fin:if                
            }//fin:else
            else { bPdf = true; }

            //Parámetro para identificar si se generan un solo PDF     
            bUnDocumento = false;
            if (_PostParams["lUnDocumento"] != null)
            {
                if (!string.IsNullOrEmpty(_PostParams.Value<string>("lUnDocumento").Trim()))
                {
                    bUnDocumento = Convert.ToBoolean(_PostParams.Value<string>("lUnDocumento").Trim().ToLower());
                }//fin:if                
            }//fin:else
            else { bUnDocumento = false; }

            //Obtener la configuracion global
            sFileNameConfig = System.Web.Hosting.HostingEnvironment.MapPath("~/config/config_reportes.json");
            sServerPath = System.Web.Hosting.HostingEnvironment.MapPath("~");

            sErrorMetodo = string.Empty;
            oDictConfig = Utilerias.obtenerConfigReporte(sFileNameConfig, string.Empty, ref sErrorMetodo);

            if (string.IsNullOrEmpty(sErrorMetodo))
            {

                sErrorMetodo = string.Empty;
                sRutaTempPDF = Path.Combine(sServerPath, oDictConfig["ruta_filesystem_pdf"]);

                try
                {

                    //Obtener la direccion ipcliente
                    if (Request.Properties.ContainsKey("MS_HttpContext"))
                    {
                        oHttpContextW = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                        if (oHttpContextW != null)
                        {
                            sIpAddress = oHttpContextW.Request.UserHostAddress;
                        }//fin:if
                    }//fin:if

                    // Get user agent.
                    if (Request.Headers.Contains("User-Agent"))
                    {
                        var headers = Request.Headers.GetValues("User-Agent");
                        sStringBuildAux = new StringBuilder();
                        foreach (var header in headers)
                        {
                            sStringBuildAux.Append(header);
                            sStringBuildAux.Append(" ");
                        }//fin:foreach
                        sUserAgentHttp = sStringBuildAux.ToString().Trim();
                        sStringBuildAux = null;
                    }//fin:if

                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    //Obtener variables de configuracion
                    oDaoDatos = new DaoConsultas();

                    if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null)
                    {
                        stAppAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                    }//fin:if    

                    switch (stAppAmbiente)
                    {
                        case Constantes._DESARROLLO:
                            KeyConfig = Constantes.DES_URL_RECIBO;
                            break;
                        case Constantes._PRODUCCION:
                        case Constantes._TEST_PRODUCCION:
                            KeyConfig = Constantes.PROD_URL_RECIBO;
                            break;
                    }//fin:switch (stAppAmbiente)

                    oConfig = oDaoDatos.ObtenerValorConfig(KeyConfig);

                    //Obtener prefijos permitidos
                    stPermitidosR = oDaoDatos.ObtenerValorConfig(Constantes.VUE_PREFIJOS_PERMITIDOS);

                    sPrefijos = stPermitidosR.cValor.Split('@');

                    sFileNameConfigMsg = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");

                    sParamAppKey = HttpUtility.UrlDecode(_PostParams.Value<string>("cAppKey").Trim());
                    sParamLineaRef = HttpUtility.UrlDecode(_PostParams.Value<string>("cLineaReferencia").Trim());

                    //Validar prefijos permitidos
                    foreach (var sPrefijo in sPrefijos)
                    {
                        if (sPrefijo == sParamLineaRef.Substring(0, 3))
                        {
                            bPrefijo = true;
                            break;
                        }
                    }

                    //validar que el parametro referencia solo contenga numeros                              
                    sIdProceso = "[" + DateTime.Now.ToString("yyyyMMdd.HHmmss.fff") + "]";
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    //Guardar registro de bitacora 
                    oBitConsulta = new DTOBitacora();
                    oBitConsulta.sFolio = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                    oBitConsulta.sEvento = "POST^" + Request.RequestUri.ToString();
                    oBitConsulta.sObservacion =
                        "[INFO]{CONSULTAR_RECIBOS}Inicio de petición información para los RECIBOS:" + sParamLineaRef;
                    oBitConsulta.sHttpUserAgent = sUserAgentHttp;
                    oBitConsulta.sIPCliente = sIpAddress;
                    oBitConsulta.sInputParams = _PostParams.ToString(Formatting.None);
                    _iIdBitacora = oDaoDatos.GuardarBitacora(oBitConsulta, new List<DTODetalleBitacora>());
                    oBitConsulta = null;
                    oDaoDatos = null;
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                
                    if (Regex.IsMatch(sParamLineaRef, "^[0-9]*$"))
                    {
                        if (bPrefijo == true)
                        {
                            bAppKeyHabilitada = FuncionesWeb.comprobarAppKeyHabilitada(sParamAppKey, ref sErrorMetodo);
                            if (!string.IsNullOrEmpty(sErrorMetodo)) Utilerias.guardarLogEventos(sErrorMetodo);

                            //AppKey Habilitada para hacer uso de los servicos para VUE
                            if (bAppKeyHabilitada)
                            {
                                urlBase = oConfig.cValor;
                                //Invocar servicio para obtener los tramites
                                oJsonRespWS = FuncionesWeb.obtenerRecibos(
                                    sParamLineaRef
                                );

                                if (oJsonRespWS.lError)
                                {

                                    sLinealog = sIdProceso + "[" + sParamLineaRef + "]";
                                    sLinealog += oJsonRespWS.iError.ToString() + "|" + oJsonRespWS.cError;
                                    Utilerias.guardarLogEventos(sLinealog);

                                    oJsonResponse["iError"] = oJsonRespWS.iError;
                                    oJsonResponse["cError"] = oJsonRespWS.cError;

                                    FuncionesWeb.enviarEmailNotificacion(
                                        "ERROR al invocar el servicio para obtener los tramites[/consultar/recibos].",
                                        "ERROR al invocar el servicio para obtener los tramites",
                                        sLinealog
                                    );
                                    //FIN:RESPUESTA ERROR EN SERVICIO
                                }//fin: if (oJsonResp.lError)
                                else
                                {
                                    aJsonTramites = new JArray();
                                    aJsonTramites = JArray.Parse(oJsonRespWS.oJsonData.SelectToken("aRecibos").ToString());
                                    oJsonResponse = FuncionesWeb.RespRecibos(
                                       aJsonTramites, urlBase, bPdf, bUrlEncode, bUnDocumento
                                    );

                                    //oJsonResponse["arecibos"] = JToken.FromObject(jListAuxiliar);
                                }//fin:else de if(oJsonRespWS.lError)

                            }//fin:if (bAppKeyHabilitada)
                            else
                            {
                                //Error, falta de permiso de la AppKey
                                sLinealog = sIdProceso + sErrorMetodo;
                                Utilerias.guardarLogEventos(sLinealog);

                                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_WSCONSULTAR_REF_03);
                                oJsonResponse["iError"] = oMsgError.iError;
                                oJsonResponse["cError"] = oMsgError.cError;
                                oMsgError = null;
                            }//fin:else if (bAppKeyHabilitada) 

                        }//fin:if(bPrefijo == true)
                        else
                        {
                            //Error en el formato de la referencia o prefijo no permitido
                            sLinealog = sIdProceso + "La línea de referencia [" + sParamLineaRef + "]";
                            sLinealog += " no esta habilitada para pago";
                            Utilerias.guardarLogEventos(sLinealog);

                            oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_11);
                            oJsonResponse["iError"] = oMsgError.iError;
                            oJsonResponse["cError"] = oMsgError.cError;

                            oMsgError = null;
                        }//fin:else de if(bPrefijo == true)

                    }//fin:if (Regex.IsMatch(sParamLineaRef, "^[0-9]*$"))
                    else
                    {
                        //Error en el formato de la referencia o prefijo no permitido
                        sLinealog = sIdProceso + "La línea de referencia [" + sParamLineaRef + "]";
                        sLinealog += " no tiene un formato correcto o no es valido";
                        Utilerias.guardarLogEventos(sLinealog);

                        oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_WSCONSULTAR_REF_01);
                        oJsonResponse["iError"] = oMsgError.iError;
                        oJsonResponse["cError"] = oMsgError.cError;
                        oMsgError = null;
                    }//fin:else  

                }//fin:try
                catch (Exception oEx)
                {
                    sLinealog = sIdProceso + "Error en [ConsultarRecibos]: ";
                    sLinealog += "Message^" + oEx.Message;
                    sLinealog += "~Source^" + oEx.Source;
                    sLinealog += "~Target^" + oEx.TargetSite.ToString();
                    sLinealog += "~StackTrace^" + oEx.StackTrace;
                    Utilerias.guardarLogEventos(sLinealog);

                    ////Obtener la configuracion global del reporte a mostrar
                    //sFileNameConfigMsg = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");

                    //oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_99);
                    oJsonResponse["iError"] = 1;
                    oJsonResponse["cError"] = "ERROR al invocar el servicio para consultar recibos: " + oEx.Message;
                    oMsgError = null;
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    //Obtener la direccion ipcliente
                    if (Request.Properties.ContainsKey("MS_HttpContext"))
                    {
                        oHttpContextW = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                        if (oHttpContextW != null)
                        {
                            sIpAddress = oHttpContextW.Request.UserHostAddress;
                        }//fin:if
                    }//fin:if                
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    //Guardar registro de bitacora 
                    if (oDaoDatos != null) oDaoDatos = null;
                    oDaoDatos = new DaoConsultas();
                    oBitConsulta = new DTOBitacora();
                    oBitConsulta.sFolio = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                    oBitConsulta.sEvento = "EXCEPCION en [consultar/recibos]";
                    oBitConsulta.sObservacion =
                    "[ERROR]{CONSULTAR_RECIBOS} Ocurrió una excepcion en la ejecución del servicio.";
                    oBitConsulta.sHttpUserAgent = sUserAgentHttp;
                    oBitConsulta.sIPCliente = sIpAddress;
                    oBitConsulta.sInputParams = string.Empty;
                    _iIdBitacora = oDaoDatos.GuardarBitacora(oBitConsulta, new List<DTODetalleBitacora>());
                    oBitConsulta = null;
                    oDaoDatos = null;
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                   
                    FuncionesWeb.enviarEmailNotificacion(
                        "ERROR al invocar el servicio para consultar recibos.",
                        "ERROR, ocurrió una excepción al ejecutar el servicio [/consultar/recibos].",
                        sLinealog
                    );
                }//fin: catch
            }
            else
            {

                //Obtener la configuracion global del reporte a mostrar
                sFileNameConfigMsg = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");
                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_99);
                oJsonResponse["iError"] = oMsgError.iError;
                oJsonResponse["cError"] = oMsgError.cError;
            }
            return Ok(oJsonResponse);

        }//fin:ConsultarReferenciaVU
        //---------------------------------------------------------------------
        //---------------------------------------------------------------------
        [HttpPost]
        [ActionName("detalle-referencia")]
        public IHttpActionResult ConsultarDetalleReferencia([FromBody]JObject _PostParams)
        {

            int _iIdBitacora = 0;
            bool bAppKeyHabilitada = false;
            bool bPrefijo = false;
            string sIdProceso = string.Empty;
            string sLinealog = string.Empty;
            string sErrorMetodo = string.Empty;

            string sFileNameConfigMsg = string.Empty;
            string sParamLineaRef = string.Empty;
            string sParamAppKey = string.Empty;
            string[] sPrefijos = new string[] { };
            string sKeyMensaje = string.Empty;

            string sUserAgentHttp = string.Empty;
            string sIpAddress = "0.0.0.0";

            StringBuilder sStringBuildAux = null;

            TPostResponse oMsgError = null;
            TJsonRespuesta oJsonRespWS = null;
            HttpContextWrapper oHttpContextW = null;
            DTOConfiguracion stPermitidosR = null;

            DTOBitacora oBitConsulta = null;
            DaoConsultas oDaoDatos = null;
            HttpResponseMessage oHttpMsgError = null;

            JObject oJsonResponse = new JObject();

            oJsonResponse["iError"] = false;
            oJsonResponse["cError"] = "";

            if (_PostParams == null)
            {
                //TODO : Complementar con mensajes para ser explicito el error               
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_EMPTY);
                throw new HttpResponseException(oHttpMsgError);
            }//fin:iif

            if (_PostParams["cAppKey"] == null || _PostParams["cLineaReferencia"] == null)
            {
                //TODO : Complementar con mensajes para ser explicito el error
                //throw new HttpResponseException(HttpStatusCode.BadRequest);
                oHttpMsgError = new HttpResponseMessage(HttpStatusCode.BadRequest);
                oHttpMsgError.Content = new StringContent(Constantes.ERROR_POST_PARAMS_NO_REQUERIDOS);
                throw new HttpResponseException(oHttpMsgError);
            }//fin:iif

            try
            {

                //Obtener la direccion ipcliente
                if (Request.Properties.ContainsKey("MS_HttpContext"))
                {
                    oHttpContextW = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                    if (oHttpContextW != null)
                    {
                        sIpAddress = oHttpContextW.Request.UserHostAddress;
                    }//fin:if
                }//fin:if

                // Get user agent.
                if (Request.Headers.Contains("User-Agent"))
                {
                    var headers = Request.Headers.GetValues("User-Agent");
                    sStringBuildAux = new StringBuilder();
                    foreach (var header in headers)
                    {
                        sStringBuildAux.Append(header);
                        sStringBuildAux.Append(" ");
                    }//fin:foreach
                    sUserAgentHttp = sStringBuildAux.ToString().Trim();
                    sStringBuildAux = null;
                }//fin:if

                oDaoDatos = new DaoConsultas();


                sParamAppKey = HttpUtility.UrlDecode(_PostParams.Value<string>("cAppKey").Trim());
                sParamLineaRef = HttpUtility.UrlDecode(_PostParams.Value<string>("cLineaReferencia").Trim());

                //Obtener prefijos permitidos
                stPermitidosR = oDaoDatos.ObtenerValorConfig(Constantes.VUE_PREFIJOS_PERMITIDOS);

                sPrefijos = stPermitidosR.cValor.Split('@');

                //Validar prefijos permitidos
                foreach (var sPrefijo in sPrefijos)
                {
                    if (sPrefijo == sParamLineaRef.Substring(0, 3))
                    {
                        bPrefijo = true;
                        break;
                    }
                }

                sFileNameConfigMsg = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");


                //validar que el parametro referencia solo contenga numeros                              
                sIdProceso = "[" + DateTime.Now.ToString("yyyyMMdd.HHmmss.fff") + "]";
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //Guardar la Bitacora
                oBitConsulta = new DTOBitacora();
                oBitConsulta.sFolio = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                oBitConsulta.sEvento = "POST^" + Request.RequestUri.ToString();
                oBitConsulta.sObservacion =
                    "[INFO]{CONSULTAR_DETALLEREFERENCIA}Inicio de petición información para la referencia:" + sParamLineaRef;
                oBitConsulta.sHttpUserAgent = sUserAgentHttp;
                oBitConsulta.sIPCliente = sIpAddress;
                oBitConsulta.sInputParams = _PostParams.ToString(Formatting.None);
                _iIdBitacora = oDaoDatos.GuardarBitacora(oBitConsulta, new List<DTODetalleBitacora>());
                sLinealog = oBitConsulta.sObservacion;
                Utilerias.guardarLogEventos(sLinealog);
                oBitConsulta = null;
                oDaoDatos = null;
                sLinealog = "";
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                
                if (Regex.IsMatch(sParamLineaRef, "^[0-9]*$"))
                {
                    if (bPrefijo == true)
                    {
                        bAppKeyHabilitada = FuncionesWeb.comprobarAppKeyHabilitada(sParamAppKey, ref sErrorMetodo);
                        if (!string.IsNullOrEmpty(sErrorMetodo)) Utilerias.guardarLogEventos(sErrorMetodo);

                        //AppKey Habilitada para hacer uso de los servicos para VUE
                        if (bAppKeyHabilitada)
                        {
                            
                            //Invocar servicio para obtener los detalles de la Referencia 
                            oJsonRespWS = FuncionesWeb.obtenerDetalleReferencia(
                                sParamLineaRef, sParamAppKey
                            );

                            if (oJsonRespWS.lError)
                            {
                                switch (oJsonRespWS.iError)
                                {
                                    case 101:   //Error que se produce por el vencimiento de linea de referencia
                                        sKeyMensaje = Constantes.KEY_LR_VENCIDA;
                                        break;
                                    case 1:
                                    case 21: //No se pudo localizar la Solicitud del Trámite
                                        sKeyMensaje = Constantes.KEY_LR_NOENCONTRADA;
                                        break;

                                    default:
                                        //98 => La información de la referencia {0} no esta disponible por el momento.
                                        sKeyMensaje = string.Empty;
                                        break;
                                }//fin:switch (oJsonRespWS.iError)


                                if (!String.IsNullOrEmpty(sKeyMensaje))
                                {
                                    oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, sKeyMensaje);
                                    oJsonRespWS.iError = oMsgError.iError;
                                    oJsonRespWS.cError = oMsgError.cError;
                                }
                                else
                                {
                                    FuncionesWeb.enviarEmailNotificacion(
                                        "ERROR al invocar el servicio para obtener Detalle de Referencias[/consultar/detalle-referencia].",
                                        "ERROR al invocar el servicio para consultar una línea de referencia.",
                                        oJsonRespWS.cError
                                    );
                                }

                                sLinealog = sIdProceso + "[" + sParamLineaRef + "]";
                                sLinealog += oJsonRespWS.iError.ToString() + "|" + oJsonRespWS.cError;
                                Utilerias.guardarLogEventos(sLinealog);


                                oJsonResponse["iError"] = oJsonRespWS.iError;
                                oJsonResponse["cError"] = oJsonRespWS.cError;

                                
                                //FIN:RESPUESTA ERROR EN SERVICIO
                            }//fin: if (oJsonResp.lError)
                            else
                            {
                                //Eliminamos lo que no necesitamos
                                oJsonRespWS.oJsonData.Remove("lError");
                                oJsonRespWS.oJsonData.Remove("cMensaje");
                                oJsonRespWS.oJsonData.Remove("aPagados");

                                //Enviar la respuesta
                                oJsonResponse = oJsonRespWS.oJsonData;

                            }//fin:else
                            oJsonRespWS.oJsonData = null;
                            oJsonRespWS = null;
                        }//fin:if (bAppKeyHabilitada)
                        else
                        {
                            //Error, falta de permiso de la AppKey
                            sLinealog = sIdProceso + sErrorMetodo;
                            Utilerias.guardarLogEventos(sLinealog);

                            oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_WSCONSULTAR_REF_03);
                            oJsonResponse["iError"] = oMsgError.iError;
                            oJsonResponse["cError"] = oMsgError.cError;
                            oMsgError = null;
                        }//fin:else if (bAppKeyHabilitada) 

                    }//fin:if if (bPrefijo == true)
                    else
                    {
                        //Error en el formato de la referencia o prefijo no permitido
                        sLinealog = sIdProceso + "La línea de referencia [" + sParamLineaRef + "]";
                        sLinealog += " no esta habilitada para consulta";
                        Utilerias.guardarLogEventos(sLinealog);

                        oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_10);
                        oJsonResponse["iError"] = oMsgError.iError;
                        oJsonResponse["cError"] = oMsgError.cError;
                        oMsgError = null;
                    }//fin:else de if(bPrefijo == true)

                }//fin:if (Regex.IsMatch(sParamLineaRef, "^[0-9]*$"))
                else
                {
                    //Error en el formato de la referencia
                    sLinealog = sIdProceso + "La línea de referencia [" + sParamLineaRef + "]";
                    sLinealog += " no tiene un formato correcto.";
                    Utilerias.guardarLogEventos(sLinealog);

                    oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_WSCONSULTAR_REF_01);
                    oJsonResponse["iError"] = oMsgError.iError;
                    oJsonResponse["cError"] = oMsgError.cError;
                    oMsgError = null;
                }//fin:else                                           
            }//fin:try
            catch (Exception oEx)
            {
                sLinealog = sIdProceso + "Error en [ConsultarDetalleReferencia]: ";
                sLinealog += "Message^" + oEx.Message;
                sLinealog += "~Source^" + oEx.Source;
                sLinealog += "~Target^" + oEx.TargetSite.ToString();
                sLinealog += "~StackTrace^" + oEx.StackTrace;
                Utilerias.guardarLogEventos(sLinealog);

                //Obtener la configuracion global del reporte a mostrar
                sFileNameConfigMsg = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");

                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_99);
                oJsonResponse["iError"] = oMsgError.iError;
                oJsonResponse["cError"] = oMsgError.cError;
                oMsgError = null;
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //Obtener la direccion ipcliente
                if (Request.Properties.ContainsKey("MS_HttpContext"))
                {
                    oHttpContextW = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                    if (oHttpContextW != null)
                    {
                        sIpAddress = oHttpContextW.Request.UserHostAddress;
                    }//fin:if
                }//fin:if                
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //Guardar registro de bitacora 
                if (oDaoDatos != null) oDaoDatos = null;
                oDaoDatos = new DaoConsultas();
                oBitConsulta = new DTOBitacora();
                oBitConsulta.sFolio = sIdProceso.Replace("[", string.Empty).Replace("]", string.Empty);
                oBitConsulta.sEvento = "EXCEPCION en [consultar/detalle-referencia]";
                oBitConsulta.sObservacion =
                   "[ERROR]{CONSULTAR_DETALLEREFERENCIA} Ocurrió una excepcion en la ejecución del servicio.";
                oBitConsulta.sHttpUserAgent = sUserAgentHttp;
                oBitConsulta.sIPCliente = sIpAddress;
                oBitConsulta.sInputParams = string.Empty;
                _iIdBitacora = oDaoDatos.GuardarBitacora(oBitConsulta, new List<DTODetalleBitacora>());
                oBitConsulta = null;
                oDaoDatos = null;
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                   
                FuncionesWeb.enviarEmailNotificacion(
                    "ERROR al invocar el servicio para consultar referencia.",
                    "ERROR, ocurrió una excepción al ejecutar el servicio [/consultar/detalle-referencia].",
                    sLinealog
                );
            }//fin: catch

            return Ok(oJsonResponse);

        }//fin:ConsultarReferenciaVU
         //---------------------------------------------------------------------
         //---------------------------------------------------------------------
    }//fin:class
}
