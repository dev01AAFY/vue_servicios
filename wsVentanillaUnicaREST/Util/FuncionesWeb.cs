﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.IO;
using Newtonsoft.Json;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json.Linq;
using wsVentanillaUnicaREST.dao;
using wsVentanillaUnicaREST.Models;
using System.Net.Mail;

namespace wsVentanillaUnicaREST.Util {
    public class FuncionesWeb {
        //---------------------------------------------------------------------
        public static TPostResponse validarAppKeyAAFY(string _sAppKey) {

            string stMsgError = string.Empty;
            TPostResponse oPResp = new TPostResponse();

            //int _iIdBitacora = 0;
            int iTimeOut = Constantes.TIMEOUT_DEFAULT;
            string stAmbiente = Constantes._DESARROLLO;
            string cJsonResponse = string.Empty;
            string stURLWS = string.Empty;
            string stWsOpcion = string.Empty;
            string stErrorApp = string.Empty;
            string stDataPost = string.Empty;

            JObject oJsonResp = null;
            HttpWebRequest oHttpResquest = null;           
            DaoConsultas oDaoDatos = null;
            List<string> listKeyConfig = null;
            List<DTOConfiguracion> listConfiguraciones = null;
            //List<DTODetalleBitacora> listDetalleBit = null;
            NameValueCollection ParamsQueryString = HttpUtility.ParseQueryString(String.Empty);

            try {
                oDaoDatos = new DaoConsultas();
                ConfigurationManager.RefreshSection("appSettings");
                if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null) {
                    stAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                }//fin:if  
                
                listKeyConfig = new List<string>();
                //listKeyConfig.Add(Constantes.WS_OPCION_VALIDAR_APPKEY);
                listKeyConfig.Add(Constantes.WTIMEOUT_REQUEST);
                switch (stAmbiente) {
                    case Constantes._DESARROLLO: listKeyConfig.Add(Constantes.DES_URL_GENERICOPAGOS); break;
                    case Constantes._PRODUCCION: case Constantes._TEST_PRODUCCION:
                        listKeyConfig.Add(Constantes.PROD_URL_GENERICOPAGOS);
                    break;
                }//fin:switch

                listConfiguraciones = oDaoDatos.ObtenerConfiguraciones(listKeyConfig);

                IEnumerable<DTOConfiguracion> oElements = listConfiguraciones.Where(x => x.lError == false);
                if (oElements.Count() != listKeyConfig.Count) {
                    stErrorApp = "[ValidarAppKeyAAFY] No fue posible obtener los parametros de configuración:";
                    stErrorApp += string.Join(",", listKeyConfig);
                    oElements = null;
                    listKeyConfig = null;
                    oDaoDatos = null;
                    throw new Exception(stErrorApp);
                }//fin:if
                listKeyConfig.Clear(); listKeyConfig = null;

                DTOConfiguracion oElement = null;
       
                oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.WTIMEOUT_REQUEST);
                if (oElement != null) iTimeOut = Convert.ToInt32(oElement.cValor.Trim());

                switch (stAmbiente) {
                    case Constantes._DESARROLLO:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.DES_URL_GENERICOPAGOS);
                        if (oElement != null) stURLWS = oElement.cValor;
                        break;
                    case Constantes._PRODUCCION:
                    case Constantes._TEST_PRODUCCION:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.PROD_URL_GENERICOPAGOS);
                        if (oElement != null) stURLWS = oElement.cValor;
                    break;
                }//fin:switch (stAmbiente)
                listConfiguraciones.Clear(); listConfiguraciones = null;

                stWsOpcion = Constantes.WS_OPCION_VALIDAR_APPKEY;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //listDetalleBit = new List<DTODetalleBitacora>();

                ParamsQueryString.Clear();
                ParamsQueryString.Add("cOpcion", stWsOpcion);
                ParamsQueryString.Add("cAppKey", _sAppKey);
                ParamsQueryString.Add("cModulo", Constantes.APPKEY_VALIDA_MODULO);

                //Guardar bitacora para la aplicacion del Pago
                //foreach (string sKey in ParamsQueryString) {
                //    listDetalleBit.Add(Utilerias.CrearDetalleBitacora(0, sKey, ParamsQueryString[sKey], Constantes.DET_BIT_INPUT));
                //}//fin:foreach (string key in ParamsQueryString)

                //DTOBitacora oBitacora = this.ObtenerInfoBitacora();
                //oBitacora.cFolio = "0";
                //oBitacora.cEvento = "POST^" + stURLWS.Trim();
                //oBitacora.cObservacion = Constantes.MOD_PRINCIPAL;
                //oBitacora.cObservacion += "POST para validar el AppKey para los servicios de Caja Única.";

                //_iIdBitacora = oDaoMotor.GuardarBitacora(oBitacora, listDetalleBit);
                //listDetalleBit.Clear();
                //oBitacora = null;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                stDataPost = ParamsQueryString.ToString();

                //TODO: Para obtener certificado SSL
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                oHttpResquest = (HttpWebRequest)WebRequest.Create(stURLWS.Trim());
                oHttpResquest.Method = "POST";
                oHttpResquest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                oHttpResquest.Timeout = iTimeOut;

                byte[] PostData = Encoding.UTF8.GetBytes(stDataPost);

                oHttpResquest.ContentLength = PostData.Length;
                using (Stream requestStream = oHttpResquest.GetRequestStream()) {
                    requestStream.Write(PostData, 0, PostData.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }//fin:using                

                try {
                    using (HttpWebResponse oHttpResp = (HttpWebResponse)oHttpResquest.GetResponse()) {
                        switch (oHttpResp.StatusCode) {
                            case HttpStatusCode.OK: {
                                oPResp.lError = false;
                                using (StreamReader oReader 
                                    = new StreamReader(oHttpResp.GetResponseStream(), Encoding.GetEncoding("iso-8859-1"))) {
                                    cJsonResponse = oReader.ReadToEnd();
                                    //listDetalleBit.Add(Utilerias.CrearDetalleBitacora(_iIdBitacora, "JSON", cJsonResponse, Constantes.DET_BIT_OUTPUT));

                                    //Obtener los atributos del AppKey
                                    try {
                                        oJsonResp = JObject.Parse(cJsonResponse);
                                        if (oJsonResp["lError"] != null) {
                                            if ((bool)oJsonResp["lError"] == false) {
                                                oPResp.lError = false;
                                                oPResp.cError = "";
                                                oPResp.cMensaje = oJsonResp.Value<string>("cIdSistema");
                                            }//fin:if
                                            else {
                                                stErrorApp = oJsonResp.Value<string>("cError");
                                                oPResp.lError = true;
                                                oPResp.cError = stErrorApp;
                                            }//fin:else
                                        }//fin:if (oJsonResp["lError"] != null)
                                        else {
                                            stErrorApp = "El servicio no contiene atributos válidos para la consultar.";
                                            oPResp.lError = true;
                                            oPResp.cError = stErrorApp;
                                        }//fin:else
                                    }//fin:try
                                    catch (JsonReaderException ex) {
                                        stErrorApp = "El servicio no devolvio una respuesta valida:[" + ex.Message + "]";
                                        oPResp.lError = true;
                                        oPResp.cError = stErrorApp;
                                    }
                                }//fin:using
                            }//fin: case HttpStatusCode.OK
                            break;
                            default: {
                                stErrorApp = "HttpStatusCode=>" + oHttpResp.StatusCode.ToString();
                                oPResp.lError = true;
                                oPResp.cError = stErrorApp;
                                //listDetalleBit.Add(
                                //    Utilerias.CrearDetalleBitacora(
                                //        _iIdBitacora, "ERROR en ValidarAppKeyAAFY", "HttpStatusCode^" + oHttpResp.StatusCode.ToString(), Constantes.DET_BIT_OUTPUT
                                //    )
                                //);
                            }//fin:default
                            break;
                        }//fin:switch (oHttpResponse.StatusCode)
                    }//fin:using
                }//fin:try
                catch (WebException excep) {
                    string responseContent = "";
                    stErrorApp = excep.Message;

                    WebResponse errResp = excep.Response;
                    using (Stream respStream = errResp.GetResponseStream()) {
                        StreamReader reader = new StreamReader(respStream);
                        responseContent = reader.ReadToEnd();
                    }//fin:using (Stream respStream = errResp.GetResponseStream())

                    stErrorApp += "|" + responseContent;
                    oPResp.cError = stErrorApp;
                    oPResp.lError = true;
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    //listDetalleBit.Add(
                    //    Utilerias.CrearDetalleBitacora(_iIdBitacora, "WEB EXCEPCION en ValidarAppKeyAAFY", stErrorApp, Constantes.DET_BIT_OUTPUT)
                    //);
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++                    
                }//Fin:catch 
                //if (oDaoMotor == null) oDaoMotor = new DatosMotorPagos();
                //if (listDetalleBit != null) {
                //    oDaoMotor.GuardarDetalleBitacora(listDetalleBit);
                //}//fin:if               
                //listDetalleBit = null;
                oDaoDatos = null;
            }//fin:try
            catch (Exception oExc) {
                oPResp.cError = oExc.Message;
                oPResp.lError = true;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
                /*
                if (_iIdBitacora > 0) {
                    if (listDetalleBit == null) listDetalleBit = new List<DTODetalleBitacora>();
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(_iIdBitacora, "EXCEPCION.Message en ValidarAppKeyAAFY", oExc.Message, Constantes.DET_BIT_OUTPUT)
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(_iIdBitacora, "EXCEPCION.Source en ValidarAppKeyAAFY", oExc.Source, Constantes.DET_BIT_OUTPUT)
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(_iIdBitacora, "EXCEPCION.Target en ValidarAppKeyAAFY", oExc.TargetSite.ToString(), Constantes.DET_BIT_OUTPUT)
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(_iIdBitacora, "EXCEPCION.StackTrace en GenerarLineaReferencia", oExc.StackTrace, Constantes.DET_BIT_OUTPUT)
                    );
                    if (oDaoMotor == null) oDaoMotor = new DatosMotorPagos();
                    oDaoMotor.GuardarDetalleBitacora(listDetalleBit);
                    listDetalleBit.Clear(); listDetalleBit = null;
                    oDaoMotor = null;
                }//fin:if (_iIdBitacora > 0)
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                */
                if (oDaoDatos != null) oDaoDatos = null;                
            }//fin:catch

            return oPResp;
        }//fin:ValidarAppKeyAAFY
        //---------------------------------------------------------------------
        public static TJsonRespuesta obtenerDetalleReferenciaWS(string _LineaRef, string _sFolioExecute) {

            int _iIdBitacora = 0;
            int iTimeOut = 0;
            string cJsonResponse = string.Empty;
            string stAppAmbiente = string.Empty;
            string stURLWS = string.Empty;
            string stOpcionRecibos = string.Empty;
            string stFechaVigencia = string.Empty;
            string stFormatFloat = string.Empty;
           

            string _cError = string.Empty;
            string _cData = string.Empty;

            HttpWebRequest oHttpResquest = null;

            NameValueCollection ParamsQueryString = HttpUtility.ParseQueryString(String.Empty);            
            DaoConsultas oDaoDatos = null;
            List<string> listKeyConfig = null;
            List<DTOConfiguracion> listConfiguraciones = null;
            DTOBitacora oBitacora = null;
            List<DTODetalleBitacora> listDetalleBit = null;

            TJsonRespuesta oJsonPost = new TJsonRespuesta();

            try {
                oDaoDatos = new DaoConsultas();
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Obtener variables de configuracion
                ConfigurationManager.RefreshSection("appSettings");

                listKeyConfig = new List<string>();

                if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null) {
                    stAppAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                }//fin:if               

                //listKeyConfig.Add(Constantes.WSOPCION_OBTENER_RECIBOS);
                listKeyConfig.Add(Constantes.WTIMEOUT_REQUEST);
                switch (stAppAmbiente) {
                    case Constantes._DESARROLLO:
                        listKeyConfig.Add(Constantes.DES_URL_RECIBOSPAGOSWEB);                       
                    break;
                    case Constantes._PRODUCCION: case Constantes._TEST_PRODUCCION:
                        listKeyConfig.Add(Constantes.PROD_URL_RECIBOSPAGOSWEB);                       
                    break;
                }//fin:switch (stAppAmbiente)

                listConfiguraciones = oDaoDatos.ObtenerConfiguraciones(listKeyConfig);
                IEnumerable<DTOConfiguracion> oElements = listConfiguraciones.Where(x => x.lError == false);
                if (oElements.Count() != listKeyConfig.Count) {
                    _cError = "[obtenerDetalleReferenciaWS] No fue posible obtener los parámetros de configuración:";
                    _cError += string.Join(",", listKeyConfig);
                    oElements = null;
                    listKeyConfig = null;
                    oDaoDatos = null;
                    throw new Exception(_cError);
                }//fin:if
                listKeyConfig = null;

                DTOConfiguracion oElement = null;
                              
                oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.WTIMEOUT_REQUEST);
                if (oElement != null) iTimeOut = Convert.ToInt32(oElement.cValor.Trim());

                switch (stAppAmbiente) {
                    case Constantes._DESARROLLO:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.DES_URL_RECIBOSPAGOSWEB);
                        if (oElement != null) stURLWS = oElement.cValor;                                           
                    break;
                    case Constantes._PRODUCCION: case Constantes._TEST_PRODUCCION:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.PROD_URL_RECIBOSPAGOSWEB);
                        if (oElement != null) stURLWS = oElement.cValor;                       
                    break;
                }//fin:switch (stAmbiente) 
                listConfiguraciones = null;

                stOpcionRecibos = Constantes.WS_OP_OBTENER_DET_REFERENCIA;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                ParamsQueryString.Clear();
                ParamsQueryString.Add("cOpcion", stOpcionRecibos);
                ParamsQueryString.Add("cLineaReferencia", _LineaRef);
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Guardar bitacora de la peticion de pago                
                listDetalleBit = new List<DTODetalleBitacora>();
                foreach (string sKey in ParamsQueryString) {
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(0, sKey, ParamsQueryString[sKey], Constantes.DET_BIT_INPUT)
                    );
                }//fin:foreach (string key in ParamsQueryString)                           

                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oBitacora = new DTOBitacora();
                oBitacora.sFolio = _sFolioExecute;
                oBitacora.sEvento = "POST^" + stURLWS.Trim();
                oBitacora.sObservacion = "[obtenerDetalleReferenciaWS]";
                oBitacora.sObservacion += "POST al servicio(webspeed)" + stOpcionRecibos + ".";
                oBitacora.sInputParams = string.Empty;
                oBitacora.sIPCliente = "0.0.0.0";
                _iIdBitacora = oDaoDatos.GuardarBitacora(oBitacora, listDetalleBit);
                listDetalleBit.Clear();
                oBitacora = null;
                oDaoDatos = null;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                _cData = ParamsQueryString.ToString();

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                oHttpResquest = (HttpWebRequest)WebRequest.Create(stURLWS.Trim());
                oHttpResquest.Method = "POST";
                oHttpResquest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                oHttpResquest.Timeout = iTimeOut;
                byte[] PostData = Encoding.UTF8.GetBytes(_cData);

                oHttpResquest.ContentLength = PostData.Length;

                using (Stream requestStream = oHttpResquest.GetRequestStream()) {
                    requestStream.Write(PostData, 0, PostData.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }//fin:using                

                try {
                    using (HttpWebResponse oHttpResp = (HttpWebResponse)oHttpResquest.GetResponse()) {
                        switch (oHttpResp.StatusCode) {
                            case HttpStatusCode.OK: {
                                oJsonPost.lError = false;
                                using (StreamReader oReader = new StreamReader(oHttpResp.GetResponseStream(), Encoding.GetEncoding("iso-8859-1"))) {
                                    cJsonResponse = oReader.ReadToEnd();

                                    listDetalleBit.Add(
                                        Utilerias.CrearDetalleBitacora(_iIdBitacora, "JSON", cJsonResponse, Constantes.DET_BIT_OUTPUT)
                                    );

                                    try {
                                        oJsonPost.oJsonData = JObject.Parse(cJsonResponse);
                                        if (oJsonPost.oJsonData["iError"] != null && oJsonPost.oJsonData["cError"] != null) { 
                                            //&& oJsonPost.oJsonData["cLineaRef"] != null && oJsonPost.oJsonData["iEstatus"] != null &&
                                            //oJsonPost.oJsonData["cEstatus"] != null && oJsonPost.oJsonData["dMontoTotal"] != null &&
                                            //oJsonPost.oJsonData["dtVigencia"] != null && oJsonPost.oJsonData["aDetalles"] != null) 

                                            oJsonPost.iError = oJsonPost.oJsonData.Value<int>("iError");
                                            oJsonPost.lError = (oJsonPost.iError > 0 ? true : false);
                                            oJsonPost.cError = oJsonPost.oJsonData.Value<string>("cError");
                                            if (oJsonPost.oJsonData["cMensaje"] != null) {
                                                oJsonPost.cMensaje = oJsonPost.oJsonData.Value<string>("cMensaje");
                                            }//fin:if 
                                        }//fin:fin
                                        else {
                                            _cError = "El servicio no retorno una estructura de datos correcta(JSON inválido).";
                                            oJsonPost.lError = true;
                                            oJsonPost.cError = _cError;
                                        }//fin:else                                                                                        
                                    }//fin:if
                                    catch (JsonReaderException ex) {
                                        _cError = "El servicio no devolvio una respuesta valida:[" + ex.Message + "]";
                                        oJsonPost.lError = true;
                                        oJsonPost.cError = _cError;
                                    }
                                }//fin:using
                            }//fin:case
                            break;
                            default: {
                                _cError = "HttpStatusCode=>" + oHttpResp.StatusCode.ToString();
                                oJsonPost.lError = true;
                                oJsonPost.cError = _cError;

                                listDetalleBit.Add(
                                    Utilerias.CrearDetalleBitacora(
                                        _iIdBitacora, "ERROR", "HttpStatusCode^" + oHttpResp.StatusCode.ToString(), Constantes.DET_BIT_OUTPUT
                                    )
                                );
                            }//fin:default
                            break;
                        }//fin:switch (oHttpResponse.StatusCode)
                    }//Fin:Using
                }//fin:try
                catch (WebException excep) {
                    string responseContent = "";
                    _cError = excep.Message;

                    WebResponse errResp = excep.Response;
                    using (Stream respStream = errResp.GetResponseStream()) {
                        StreamReader reader = new StreamReader(respStream);
                        responseContent = reader.ReadToEnd();
                    }//fin:using (Stream respStream = errResp.GetResponseStream())

                    _cError += "|" + responseContent;
                    oJsonPost.cError = _cError;
                    oJsonPost.lError = true;

                    listDetalleBit.Add(
                       Utilerias.CrearDetalleBitacora(_iIdBitacora, "ERROR[WEB EXCEPCION]", _cError, Constantes.DET_BIT_OUTPUT)
                    );
                }//Fin:catch

                //Guardar bitacora con la respuesta de la peticion
                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                listDetalleBit.Clear(); listDetalleBit = null;
                oDaoDatos = null;

            }//fin:try
            catch (Exception ex) {
                _cError += " | " + ex.Message;
                oJsonPost.cError = _cError;
                oJsonPost.lError = true;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
                if (_iIdBitacora > 0) {
                    if (listDetalleBit == null) listDetalleBit = new List<DTODetalleBitacora>();
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Message en [ObtenerDetalleReferencia]",
                             ex.Message,
                             Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Source en [ObtenerDetalleReferencia]",
                            ex.Source,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Target en en [ObtenerDetalleReferencia]",
                            ex.TargetSite.ToString(),
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].StackTrace en [ObtenerDetalleReferencia]",
                            ex.StackTrace,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                    oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                    listDetalleBit.Clear(); listDetalleBit = null;
                    oDaoDatos = null;
                }//fin:if (_iIdBitacora > 0)
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                if (oDaoDatos != null) oDaoDatos = null;
            }//fin:catch
            return oJsonPost;
        }//fin:if ObtenerDetalleReferencia
        //-----------------------------------------------------------------------------------------
        public static TJsonRespuesta obtenerTramites(string _LineaRef)
        {

            int _iIdBitacora = 0;
            int iTimeOut = 0;
            string cJsonResponse = string.Empty;
            string stAppAmbiente = string.Empty;
            string stURLWS = string.Empty;
            string stOpcionRecibos = string.Empty;


            string _cError = string.Empty;
            string _cData = string.Empty;

            HttpWebRequest oHttpResquest = null;

            NameValueCollection ParamsQueryString = HttpUtility.ParseQueryString(String.Empty);
            DaoConsultas oDaoDatos = null;
            List<string> listKeyConfig = null;
            List<DTOConfiguracion> listConfiguraciones = null;
            DTOBitacora oBitacora = null;
            List<DTODetalleBitacora> listDetalleBit = null;

            TJsonRespuesta oJsonPost = new TJsonRespuesta();

            try
            {
                oDaoDatos = new DaoConsultas();
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Obtener variables de configuracion
                ConfigurationManager.RefreshSection("appSettings");

                listKeyConfig = new List<string>();

                if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null)
                {
                    stAppAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                }//fin:if               

                //listKeyConfig.Add(Constantes.WSOPCION_OBTENER_RECIBOS);
                listKeyConfig.Add(Constantes.WTIMEOUT_REQUEST);
                switch (stAppAmbiente)
                {
                    case Constantes._DESARROLLO:
                        listKeyConfig.Add(Constantes.DES_URL_RECIBOSPAGOSWEB);
                        break;
                    case Constantes._PRODUCCION:
                    case Constantes._TEST_PRODUCCION:
                        listKeyConfig.Add(Constantes.PROD_URL_RECIBOSPAGOSWEB);
                        break;
                }//fin:switch (stAppAmbiente)

                listConfiguraciones = oDaoDatos.ObtenerConfiguraciones(listKeyConfig);
                IEnumerable<DTOConfiguracion> oElements = listConfiguraciones.Where(x => x.lError == false);
                if (oElements.Count() != listKeyConfig.Count)
                {
                    _cError = "[obtenerTramites] No fue posible obtener los parámetros de configuración:";
                    _cError += string.Join(",", listKeyConfig);
                    oElements = null;
                    listKeyConfig = null;
                    oDaoDatos = null;
                    throw new Exception(_cError);
                }//fin:if
                listKeyConfig = null;

                DTOConfiguracion oElement = null;

                oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.WTIMEOUT_REQUEST);
                if (oElement != null) iTimeOut = Convert.ToInt32(oElement.cValor.Trim());

                switch (stAppAmbiente)
                {
                    case Constantes._DESARROLLO:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.DES_URL_RECIBOSPAGOSWEB);
                        if (oElement != null) stURLWS = oElement.cValor;
                        break;
                    case Constantes._PRODUCCION:
                    case Constantes._TEST_PRODUCCION:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.PROD_URL_RECIBOSPAGOSWEB);
                        if (oElement != null) stURLWS = oElement.cValor;
                        break;
                }//fin:switch (stAmbiente) 
                listConfiguraciones = null;

                stOpcionRecibos = "obtener-tramites";
                
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                ParamsQueryString.Clear();
                ParamsQueryString.Add("cOpcion", stOpcionRecibos);
                ParamsQueryString.Add("cLineaReferencia", _LineaRef);
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Guardar bitacora de la peticion de Traer Tramites               
                listDetalleBit = new List<DTODetalleBitacora>();
                foreach (string sKey in ParamsQueryString)
                {
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(0, sKey, ParamsQueryString[sKey], Constantes.DET_BIT_INPUT)
                    );
                }//fin:foreach (string key in ParamsQueryString)                           

                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oBitacora = new DTOBitacora();
                oBitacora.sEvento = "POST^" + stURLWS.Trim();
                oBitacora.sObservacion = "[obtenerTramites]";
                oBitacora.sObservacion += "POST al servicio(webspeed)" + stOpcionRecibos + ".";
                oBitacora.sInputParams = string.Empty;
                oBitacora.sIPCliente = "0.0.0.0";
                _iIdBitacora = oDaoDatos.GuardarBitacora(oBitacora, listDetalleBit);
                listDetalleBit.Clear();
                oBitacora = null;
                oDaoDatos = null;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                _cData = ParamsQueryString.ToString();

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                oHttpResquest = (HttpWebRequest)WebRequest.Create(stURLWS.Trim());
                oHttpResquest.Method = "POST";
                oHttpResquest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                oHttpResquest.Timeout = iTimeOut;
                byte[] PostData = Encoding.UTF8.GetBytes(_cData);

                oHttpResquest.ContentLength = PostData.Length;

                using (Stream requestStream = oHttpResquest.GetRequestStream())
                {
                    requestStream.Write(PostData, 0, PostData.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }//fin:using                

                try
                {
                    using (HttpWebResponse oHttpResp = (HttpWebResponse)oHttpResquest.GetResponse())
                    {
                        switch (oHttpResp.StatusCode)
                        {
                            case HttpStatusCode.OK:
                                {
                                    oJsonPost.lError = false;
                                    using (StreamReader oReader = new StreamReader(oHttpResp.GetResponseStream(), Encoding.GetEncoding("iso-8859-1")))
                                    {
                                        cJsonResponse = oReader.ReadToEnd();

                                        listDetalleBit.Add(
                                            Utilerias.CrearDetalleBitacora(_iIdBitacora, "JSON", cJsonResponse, Constantes.DET_BIT_OUTPUT)
                                        );

                                        try
                                        {
                                            oJsonPost.oJsonData = JObject.Parse(cJsonResponse);
                                            if (oJsonPost.oJsonData["iError"] != null && oJsonPost.oJsonData["cError"] != null)
                                            {
                                                oJsonPost.iError = oJsonPost.oJsonData.Value<int>("iError");
                                                oJsonPost.lError = (oJsonPost.iError > 0 ? true : false);
                                                oJsonPost.cError = oJsonPost.oJsonData.Value<string>("cError");
                                          
                                            }//fin:fin
                                            else
                                            {
                                                _cError = "El servicio no retorno una estructura de datos correcta(JSON inválido).";
                                                oJsonPost.lError = true;
                                                oJsonPost.cError = _cError;
                                            }//fin:else                                                                                        
                                        }//fin:if
                                        catch (JsonReaderException ex)
                                        {
                                            _cError = "El servicio no devolvio una respuesta valida:[" + ex.Message + "]";
                                            oJsonPost.lError = true;
                                            oJsonPost.cError = _cError;
                                        }
                                    }//fin:using
                                }//fin:case
                                break;
                            default:
                                {
                                    _cError = "HttpStatusCode=>" + oHttpResp.StatusCode.ToString();
                                    oJsonPost.lError = true;
                                    oJsonPost.cError = _cError;

                                    listDetalleBit.Add(
                                        Utilerias.CrearDetalleBitacora(
                                            _iIdBitacora, "ERROR", "HttpStatusCode^" + oHttpResp.StatusCode.ToString(), Constantes.DET_BIT_OUTPUT
                                        )
                                    );
                                }//fin:default
                                break;
                        }//fin:switch (oHttpResponse.StatusCode)
                    }//Fin:Using
                }//fin:try
                catch (WebException excep)
                {
                    string responseContent = "";
                    _cError = excep.Message;

                    WebResponse errResp = excep.Response;
                    using (Stream respStream = errResp.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(respStream);
                        responseContent = reader.ReadToEnd();
                    }//fin:using (Stream respStream = errResp.GetResponseStream())

                    _cError += "|" + responseContent;
                    oJsonPost.cError = _cError;
                    oJsonPost.lError = true;

                    listDetalleBit.Add(
                       Utilerias.CrearDetalleBitacora(_iIdBitacora, "ERROR[WEB EXCEPCION]", _cError, Constantes.DET_BIT_OUTPUT)
                    );
                }//Fin:catch

                //Guardar bitacora con la respuesta de la peticion
                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                listDetalleBit.Clear(); listDetalleBit = null;
                oDaoDatos = null;

            }//fin:try
            catch (Exception ex)
            {
                _cError += " | " + ex.Message;
                oJsonPost.cError = _cError;
                oJsonPost.lError = true;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
                if (_iIdBitacora > 0)
                {
                    if (listDetalleBit == null) listDetalleBit = new List<DTODetalleBitacora>();
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Message en [ObtenerTramites]",
                             ex.Message,
                             Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Source en [ObtenerTramites]",
                            ex.Source,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Target en en [ObtenerTramites]",
                            ex.TargetSite.ToString(),
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].StackTrace en [ObtenerTramites]",
                            ex.StackTrace,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                    oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                    listDetalleBit.Clear(); listDetalleBit = null;
                    oDaoDatos = null;
                }//fin:if (_iIdBitacora > 0)
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                if (oDaoDatos != null) oDaoDatos = null;
            }//fin:catch
            return oJsonPost;
        }//fin:ObtenerTramites
         //----------------------------------------------------------------------------------------
        public static TJsonRespuesta obtenerRecibos(string _LineaRef)
        {

            int _iIdBitacora = 0;
            int iTimeOut = 0;
            string cJsonResponse = string.Empty;
            string stAppAmbiente = string.Empty;
            string stURLWS = string.Empty;
            string stOpcionRecibos = string.Empty;


            string _cError = string.Empty;
            string _cData = string.Empty;

            HttpWebRequest oHttpResquest = null;

            NameValueCollection ParamsQueryString = HttpUtility.ParseQueryString(String.Empty);
            DaoConsultas oDaoDatos = null;
            List<string> listKeyConfig = null;
            List<DTOConfiguracion> listConfiguraciones = null;
            DTOBitacora oBitacora = null;
            List<DTODetalleBitacora> listDetalleBit = null;

            TJsonRespuesta oJsonPost = new TJsonRespuesta();

            try
            {
                oDaoDatos = new DaoConsultas();
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Obtener variables de configuracion
                ConfigurationManager.RefreshSection("appSettings");

                listKeyConfig = new List<string>();

                if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null)
                {
                    stAppAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                }//fin:if               

                //listKeyConfig.Add(Constantes.WSOPCION_OBTENER_RECIBOS);
                listKeyConfig.Add(Constantes.WTIMEOUT_REQUEST);
                switch (stAppAmbiente)
                {
                    case Constantes._DESARROLLO:
                        listKeyConfig.Add(Constantes.DES_URL_RECIBOSPAGOSWEB);
                        break;
                    case Constantes._PRODUCCION:
                    case Constantes._TEST_PRODUCCION:
                        listKeyConfig.Add(Constantes.PROD_URL_RECIBOSPAGOSWEB);
                        break;
                }//fin:switch (stAppAmbiente)

                listConfiguraciones = oDaoDatos.ObtenerConfiguraciones(listKeyConfig);
                IEnumerable<DTOConfiguracion> oElements = listConfiguraciones.Where(x => x.lError == false);
                if (oElements.Count() != listKeyConfig.Count)
                {
                    _cError = "[obtenerRecibos] No fue posible obtener los parámetros de configuración:";
                    _cError += string.Join(",", listKeyConfig);
                    oElements = null;
                    listKeyConfig = null;
                    oDaoDatos = null;
                    throw new Exception(_cError);
                }//fin:if
                listKeyConfig = null;

                DTOConfiguracion oElement = null;

                oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.WTIMEOUT_REQUEST);
                if (oElement != null) iTimeOut = Convert.ToInt32(oElement.cValor.Trim());

                switch (stAppAmbiente)
                {
                    case Constantes._DESARROLLO:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.DES_URL_RECIBOSPAGOSWEB);
                        if (oElement != null) stURLWS = oElement.cValor;
                        break;
                    case Constantes._PRODUCCION:
                    case Constantes._TEST_PRODUCCION:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.PROD_URL_RECIBOSPAGOSWEB);
                        if (oElement != null) stURLWS = oElement.cValor;
                        break;
                }//fin:switch (stAmbiente) 
                listConfiguraciones = null;

                stOpcionRecibos = "obtenerRecibos";

                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                ParamsQueryString.Clear();
                ParamsQueryString.Add("cOpcion", stOpcionRecibos);
                ParamsQueryString.Add("cLineaReferencia", _LineaRef);
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Guardar bitacora de la peticion Obtener Recibos                
                listDetalleBit = new List<DTODetalleBitacora>();
                foreach (string sKey in ParamsQueryString)
                {
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(0, sKey, ParamsQueryString[sKey], Constantes.DET_BIT_INPUT)
                    );
                }//fin:foreach (string key in ParamsQueryString)                           

                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oBitacora = new DTOBitacora();
                oBitacora.sEvento = "POST^" + stURLWS.Trim();
                oBitacora.sObservacion = "[obtenerRecibos]";
                oBitacora.sObservacion += "POST al servicio(webspeed)" + stOpcionRecibos + ".";
                oBitacora.sInputParams = string.Empty;
                oBitacora.sIPCliente = "0.0.0.0";
                _iIdBitacora = oDaoDatos.GuardarBitacora(oBitacora, listDetalleBit);
                listDetalleBit.Clear();
                oBitacora = null;
                oDaoDatos = null;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                _cData = ParamsQueryString.ToString();

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                oHttpResquest = (HttpWebRequest)WebRequest.Create(stURLWS.Trim());
                oHttpResquest.Method = "POST";
                oHttpResquest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                oHttpResquest.Timeout = iTimeOut;
                byte[] PostData = Encoding.UTF8.GetBytes(_cData);

                oHttpResquest.ContentLength = PostData.Length;

                using (Stream requestStream = oHttpResquest.GetRequestStream())
                {
                    requestStream.Write(PostData, 0, PostData.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }//fin:using                

                try
                {
                    using (HttpWebResponse oHttpResp = (HttpWebResponse)oHttpResquest.GetResponse())
                    {
                        switch (oHttpResp.StatusCode)
                        {
                            case HttpStatusCode.OK:
                                {
                                    oJsonPost.lError = false;
                                    using (StreamReader oReader = new StreamReader(oHttpResp.GetResponseStream(), Encoding.GetEncoding("iso-8859-1")))
                                    {
                                        cJsonResponse = oReader.ReadToEnd();

                                        listDetalleBit.Add(
                                            Utilerias.CrearDetalleBitacora(_iIdBitacora, "JSON", cJsonResponse, Constantes.DET_BIT_OUTPUT)
                                        );

                                        try
                                        {
                                            oJsonPost.oJsonData = JObject.Parse(cJsonResponse);
                                            if (oJsonPost.oJsonData["iError"] != null && oJsonPost.oJsonData["cError"] != null)
                                            {
                                                oJsonPost.iError = oJsonPost.oJsonData.Value<int>("iError");
                                                oJsonPost.lError = (oJsonPost.iError > 0 ? true : false);
                                                oJsonPost.cError = oJsonPost.oJsonData.Value<string>("cError");

                                            }//fin:fin
                                            else
                                            {
                                                _cError = "El servicio no retorno una estructura de datos correcta(JSON inválido).";
                                                oJsonPost.lError = true;
                                                oJsonPost.cError = _cError;
                                            }//fin:else                                                                                        
                                        }//fin:if
                                        catch (JsonReaderException ex)
                                        {
                                            _cError = "El servicio no devolvio una respuesta valida:[" + ex.Message + "]";
                                            oJsonPost.lError = true;
                                            oJsonPost.cError = _cError;
                                        }
                                    }//fin:using
                                }//fin:case
                                break;
                            default:
                                {
                                    _cError = "HttpStatusCode=>" + oHttpResp.StatusCode.ToString();
                                    oJsonPost.lError = true;
                                    oJsonPost.cError = _cError;

                                    listDetalleBit.Add(
                                        Utilerias.CrearDetalleBitacora(
                                            _iIdBitacora, "ERROR", "HttpStatusCode^" + oHttpResp.StatusCode.ToString(), Constantes.DET_BIT_OUTPUT
                                        )
                                    );
                                }//fin:default
                                break;
                        }//fin:switch (oHttpResponse.StatusCode)
                    }//Fin:Using
                }//fin:try
                catch (WebException excep)
                {
                    string responseContent = "";
                    _cError = excep.Message;

                    WebResponse errResp = excep.Response;
                    using (Stream respStream = errResp.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(respStream);
                        responseContent = reader.ReadToEnd();
                    }//fin:using (Stream respStream = errResp.GetResponseStream())

                    _cError += "|" + responseContent;
                    oJsonPost.cError = _cError;
                    oJsonPost.lError = true;

                    listDetalleBit.Add(
                       Utilerias.CrearDetalleBitacora(_iIdBitacora, "ERROR[WEB EXCEPCION]", _cError, Constantes.DET_BIT_OUTPUT)
                    );
                }//Fin:catch

                //Guardar bitacora con la respuesta de la peticion
                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                listDetalleBit.Clear(); listDetalleBit = null;
                oDaoDatos = null;

            }//fin:try
            catch (Exception ex)
            {
                _cError += " | " + ex.Message;
                oJsonPost.cError = _cError;
                oJsonPost.lError = true;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
                if (_iIdBitacora > 0)
                {
                    if (listDetalleBit == null) listDetalleBit = new List<DTODetalleBitacora>();
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Message en [obtenerRecibos]",
                             ex.Message,
                             Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Source en [obtenerRecibos]",
                            ex.Source,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Target en en [obtenerRecibos]",
                            ex.TargetSite.ToString(),
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].StackTrace en [obtenerRecibos]",
                            ex.StackTrace,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                    oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                    listDetalleBit.Clear(); listDetalleBit = null;
                    oDaoDatos = null;
                }//fin:if (_iIdBitacora > 0)
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                if (oDaoDatos != null) oDaoDatos = null;
            }//fin:catch
            return oJsonPost;
        }//fin:ObtenerRecibos
         //----------------------------------------------------------------------------------------
        public static TJsonRespuesta reciboJsonPDF(string _LineaRef)
        {

            int _iIdBitacora = 0;
            int iTimeOut = 0;
            string cJsonResponse = string.Empty;
            string stAppAmbiente = string.Empty;
            string stURLWS = string.Empty;
            string stOpcionRecibos = string.Empty;
            string sValueContentType = string.Empty;

            string _cError = string.Empty;
            string _cData = string.Empty;

            HttpWebRequest oHttpResquest = null;

            NameValueCollection ParamsQueryString = HttpUtility.ParseQueryString(String.Empty);
            DaoConsultas oDaoDatos = null;
            List<string> listKeyConfig = null;
            List<DTOConfiguracion> listConfiguraciones = null;
            DTOBitacora oBitacora = null;
            List<DTODetalleBitacora> listDetalleBit = null;

            TJsonRespuesta oJsonPost = new TJsonRespuesta();

            try
            {
                oDaoDatos = new DaoConsultas();
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Obtener variables de configuracion
                ConfigurationManager.RefreshSection("appSettings");

                listKeyConfig = new List<string>();

                if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null)
                {
                    stAppAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                }//fin:if               

                //listKeyConfig.Add(Constantes.WSOPCION_OBTENER_RECIBOS);
                listKeyConfig.Add(Constantes.WTIMEOUT_REQUEST);
                switch (stAppAmbiente)
                {
                    case Constantes._DESARROLLO:
                        listKeyConfig.Add(Constantes.DES_URL_DATOS_RECIBO);
                        break;
                    case Constantes._PRODUCCION:
                    case Constantes._TEST_PRODUCCION:
                        listKeyConfig.Add(Constantes.PROD_URL_DATOS_RECIBO);
                        break;
                }//fin:switch (stAppAmbiente)

                listConfiguraciones = oDaoDatos.ObtenerConfiguraciones(listKeyConfig);
                IEnumerable<DTOConfiguracion> oElements = listConfiguraciones.Where(x => x.lError == false);
                if (oElements.Count() != listKeyConfig.Count)
                {
                    _cError = "[reciboJsonPDF] No fue posible obtener los parámetros de configuración:";
                    _cError += string.Join(",", listKeyConfig);
                    oElements = null;
                    listKeyConfig = null;
                    oDaoDatos = null;
                    throw new Exception(_cError);
                }//fin:if
                listKeyConfig = null;

                DTOConfiguracion oElement = null;

                oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.WTIMEOUT_REQUEST);
                if (oElement != null) iTimeOut = Convert.ToInt32(oElement.cValor.Trim());

                switch (stAppAmbiente)
                {
                    case Constantes._DESARROLLO:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.DES_URL_DATOS_RECIBO);
                        if (oElement != null) stURLWS = oElement.cValor;
                        break;
                    case Constantes._PRODUCCION:
                    case Constantes._TEST_PRODUCCION:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.PROD_URL_DATOS_RECIBO);
                        if (oElement != null) stURLWS = oElement.cValor;
                        break;
                }//fin:switch (stAmbiente) 
                listConfiguraciones = null;

                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                ParamsQueryString.Clear();
                ParamsQueryString.Add("iID-frm", "0");
                ParamsQueryString.Add("cRef-Frm", _LineaRef);
                ParamsQueryString.Add("output", "JSON");
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Guardar bitacora de la peticion Obtener Recibos                
                listDetalleBit = new List<DTODetalleBitacora>();
                foreach (string sKey in ParamsQueryString)
                {
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(0, sKey, ParamsQueryString[sKey], Constantes.DET_BIT_INPUT)
                    );
                }//fin:foreach (string key in ParamsQueryString)                           

                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oBitacora = new DTOBitacora();
                oBitacora.sEvento = "POST^" + stURLWS.Trim();
                oBitacora.sObservacion = "[reciboJsonPDF]";
                oBitacora.sObservacion += "POST al servicio(webspeed) GENERAR JSON RECIBO PDF";
                oBitacora.sInputParams = string.Empty;
                oBitacora.sIPCliente = "0.0.0.0";
                _iIdBitacora = oDaoDatos.GuardarBitacora(oBitacora, listDetalleBit);
                listDetalleBit.Clear();
                oBitacora = null;
                oDaoDatos = null;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                _cData = ParamsQueryString.ToString();

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                oHttpResquest = (HttpWebRequest)WebRequest.Create(stURLWS.Trim());
                oHttpResquest.Method = "POST";
                oHttpResquest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                oHttpResquest.Timeout = iTimeOut;
                byte[] PostData = Encoding.UTF8.GetBytes(_cData);

                oHttpResquest.ContentLength = PostData.Length;

                using (Stream requestStream = oHttpResquest.GetRequestStream())
                {
                    requestStream.Write(PostData, 0, PostData.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }//fin:using                

                try
                {
                    using (HttpWebResponse oHttpResp = (HttpWebResponse)oHttpResquest.GetResponse())
                    {
                        switch (oHttpResp.StatusCode)
                        {
                            case HttpStatusCode.OK:
                                {
                                    for (int iIdx = 0; iIdx < oHttpResp.Headers.Count; iIdx++)
                                    {
                                        if (oHttpResp.Headers.Keys[iIdx].Trim() == Constantes.HEADER_CONTENT_TYPE)
                                        {
                                            sValueContentType = (oHttpResp.Headers[Constantes.HEADER_CONTENT_TYPE]).Replace("\"", string.Empty);
                                            iIdx = oHttpResp.Headers.Count;
                                        }//fin:if                                         
                                    }//fin:for

                                    if (sValueContentType.ToLower().StartsWith(Constantes.MIME_TYPE_JSON))
                                    {
                                            oJsonPost.lError = false;
                                            using (StreamReader oReader = new StreamReader(oHttpResp.GetResponseStream(), Encoding.GetEncoding("iso-8859-1")))
                                            {
                                                cJsonResponse = oReader.ReadToEnd();
                                                listDetalleBit.Add(
                                                    Utilerias.CrearDetalleBitacora(_iIdBitacora, "JSON", cJsonResponse, Constantes.DET_BIT_OUTPUT)
                                                );

                                                try
                                                {
                                                    oJsonPost.oJsonData = JObject.Parse(cJsonResponse);
                                                    oJsonPost.iError = 0;
                                                    oJsonPost.lError = false;
                                                }//fin:if
                                                catch (JsonReaderException ex)
                                                {
                                                    _cError = "El servicio no devolvio una respuesta valida:[" + ex.Message + "]";
                                                    oJsonPost.lError = true;
                                                    oJsonPost.cError = _cError;
                                                }
                                            }//fin:using
                                    }
                                    else
                                    {
                                        _cError = "El servicio no devolvio una respuesta valida";
                                        oJsonPost.lError = true;
                                        oJsonPost.cError = _cError;
                                    }
                                }//fin:case
                                break;
                            default:
                                {
                                    _cError = "HttpStatusCode=>" + oHttpResp.StatusCode.ToString();
                                    oJsonPost.lError = true;
                                    oJsonPost.cError = _cError;

                                    listDetalleBit.Add(
                                        Utilerias.CrearDetalleBitacora(
                                            _iIdBitacora, "ERROR", "HttpStatusCode^" + oHttpResp.StatusCode.ToString(), Constantes.DET_BIT_OUTPUT
                                        )
                                    );
                                }//fin:default
                                break;
                        }//fin:switch (oHttpResponse.StatusCode)
                    }//Fin:Using
                }//fin:try
                catch (WebException excep)
                {
                    string responseContent = "";
                    _cError = excep.Message;

                    WebResponse errResp = excep.Response;
                    using (Stream respStream = errResp.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(respStream);
                        responseContent = reader.ReadToEnd();
                    }//fin:using (Stream respStream = errResp.GetResponseStream())

                    _cError += "|" + responseContent;
                    oJsonPost.cError = _cError;
                    oJsonPost.lError = true;

                    listDetalleBit.Add(
                       Utilerias.CrearDetalleBitacora(_iIdBitacora, "ERROR[WEB EXCEPCION]", _cError, Constantes.DET_BIT_OUTPUT)
                    );
                }//Fin:catch

                //Guardar bitacora con la respuesta de la peticion
                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                listDetalleBit.Clear(); listDetalleBit = null;
                oDaoDatos = null;

            }//fin:try
            catch (Exception ex)
            {
                _cError += " | " + ex.Message;
                oJsonPost.cError = _cError;
                oJsonPost.lError = true;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
                if (_iIdBitacora > 0)
                {
                    if (listDetalleBit == null) listDetalleBit = new List<DTODetalleBitacora>();
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Message en [reciboJsonPDF]",
                             ex.Message,
                             Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Source en [reciboJsonPDF]",
                            ex.Source,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Target en en [reciboJsonPDF]",
                            ex.TargetSite.ToString(),
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].StackTrace en [reciboJsonPDF]",
                            ex.StackTrace,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                    oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                    listDetalleBit.Clear(); listDetalleBit = null;
                    oDaoDatos = null;
                }//fin:if (_iIdBitacora > 0)
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                if (oDaoDatos != null) oDaoDatos = null;
            }//fin:catch
            return oJsonPost;
        }//fin:ObtenerTramites
        //-----------------------------------------------------------------------------------------
        public static TJsonRespuesta obtenerPDF(string _jsonRecibo)
        {

            int _iIdBitacora = 0;
            int iTimeOut = 0;
            string cJsonResponse = string.Empty;
            string stAppAmbiente = string.Empty;
            string stURLWS = string.Empty;
            string stOpcionRecibos = string.Empty;
            string sValueContentType = string.Empty;

            string _cError = string.Empty;
            string _cData = string.Empty;

            HttpWebRequest oHttpResquest = null;

            NameValueCollection ParamsQueryString = HttpUtility.ParseQueryString(String.Empty);
            DaoConsultas oDaoDatos = null;
            List<string> listKeyConfig = null;
            List<DTOConfiguracion> listConfiguraciones = null;
            DTOBitacora oBitacora = null;
            List<DTODetalleBitacora> listDetalleBit = null;

            TJsonRespuesta oJsonPost = new TJsonRespuesta();

            try
            {
                oDaoDatos = new DaoConsultas();
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Obtener variables de configuracion
                ConfigurationManager.RefreshSection("appSettings");

                listKeyConfig = new List<string>();

                if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null)
                {
                    stAppAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                }//fin:if               

                //listKeyConfig.Add(Constantes.WSOPCION_OBTENER_RECIBOS);
                listKeyConfig.Add(Constantes.WTIMEOUT_REQUEST);
                switch (stAppAmbiente)
                {
                    case Constantes._DESARROLLO:
                        listKeyConfig.Add(Constantes.DES_URL_PDF_RECIBO);
                        break;
                    case Constantes._PRODUCCION:
                    case Constantes._TEST_PRODUCCION:
                        listKeyConfig.Add(Constantes.PROD_URL_PDF_RECIBO);
                        break;
                }//fin:switch (stAppAmbiente)

                listConfiguraciones = oDaoDatos.ObtenerConfiguraciones(listKeyConfig);
                IEnumerable<DTOConfiguracion> oElements = listConfiguraciones.Where(x => x.lError == false);
                if (oElements.Count() != listKeyConfig.Count)
                {
                    _cError = "[obtenerRecibos] No fue posible obtener los parámetros de configuración:";
                    _cError += string.Join(",", listKeyConfig);
                    oElements = null;
                    listKeyConfig = null;
                    oDaoDatos = null;
                    throw new Exception(_cError);
                }//fin:if
                listKeyConfig = null;

                DTOConfiguracion oElement = null;

                oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.WTIMEOUT_REQUEST);
                if (oElement != null) iTimeOut = Convert.ToInt32(oElement.cValor.Trim());

                switch (stAppAmbiente)
                {
                    case Constantes._DESARROLLO:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.DES_URL_PDF_RECIBO);
                        if (oElement != null) stURLWS = oElement.cValor;
                        break;
                    case Constantes._PRODUCCION:
                    case Constantes._TEST_PRODUCCION:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.PROD_URL_PDF_RECIBO);
                        if (oElement != null) stURLWS = oElement.cValor;
                        break;
                }//fin:switch (stAmbiente) 
                listConfiguraciones = null;


                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Guardar bitacora de la peticion Obtener Recibos                
                listDetalleBit = new List<DTODetalleBitacora>();
                foreach (string sKey in ParamsQueryString)
                {
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(0, sKey, ParamsQueryString[sKey], Constantes.DET_BIT_INPUT)
                    );
                }//fin:foreach (string key in ParamsQueryString)                           

                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oBitacora = new DTOBitacora();
                oBitacora.sEvento = "POST^" + stURLWS.Trim();
                oBitacora.sObservacion = "[obtenerRecibos]";
                oBitacora.sObservacion += "POST al servicio(webspeed)" + stOpcionRecibos + ".";
                oBitacora.sInputParams = string.Empty;
                oBitacora.sIPCliente = "0.0.0.0";
                _iIdBitacora = oDaoDatos.GuardarBitacora(oBitacora, listDetalleBit);
                listDetalleBit.Clear();
                oBitacora = null;
                oDaoDatos = null;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                oHttpResquest = (HttpWebRequest)WebRequest.Create(stURLWS.Trim());
                oHttpResquest.Method = "POST";
                oHttpResquest.ContentType = "application/json; charset=UTF-8";
                oHttpResquest.Timeout = iTimeOut;

                using (var streamWriter = new StreamWriter(oHttpResquest.GetRequestStream()))
                {
                    streamWriter.Write("{arecibos:" + _jsonRecibo +"}");
                }            

                try
                {
                    using (HttpWebResponse oHttpResp = (HttpWebResponse)oHttpResquest.GetResponse())
                    {
                        switch (oHttpResp.StatusCode)
                        {
                            case HttpStatusCode.OK:
                                {
                                    oJsonPost.lError = false;
                                    using (StreamReader oReader = new StreamReader(oHttpResp.GetResponseStream(), Encoding.GetEncoding("iso-8859-1")))
                                    {
                                        cJsonResponse = oReader.ReadToEnd();

                                        listDetalleBit.Add(
                                            Utilerias.CrearDetalleBitacora(_iIdBitacora, "JSON", cJsonResponse, Constantes.DET_BIT_OUTPUT)
                                        );

                                        try
                                        {
                                            oJsonPost.oJsonData = JObject.Parse(cJsonResponse);
                                            if (oJsonPost.oJsonData["iError"] != null && oJsonPost.oJsonData["cError"] != null)
                                            {
                                                oJsonPost.iError = oJsonPost.oJsonData.Value<int>("iError");
                                                oJsonPost.lError = (oJsonPost.iError > 0 ? true : false);
                                                oJsonPost.cError = oJsonPost.oJsonData.Value<string>("cError");

                                            }//fin:fin
                                            else
                                            {
                                                _cError = "El servicio no retorno una estructura de datos correcta(JSON inválido).";
                                                oJsonPost.lError = true;
                                                oJsonPost.cError = _cError;
                                            }//fin:else                                                                                        
                                        }//fin:if
                                        catch (JsonReaderException ex)
                                        {
                                            _cError = "El servicio no devolvio una respuesta valida:[" + ex.Message + "]";
                                            oJsonPost.lError = true;
                                            oJsonPost.cError = _cError;
                                        }
                                    }//fin:using
                                }//fin:case
                                break;
                            default:
                                {
                                    _cError = "HttpStatusCode=>" + oHttpResp.StatusCode.ToString();
                                    oJsonPost.lError = true;
                                    oJsonPost.cError = _cError;

                                    listDetalleBit.Add(
                                        Utilerias.CrearDetalleBitacora(
                                            _iIdBitacora, "ERROR", "HttpStatusCode^" + oHttpResp.StatusCode.ToString(), Constantes.DET_BIT_OUTPUT
                                        )
                                    );
                                }//fin:default
                                break;
                        }//fin:switch (oHttpResponse.StatusCode)
                    }//Fin:Using
                }//fin:try
                catch (WebException excep)
                {
                    string responseContent = "";
                    _cError = excep.Message;

                    WebResponse errResp = excep.Response;
                    using (Stream respStream = errResp.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(respStream);
                        responseContent = reader.ReadToEnd();
                    }//fin:using (Stream respStream = errResp.GetResponseStream())

                    _cError += "|" + responseContent;
                    oJsonPost.cError = _cError;
                    oJsonPost.lError = true;

                    listDetalleBit.Add(
                       Utilerias.CrearDetalleBitacora(_iIdBitacora, "ERROR[WEB EXCEPCION]", _cError, Constantes.DET_BIT_OUTPUT)
                    );
                }//Fin:catch

                //Guardar bitacora con la respuesta de la peticion
                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                listDetalleBit.Clear(); listDetalleBit = null;
                oDaoDatos = null;

            }//fin:try
            catch (Exception ex)
            {
                _cError += " | " + ex.Message;
                oJsonPost.cError = _cError;
                oJsonPost.lError = true;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
                if (_iIdBitacora > 0)
                {
                    if (listDetalleBit == null) listDetalleBit = new List<DTODetalleBitacora>();
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Message en [obtenerRecibos]",
                             ex.Message,
                             Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Source en [obtenerRecibos]",
                            ex.Source,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Target en en [obtenerRecibos]",
                            ex.TargetSite.ToString(),
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].StackTrace en [obtenerRecibos]",
                            ex.StackTrace,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                    oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                    listDetalleBit.Clear(); listDetalleBit = null;
                    oDaoDatos = null;
                }//fin:if (_iIdBitacora > 0)
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                if (oDaoDatos != null) oDaoDatos = null;
            }//fin:catch
            return oJsonPost;
        }//fin:ObtenerPDF
        //-----------------------------------------------------------------------------------------
        public static TJsonRespuesta generarReferenciaGNWS(TInputGenerarLR _oParamsWS) {

            int _iIdBitacora = 0;
            int iTimeOut = 0;
            string cJsonResponse = string.Empty;
            string stAppAmbiente = string.Empty;
            string stURLWS = string.Empty;
            string stOpcionRecibos = string.Empty;
            string stFechaVigencia = string.Empty;
            string stFormatFloat = string.Empty;

            string _cError = string.Empty;
            string _cData = string.Empty;

            HttpWebRequest oHttpResquest = null;

            NameValueCollection ParamsQueryString = HttpUtility.ParseQueryString(String.Empty);
            DaoConsultas oDaoDatos = null;
            List<string> listKeyConfig = null;
            List<DTOConfiguracion> listConfiguraciones = null;
            DTOBitacora oBitacora = null;
            List<DTODetalleBitacora> listDetalleBit = null;

            TJsonRespuesta oJsonPost = new TJsonRespuesta();

            try {
                oDaoDatos = new DaoConsultas();
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Obtener variables de configuracion
                ConfigurationManager.RefreshSection("appSettings");

                listKeyConfig = new List<string>();

                if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null) {
                    stAppAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                }//fin:if                               

                listKeyConfig.Add(Constantes.WTIMEOUT_REQUEST);
                switch (stAppAmbiente) {
                    case Constantes._DESARROLLO:
                        listKeyConfig.Add(Constantes.DES_URL_SERVICIOS_VUE);
                    break;
                    case Constantes._PRODUCCION: case Constantes._TEST_PRODUCCION:
                        listKeyConfig.Add(Constantes.PROD_URL_SERVICIOS_VUE);
                    break;
                }//fin:switch (stAppAmbiente)

                listConfiguraciones = oDaoDatos.ObtenerConfiguraciones(listKeyConfig);
                IEnumerable<DTOConfiguracion> oElements = listConfiguraciones.Where(x => x.lError == false);
                if (oElements.Count() != listKeyConfig.Count) {
                    _cError = "[generarReferenciaGNWS] No fue posible obtener los parámetros de configuración:";
                    _cError += string.Join(",", listKeyConfig);
                    oElements = null;
                    listKeyConfig = null;
                    oDaoDatos = null;
                    throw new Exception(_cError);
                }//fin:if
                listKeyConfig = null;

                DTOConfiguracion oElement = null;

                oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.WTIMEOUT_REQUEST);
                if (oElement != null) iTimeOut = Convert.ToInt32(oElement.cValor.Trim());

                switch (stAppAmbiente) {
                    case Constantes._DESARROLLO:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.DES_URL_SERVICIOS_VUE);
                        if (oElement != null) stURLWS = oElement.cValor;
                    break;
                    case Constantes._PRODUCCION: case Constantes._TEST_PRODUCCION:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.PROD_URL_SERVICIOS_VUE);
                        if (oElement != null) stURLWS = oElement.cValor;
                    break;
                }//fin:switch (stAmbiente) 
                listConfiguraciones = null;

                stOpcionRecibos = Constantes.WS_OPCION_GENERAR_REF_GN;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                ParamsQueryString.Clear();
                ParamsQueryString.Add("cOpcion", stOpcionRecibos);
                ParamsQueryString.Add("iIdTramite", _oParamsWS.iIdTramite.ToString());
                ParamsQueryString.Add("cAppKey", _oParamsWS.sAppKey);
                ParamsQueryString.Add("cDatosRecibo", _oParamsWS.sDatosRecibos);
                ParamsQueryString.Add("iIdDependencia", _oParamsWS.iIdDependencia.ToString());
                ParamsQueryString.Add("iIdSubDependencia", _oParamsWS.iIdSubDependencia.ToString());
                ParamsQueryString.Add("cAtributos", _oParamsWS.sAtributos);
                ParamsQueryString.Add("dtVigencia", _oParamsWS.sFechaVigenciaLR);
                ParamsQueryString.Add("dImporteTramite", _oParamsWS.sImporteTramite);
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Guardar bitacora de la peticion de pago                                           
                listDetalleBit = new List<DTODetalleBitacora>();
                foreach (string sKey in ParamsQueryString) {
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(0, sKey, ParamsQueryString[sKey], Constantes.DET_BIT_INPUT)
                    );
                }//fin:foreach (string key in ParamsQueryString)                

                oBitacora = new DTOBitacora();
                oBitacora.sFolio = _oParamsWS.sFolioExecute;
                oBitacora.sEvento = "POST^" + stURLWS.Trim();
                oBitacora.sObservacion = "[generarReferenciaGN]";
                oBitacora.sObservacion += "POST al servicio(webspeed) para generar una línea de referencia.";
                oBitacora.sInputParams = string.Empty;
                oBitacora.sIPCliente = "0.0.0.0";
                _iIdBitacora = oDaoDatos.GuardarBitacora(oBitacora, listDetalleBit);
                listDetalleBit.Clear();
                oBitacora = null;
                oDaoDatos = null;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                _cData = ParamsQueryString.ToString();

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                oHttpResquest = (HttpWebRequest)WebRequest.Create(stURLWS.Trim());
                oHttpResquest.Method = "POST";
                //oHttpResquest.ContentType = "application/x-www-form-urlencoded; charset=ISO-8859-1";
                oHttpResquest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                oHttpResquest.Timeout = iTimeOut;

                byte[] PostData = Encoding.UTF8.GetBytes(_cData);
                //byte[] PostData = Encoding.GetEncoding("ISO-8859-1").GetBytes(_cData);
                /*
                byte[] PostData = Encoding.Convert(
                    Encoding.UTF8, 
                    Encoding.GetEncoding("ISO-8859-1"), 
                    Encoding.UTF8.GetBytes(_cData)
                );
                */               
                oHttpResquest.ContentLength = PostData.Length;

                using (Stream requestStream = oHttpResquest.GetRequestStream()) {
                    requestStream.Write(PostData, 0, PostData.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }//fin:using                

                try {
                    using (HttpWebResponse oHttpResp = (HttpWebResponse)oHttpResquest.GetResponse()) {
                        switch (oHttpResp.StatusCode) {
                            case HttpStatusCode.OK: {
                                oJsonPost.lError = false;
                                using (StreamReader oReader = new StreamReader(oHttpResp.GetResponseStream(), Encoding.GetEncoding("iso-8859-1"))) {
                                    cJsonResponse = oReader.ReadToEnd();
                                    listDetalleBit.Add(
                                        Utilerias.CrearDetalleBitacora(_iIdBitacora, "JSON", cJsonResponse, Constantes.DET_BIT_OUTPUT)
                                    );

                                    try {
                                        oJsonPost.oJsonData = JObject.Parse(cJsonResponse);
                                        if (oJsonPost.oJsonData["iError"] != null && oJsonPost.oJsonData["cError"] != null) {                                                
                                            oJsonPost.iError = oJsonPost.oJsonData.Value<int>("iError");
                                            oJsonPost.lError = (oJsonPost.iError > 0 ? true : false);
                                            oJsonPost.cError = oJsonPost.oJsonData.Value<string>("cError");
                                        }//fin:fin
                                        else {
                                            _cError = "El servicio no retorno una estructura de datos correcta(JSON inválido).";
                                            oJsonPost.lError = true;
                                            oJsonPost.cError = _cError;
                                        }//fin:else                                                                                        
                                    }//fin:if
                                    catch (JsonReaderException ex) {
                                        _cError = "El servicio no devolvio una respuesta valida:[" + ex.Message + "]";
                                        oJsonPost.lError = true;
                                        oJsonPost.cError = _cError;
                                    }
                                }//fin:using
                            }//fin:case HttpStatusCode.OK
                            break;
                            default: {
                                _cError = "HttpStatusCode=>" + oHttpResp.StatusCode.ToString();
                                oJsonPost.lError = true;
                                oJsonPost.cError = _cError;                                    
                                listDetalleBit.Add(
                                    Utilerias.CrearDetalleBitacora(
                                        _iIdBitacora, "ERROR", "HttpStatusCode^" + oHttpResp.StatusCode.ToString(), Constantes.DET_BIT_OUTPUT
                                    )
                                );
                            }//fin:default
                            break;
                        }//fin:switch (oHttpResponse.StatusCode)
                    }//Fin:Using
                }//fin:try
                catch (WebException excep) {
                    string responseContent = "";
                    //_cError = excep.Message;
                    _cError = excep.Status.ToString() + "^" + excep.Message;

                    /*
                    WebResponse errResp = excep.Response;
                    using (Stream respStream = errResp.GetResponseStream()) {
                        StreamReader reader = new StreamReader(respStream);
                        responseContent = reader.ReadToEnd();
                    }//fin:using (Stream respStream = errResp.GetResponseStream())
                    */
                    if (excep.Response != null) {
                        using (Stream respStream = excep.Response.GetResponseStream()) {
                            StreamReader reader = new StreamReader(respStream);
                            responseContent = reader.ReadToEnd();
                        }//fin:using (Stream respStream = errResp.GetResponseStream())
                    }//fin:if

                    _cError += "|" + responseContent;
                    oJsonPost.cError = _cError;
                    oJsonPost.lError = true;

                    listDetalleBit.Add(
                       Utilerias.CrearDetalleBitacora(_iIdBitacora, "ERROR[WEB EXCEPCION]", _cError, Constantes.DET_BIT_OUTPUT)
                    );                    
                }//Fin:catch
                
                //Guardar bitacora con la respuesta de la peticion
                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                listDetalleBit = null;
                oDaoDatos = null;
            }//fin:try
            catch (Exception ex) {
                _cError += " | " + ex.Message;
                oJsonPost.cError = _cError;
                oJsonPost.lError = true;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
                if (_iIdBitacora > 0) {
                    if (listDetalleBit == null) listDetalleBit = new List<DTODetalleBitacora>();
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora, "EXCEPCION.Message en [generarReferenciaGNWS]", ex.Message, Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora, "EXCEPCION.Source en [generarReferenciaGNWS]", ex.Source, Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora, "EXCEPCION.Target en en [generarReferenciaGNWS]", ex.TargetSite.ToString(), Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora, "EXCEPCION.StackTrace en [generarReferenciaGNWS]", ex.StackTrace, Constantes.DET_BIT_OUTPUT
                        )
                    );

                    if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                    oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                    listDetalleBit.Clear(); listDetalleBit = null;
                    oDaoDatos = null;
                }//fin:if (_iIdBitacora > 0)
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                if (oDaoDatos != null) oDaoDatos = null;
            }//fin:catch

            return oJsonPost;
        }//fin:generarReferenciaGNWS
        //-----------------------------------------------------------------------------------------
        public static TJsonRespuesta generarReferenciaWS(TInputGenerarLR _oParamsWS) {

            int _iIdBitacora = 0;
            int iTimeOut = 0;
            string cJsonResponse = string.Empty;
            string stAppAmbiente = string.Empty;
            string stURLWS = string.Empty;
            string stOpcionRecibos = string.Empty;
            string stFechaVigencia = string.Empty;
            string stFormatFloat = string.Empty;

            string _cError = string.Empty;
            string _cData = string.Empty;

            HttpWebRequest oHttpResquest = null;

            NameValueCollection ParamsQueryString = HttpUtility.ParseQueryString(String.Empty);
            DaoConsultas oDaoDatos = null;
            List<string> listKeyConfig = null;
            List<DTOConfiguracion> listConfiguraciones = null;
            DTOBitacora oBitacora = null;
            List<DTODetalleBitacora> listDetalleBit = null;

            TJsonRespuesta oJsonPost = new TJsonRespuesta();

            try {
                oDaoDatos = new DaoConsultas();
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Obtener variables de configuracion
                ConfigurationManager.RefreshSection("appSettings");

                listKeyConfig = new List<string>();

                if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null) {
                    stAppAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                }//fin:if                               

                listKeyConfig.Add(Constantes.WTIMEOUT_REQUEST);
                switch (stAppAmbiente) {
                    case Constantes._DESARROLLO:
                        listKeyConfig.Add(Constantes.DES_URL_SERVICIOS_VUE);
                        break;
                    case Constantes._PRODUCCION:
                    case Constantes._TEST_PRODUCCION:
                        listKeyConfig.Add(Constantes.PROD_URL_SERVICIOS_VUE);
                        break;
                }//fin:switch (stAppAmbiente)

                listConfiguraciones = oDaoDatos.ObtenerConfiguraciones(listKeyConfig);
                IEnumerable<DTOConfiguracion> oElements = listConfiguraciones.Where(x => x.lError == false);
                if (oElements.Count() != listKeyConfig.Count) {
                    _cError = "[generarReferenciaWS] No fue posible obtener los parámetros de configuración:";
                    _cError += string.Join(",", listKeyConfig);
                    oElements = null;
                    listKeyConfig = null;
                    oDaoDatos = null;
                    throw new Exception(_cError);
                }//fin:if
                listKeyConfig = null;

                DTOConfiguracion oElement = null;

                oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.WTIMEOUT_REQUEST);
                if (oElement != null) iTimeOut = Convert.ToInt32(oElement.cValor.Trim());

                switch (stAppAmbiente) {
                    case Constantes._DESARROLLO:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.DES_URL_SERVICIOS_VUE);
                        if (oElement != null) stURLWS = oElement.cValor;
                        break;
                    case Constantes._PRODUCCION: case Constantes._TEST_PRODUCCION:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.PROD_URL_SERVICIOS_VUE);
                        if (oElement != null) stURLWS = oElement.cValor;
                    break;
                }//fin:switch (stAmbiente) 
                listConfiguraciones = null;

                stOpcionRecibos = Constantes.WS_OPCION_GENERAR_REF_VU;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                ParamsQueryString.Clear();
                ParamsQueryString.Add("cOpcion", stOpcionRecibos);
                ParamsQueryString.Add("iIdTramite", _oParamsWS.iIdTramite.ToString());
                ParamsQueryString.Add("cAppKey", _oParamsWS.sAppKey);
                ParamsQueryString.Add("cDatosRecibo", _oParamsWS.sDatosRecibos);
                ParamsQueryString.Add("iIdDependencia", _oParamsWS.iIdDependencia.ToString());
                ParamsQueryString.Add("iIdSubDependencia", _oParamsWS.iIdSubDependencia.ToString());
                ParamsQueryString.Add("cAtributos", _oParamsWS.sAtributos);
                ParamsQueryString.Add("dtVigencia", _oParamsWS.sFechaVigenciaLR);
                ParamsQueryString.Add("dImporteTramite", _oParamsWS.sImporteTramite);
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Guardar bitacora de la peticion de pago
                listDetalleBit = new List<DTODetalleBitacora>();
                foreach (string sKey in ParamsQueryString) {
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(0, sKey, ParamsQueryString[sKey], Constantes.DET_BIT_INPUT)
                    );
                }//fin:foreach (string key in ParamsQueryString)                

                oBitacora = new DTOBitacora();
                oBitacora.sFolio = _oParamsWS.sFolioExecute;
                oBitacora.sEvento = "POST^" + stURLWS.Trim();
                oBitacora.sObservacion = "[generarReferenciaVU]";
                oBitacora.sObservacion += "POST al servicio(webspeed)para generar una línea de referencia.";
                oBitacora.sInputParams = string.Empty;
                oBitacora.sIPCliente = "0.0.0.0";
                _iIdBitacora = oDaoDatos.GuardarBitacora(oBitacora, listDetalleBit);
                listDetalleBit.Clear();
                oBitacora = null;
                oDaoDatos = null;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                _cData = ParamsQueryString.ToString();

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                oHttpResquest = (HttpWebRequest)WebRequest.Create(stURLWS.Trim());
                oHttpResquest.Method = "POST";
                //oHttpResquest.ContentType = "application/x-www-form-urlencoded; charset=ISO-8859-1";
                oHttpResquest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                oHttpResquest.Timeout = iTimeOut;

                byte[] PostData = Encoding.UTF8.GetBytes(_cData);
                
                oHttpResquest.ContentLength = PostData.Length;

                using (Stream requestStream = oHttpResquest.GetRequestStream()) {
                    requestStream.Write(PostData, 0, PostData.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }//fin:using                

                try {
                    using (HttpWebResponse oHttpResp = (HttpWebResponse)oHttpResquest.GetResponse()) {
                        switch (oHttpResp.StatusCode) {
                            case HttpStatusCode.OK: {
                                    oJsonPost.lError = false;
                                    using (StreamReader oReader = new StreamReader(oHttpResp.GetResponseStream(), Encoding.GetEncoding("iso-8859-1"))) {
                                        cJsonResponse = oReader.ReadToEnd();
                                        listDetalleBit.Add(
                                            Utilerias.CrearDetalleBitacora(_iIdBitacora, "JSON", cJsonResponse, Constantes.DET_BIT_OUTPUT)
                                        );

                                        try {
                                            oJsonPost.oJsonData = JObject.Parse(cJsonResponse);
                                            if (oJsonPost.oJsonData["iError"] != null && oJsonPost.oJsonData["cError"] != null) {
                                                oJsonPost.iError = oJsonPost.oJsonData.Value<int>("iError");
                                                oJsonPost.lError = (oJsonPost.iError > 0 ? true : false);
                                                oJsonPost.cError = oJsonPost.oJsonData.Value<string>("cError");
                                            }//fin:fin
                                            else {
                                                _cError = "El servicio no retorno una estructura de datos correcta(JSON inválido).";
                                                oJsonPost.lError = true;
                                                oJsonPost.cError = _cError;
                                            }//fin:else                                                                                        
                                        }//fin:if
                                        catch (JsonReaderException ex) {
                                            _cError = "El servicio no devolvio una respuesta valida:[" + ex.Message + "]";
                                            oJsonPost.lError = true;
                                            oJsonPost.cError = _cError;
                                        }
                                    }//fin:using
                                }//fin:case HttpStatusCode.OK
                                break;
                            default: {
                                    _cError = "HttpStatusCode=>" + oHttpResp.StatusCode.ToString();
                                    oJsonPost.lError = true;
                                    oJsonPost.cError = _cError;
                                    listDetalleBit.Add(
                                        Utilerias.CrearDetalleBitacora(
                                            _iIdBitacora, "ERROR", "HttpStatusCode^" + oHttpResp.StatusCode.ToString(), Constantes.DET_BIT_OUTPUT
                                        )
                                    );
                                }//fin:default
                                break;
                        }//fin:switch (oHttpResponse.StatusCode)
                    }//Fin:Using
                }//fin:try
                catch (WebException excep) {
                    string responseContent = "";
                    //_cError = excep.Message;
                    _cError = excep.Status.ToString() + "^" + excep.Message;
                    
                    if (excep.Response != null) {
                        using (Stream respStream = excep.Response.GetResponseStream()) {
                            StreamReader reader = new StreamReader(respStream);
                            responseContent = reader.ReadToEnd();
                        }//fin:using (Stream respStream = errResp.GetResponseStream())
                    }//fin:if

                    _cError += "|" + responseContent;
                    oJsonPost.cError = _cError;
                    oJsonPost.lError = true;

                    listDetalleBit.Add(
                       Utilerias.CrearDetalleBitacora(_iIdBitacora, "ERROR[WEB EXCEPCION]", _cError, Constantes.DET_BIT_OUTPUT)
                    );
                }//Fin:catch

                //Guardar bitacora con la respuesta de la peticion
                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                listDetalleBit = null;
                oDaoDatos = null;
            }//fin:try
            catch (Exception ex) {
                _cError += " | " + ex.Message;
                oJsonPost.cError = _cError;
                oJsonPost.lError = true;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
                if (_iIdBitacora > 0) {
                    if (listDetalleBit == null) listDetalleBit = new List<DTODetalleBitacora>();
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,"EXCEPCION.Message en [generarReferenciaWS]",ex.Message, Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,"EXCEPCION.Source en [generarReferenciaWS]", ex.Source, Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "EXCEPCION.Target en en [generarReferenciaWS]", ex.TargetSite.ToString(), Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora, "EXCEPCION.StackTrace en [generarReferenciaWS]", ex.StackTrace, Constantes.DET_BIT_OUTPUT
                        )
                    );

                    if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                    oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                    listDetalleBit.Clear(); listDetalleBit = null;
                    oDaoDatos = null;
                }//fin:if (_iIdBitacora > 0)
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                if (oDaoDatos != null) oDaoDatos = null;
            }//fin:catch

            return oJsonPost;
        }//fin:generarReferenciaWS
        //-----------------------------------------------------------------------------------------
        public static TJsonRespuesta consultarReferenciaVU(string _sLineaReferencia, string _sAppKey, string _sFolioExecute) {

            int _iIdBitacora = 0;
            int iTimeOut = 0;
            string cJsonResponse = string.Empty;
            string stAppAmbiente = string.Empty;
            string stURLWS = string.Empty;
            string stOpcionRecibos = string.Empty;
            string stFechaVigencia = string.Empty;
            string stFormatFloat = string.Empty;

            string _cError = string.Empty;
            string _cData = string.Empty;

            HttpWebRequest oHttpResquest = null;

            NameValueCollection ParamsQueryString = HttpUtility.ParseQueryString(String.Empty);
            DaoConsultas oDaoDatos = null;
            List<string> listKeyConfig = null;
            List<DTOConfiguracion> listConfiguraciones = null;
            DTOBitacora oBitacora = null;
            List<DTODetalleBitacora> listDetalleBit = null;

            TJsonRespuesta oJsonPost = new TJsonRespuesta();

            try {
                oDaoDatos = new DaoConsultas();
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Obtener variables de configuracion
                ConfigurationManager.RefreshSection("appSettings");

                listKeyConfig = new List<string>();

                if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null) {
                    stAppAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                }//fin:if                               

                listKeyConfig.Add(Constantes.WTIMEOUT_REQUEST);
                switch (stAppAmbiente) {
                    case Constantes._DESARROLLO:
                        listKeyConfig.Add(Constantes.DES_URL_SERVICIOS_VUE);
                        break;
                    case Constantes._PRODUCCION: case Constantes._TEST_PRODUCCION:
                        listKeyConfig.Add(Constantes.PROD_URL_SERVICIOS_VUE);
                        break;
                }//fin:switch (stAppAmbiente)

                listConfiguraciones = oDaoDatos.ObtenerConfiguraciones(listKeyConfig);
                IEnumerable<DTOConfiguracion> oElements = listConfiguraciones.Where(x => x.lError == false);
                if (oElements.Count() != listKeyConfig.Count) {
                    _cError = "[consultarReferenciaVU] No fue posible obtener los parámetros de configuración:";
                    _cError += string.Join(",", listKeyConfig);
                    oElements = null;
                    listKeyConfig = null;
                    oDaoDatos = null;
                    throw new Exception(_cError);
                }//fin:if
                listKeyConfig = null;

                DTOConfiguracion oElement = null;

                oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.WTIMEOUT_REQUEST);
                if (oElement != null) iTimeOut = Convert.ToInt32(oElement.cValor.Trim());

                switch (stAppAmbiente) {
                    case Constantes._DESARROLLO:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.DES_URL_SERVICIOS_VUE);
                        if (oElement != null) stURLWS = oElement.cValor;
                    break;
                    case Constantes._PRODUCCION: case Constantes._TEST_PRODUCCION:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.PROD_URL_SERVICIOS_VUE);
                        if (oElement != null) stURLWS = oElement.cValor;
                    break;
                }//fin:switch (stAmbiente) 
                listConfiguraciones = null;

                stOpcionRecibos = Constantes.WS_OPCION_VERIFICAR_REF_VU;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
                ParamsQueryString.Clear();
                ParamsQueryString.Add("cOpcion", stOpcionRecibos);
                ParamsQueryString.Add("cAppKey", _sAppKey);
                ParamsQueryString.Add("cLineaReferencia", _sLineaReferencia);
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Guardar bitacora de la peticion de pago                
                listDetalleBit = new List<DTODetalleBitacora>();
                foreach (string sKey in ParamsQueryString) {
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(0, sKey, ParamsQueryString[sKey], Constantes.DET_BIT_INPUT)
                    );
                }//fin:foreach (string key in ParamsQueryString)                

                oBitacora = new DTOBitacora();
                oBitacora.sFolio = _sFolioExecute;
                oBitacora.sEvento = "POST^" + stURLWS.Trim();
                oBitacora.sObservacion = "[VerificarReferenciaVU]";
                oBitacora.sObservacion += "POST al servicio(webspeed)para obtener la información de una línea de referencia.";
                oBitacora.sInputParams = string.Empty;
                oBitacora.sIPCliente   = "0.0.0.0";
                _iIdBitacora = oDaoDatos.GuardarBitacora(oBitacora, listDetalleBit);
                listDetalleBit.Clear();
                oBitacora = null;
                oDaoDatos = null;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                _cData = ParamsQueryString.ToString();

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                oHttpResquest = (HttpWebRequest)WebRequest.Create(stURLWS.Trim());
                oHttpResquest.Method = "POST";
                //oHttpResquest.ContentType = "application/x-www-form-urlencoded; charset=ISO-8859-1";
                oHttpResquest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                oHttpResquest.Timeout     = iTimeOut;

                byte[] aPostData = Encoding.UTF8.GetBytes(_cData);
                
                oHttpResquest.ContentLength = aPostData.Length;

                using (Stream requestStream = oHttpResquest.GetRequestStream()) {
                    requestStream.Write(aPostData, 0, aPostData.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }//fin:using                

                try {
                    using (HttpWebResponse oHttpResp = (HttpWebResponse)oHttpResquest.GetResponse()) {
                        switch (oHttpResp.StatusCode) {
                            case HttpStatusCode.OK: {
                                oJsonPost.lError = false;
                                using (StreamReader oReader = new StreamReader(oHttpResp.GetResponseStream(), Encoding.GetEncoding("iso-8859-1"))) {
                                    cJsonResponse = oReader.ReadToEnd();

                                    listDetalleBit.Add(
                                        Utilerias.CrearDetalleBitacora(_iIdBitacora, "JSON", cJsonResponse, Constantes.DET_BIT_OUTPUT)
                                    );

                                    try {
                                        oJsonPost.oJsonData = JObject.Parse(cJsonResponse);
                                        if (oJsonPost.oJsonData["iError"] != null && oJsonPost.oJsonData["cError"] != null) {
                                            oJsonPost.iError = oJsonPost.oJsonData.Value<int>("iError");
                                            oJsonPost.lError = (oJsonPost.iError > 0 ? true : false);
                                            oJsonPost.cError = oJsonPost.oJsonData.Value<string>("cError");
                                        }//fin:fin
                                        else {
                                            _cError = "El servicio no retorno una estructura de datos correcta(JSON inválido).";
                                            oJsonPost.lError = true;
                                            oJsonPost.cError = _cError;
                                        }//fin:else                                                                                        
                                    }//fin:if
                                    catch (JsonReaderException ex) {
                                        _cError = "El servicio no devolvio una respuesta valida:[" + ex.Message + "]";
                                        oJsonPost.lError = true;
                                        oJsonPost.cError = _cError;
                                    }
                                }//fin:using
                            }//fin:case HttpStatusCode.OK
                            break;
                            default: {
                                _cError = "HttpStatusCode=>" + oHttpResp.StatusCode.ToString();
                                oJsonPost.lError = true;
                                oJsonPost.cError = _cError;
                                listDetalleBit.Add(
                                    Utilerias.CrearDetalleBitacora(
                                        _iIdBitacora, "ERROR", "HttpStatusCode^" + oHttpResp.StatusCode.ToString(), Constantes.DET_BIT_OUTPUT
                                    )
                                );
                            }//fin:default
                            break;
                        }//fin:switch (oHttpResponse.StatusCode)
                    }//Fin:Using
                }//fin:try
                catch (WebException excep) {
                    string responseContent = "";
                    _cError = excep.Message;

                    WebResponse errResp = excep.Response;
                    using (Stream respStream = errResp.GetResponseStream()) {
                        StreamReader reader = new StreamReader(respStream);
                        responseContent = reader.ReadToEnd();
                    }//fin:using (Stream respStream = errResp.GetResponseStream())

                    _cError += "|" + responseContent;
                    oJsonPost.cError = _cError;
                    oJsonPost.lError = true;

                    listDetalleBit.Add(
                       Utilerias.CrearDetalleBitacora(_iIdBitacora, "ERROR[WEB EXCEPCION]", _cError, Constantes.DET_BIT_OUTPUT)
                    );
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                }//Fin:catch

                //Guardar bitacora con la respuesta de la peticion
                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                listDetalleBit = null;
                oDaoDatos = null;
            }//fin:try
            catch (Exception ex) {
                _cError += " | " + ex.Message;
                oJsonPost.cError = _cError;
                oJsonPost.lError = true;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
                if (_iIdBitacora > 0) {
                    if (listDetalleBit == null) listDetalleBit = new List<DTODetalleBitacora>();
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Message en [consultarReferenciaVU]",
                             ex.Message,
                             Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Source en [consultarReferenciaVU]",
                            ex.Source,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Target en en [consultarReferenciaVU]",
                            ex.TargetSite.ToString(),
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].StackTrace en [consultarReferenciaVU]",
                            ex.StackTrace,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                    oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                    listDetalleBit.Clear(); listDetalleBit = null;
                    oDaoDatos = null;
                }//fin:if (_iIdBitacora > 0)
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                if (oDaoDatos != null) oDaoDatos = null;
            }//fin:catch

            return oJsonPost;

        }//fin:consultarReferenciaVU
        //-----------------------------------------------------------------------------------------
        public static bool comprobarAppKeyHabilitada(string _sAppKeyEvaluar, ref string _sMensajeError) {

            bool bHabilitada = false;
            string sKeyConfiguracion = Constantes.VUE_APPKEY_HABILITADAS_SERVICIO;
            string sValueConfig = string.Empty;
            string[] aListaAppKeys = new string[] { };

            DaoConsultas oDaoDatos = null;
            DTOConfiguracion oParamConfig = null;

            _sMensajeError = string.Empty;

            try {
                //{0}æ{1}
                oDaoDatos = new DaoConsultas();

                oParamConfig = oDaoDatos.ObtenerValorConfig(sKeyConfiguracion);
                if (oParamConfig.lError == false) {
                    sValueConfig = oParamConfig.cValor.Trim();
                }//fin:if                                               
                oParamConfig = null;
                oDaoDatos = null;

                if (!string.IsNullOrEmpty(sValueConfig)) {
                    aListaAppKeys = sValueConfig.Trim().Split(Constantes.SEPARADOR_DIPTONGO);
                    for (int iIdx = 0; iIdx < aListaAppKeys.Length; iIdx++) {
                        if (_sAppKeyEvaluar.Trim() == aListaAppKeys[iIdx].Trim()) {
                            bHabilitada = true;
                            iIdx = aListaAppKeys.Length;
                        }//fin:
                    }//fin:for
                }//fin:if
                //else { bHabilitada = false; }
                else { bHabilitada = true; }
            }//fin:try
            catch (Exception oExc) {
                _sMensajeError  = "Error en [comprobarAppKeyHabilitada]:";
                _sMensajeError += "Message^"     + oExc.Message;
                _sMensajeError += "~Source^"     + oExc.Source;
                _sMensajeError += "~Target^"     + oExc.TargetSite.ToString();
                _sMensajeError += "~StackTrace^" + oExc.StackTrace;
                bHabilitada = false;
                if (oDaoDatos != null) oDaoDatos = null;

            }//fin:catch

            return bHabilitada;
        }//fin:comprobarAppKeyHabilitada
        //-----------------------------------------------------------------------------------------
        public static string CrearBodyEmail(string sAviso, string sMensaje) {

            string stBody = string.Empty;

            stBody = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>" +
                   " <html xmlns='http://www.w3.org/1999/xhtml'> " +
                   "<head> " +
                   "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />" +
                   "<title>Avisos</title>" +
                   "</head>" +
                   "<body>" +
                   "<table cellpadding='0' cellspacing='0' border='0' width='500'>" +
                   "<tr>" +
                   "   <td height='30' width='500'><font face='Arial, Helvetica, sans-serif' size='2'><b>Aviso</b></font></td>" +
                   "</tr>" +
                   "<tr>" +
                   "   <td height='30'><font face='Arial' size='2'>" + sAviso + "</font></td>" +
                   "</tr>" +
                   "<tr>" +
                   "   <td height='30'><font face='Arial' size='2'><b>Descripci&oacute;n</b></font></td>" +
                   "</tr>" +
                   "<tr>" +
                   "   <td height='30'><font face='Arial' size='2' color='#FF0000'>" + sMensaje + "</font></td>" +
                   "</tr>" +
                   "<tr>" +
                   "   <td height='25'></td>" +
                   "</tr>" +
                   "</table>" +
                   "</body>" +
                   "</html>";
            return stBody;
        }//fin:CrearBodyEmail
        //-----------------------------------------------------------------------------------------
        public static void enviarEmailNotificacion(string _Asunto, string _Aviso, string _Mensaje) {
           
            string stBody = string.Empty;
            int    iEmailPort = 25;
            string stEmailFrom = string.Empty;
            string stSMTP_User = string.Empty;
            string stSMTP_Pass = string.Empty;
            string stSMTP_Host = string.Empty;
            string sEmailTo    = string.Empty;
            string sAppAmbiente = string.Empty;
            string sEmailAsunto = string.Empty;
            string stErrors = string.Empty;

            MailMessage oMailMessage = null;            
            DaoConsultas oDaoConsulta = null;
            DTOConfiguracion oConfigDB = null;
            JObject oJsonConfig = null;

            try {

                oDaoConsulta = new DaoConsultas();

                ConfigurationManager.RefreshSection("appSettings");
                sEmailTo = ConfigurationManager.AppSettings[Constantes.EMAIL_TO_ERRORES];

                sAppAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE];
                switch (sAppAmbiente) {
                    case Constantes._DESARROLLO:
                        sEmailAsunto = string.Format("[{0}]", Constantes._DESARROLLO);
                        break;
                    case Constantes._TEST_PRODUCCION:
                        sEmailAsunto = string.Format("[{0}]", Constantes._TEST_PRODUCCION);
                        break;
                    case Constantes._PRODUCCION:
                        sEmailAsunto = string.Format("[{0}]", Constantes._PRODUCCION);
                        break;
                    default:
                        sEmailAsunto = string.Format("[{0}]", Constantes._DESARROLLO);
                    break;
                }//fin:switch
                sEmailAsunto += _Asunto;
                
                oConfigDB = oDaoConsulta.ObtenerValorConfig(Constantes.WJSON_CONFIG_SMTP);
                if (oConfigDB.lError == false) {
                    oJsonConfig = JObject.Parse(oConfigDB.cValor);

                    if (oJsonConfig["email_usuario"] != null) {
                        stSMTP_User = oJsonConfig.Value<string>("email_usuario");
                    }//fin:if

                    if (oJsonConfig["email_password"] != null) {
                        stSMTP_Pass = oJsonConfig.Value<string>("email_password");
                    }//fin:if

                    if (oJsonConfig["email_smtp_host"] != null) {
                        stSMTP_Host = oJsonConfig.Value<string>("email_smtp_host");
                    }//fin:if

                    if (oJsonConfig["email_from"] != null) {
                        stEmailFrom  = oJsonConfig.Value<string>("email_from");
                    }//fin:if

                    if (oJsonConfig["email_port"] != null) {
                        iEmailPort = oJsonConfig.Value<int>("email_port");
                    }//fin:if

                    oMailMessage = new MailMessage();

                    //Body
                    stBody = FuncionesWeb.CrearBodyEmail(_Aviso, _Mensaje);
                    oMailMessage.Body = stBody;
                    oMailMessage.IsBodyHtml = true;

                    //Para [,]
                    oMailMessage.To.Add(sEmailTo);

                    //De
                    oMailMessage.From = new MailAddress(stEmailFrom);

                    //Asunto
                    //oMailMessage.Subject = _Asunto;
                    oMailMessage.Subject = sEmailAsunto;

                    //Asignacion del Host                                     
                    using (SmtpClient oSMTP = new SmtpClient()) {
                        oSMTP.Host = stSMTP_Host;
                        //if (iEmailPort != null && stEmailPort != "0" && stEmailPort != "") {
                        //    oSMTP.Port = int.Parse(stEmailPort);
                        //    oSMTP.EnableSsl = true;
                        //}//fin:if

                        oSMTP.Port = iEmailPort;
                        oSMTP.EnableSsl = true;

                        oSMTP.Credentials = new NetworkCredential(stSMTP_User, stSMTP_Pass);
                        oSMTP.Send(oMailMessage);

                        oMailMessage.Dispose();
                        oSMTP.Dispose();
                    }//fin: using (System.Net.Mail.SmtpClient oSMTP = new System.Net.Mail.SmtpClient())
                }//fin:if (oConfigDB.lError == false)              
            }//fin:try
            catch (Exception oEx) {
                stErrors = "Error en [enviarEmailNotificacion]: "; ;
                stErrors += "Mensaje^" + oEx.Message;
                stErrors += "~Source^" + oEx.Source;
                stErrors += "~Target^" + oEx.TargetSite.ToString();
                stErrors += "~StackTrace^" + oEx.StackTrace;
                Utilerias.guardarLogEventos(stErrors);
            }
        }//fin:EnviaEmail  
        //---------------------------------------------------------------------        
        public static TJsonRespuesta ComprobarReciboPDF(string _sUrlRecibo, int _iTimeOut, string _basePath){

            bool bSuccess = false;
            TJsonRespuesta oJsonPost = new TJsonRespuesta();
            string sValueContentType = string.Empty;
            string sFileNamePDF = _basePath + "\\" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".pdf";
            string _sErrorMsg = string.Empty;
            string sErrorMetodo = string.Empty;
            

                HttpWebRequest oHttpResquest = null;

                try
                {
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                               
                    //TODO: Para obtener certificado SSL
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    oHttpResquest = (HttpWebRequest)WebRequest.Create(_sUrlRecibo);
                    oHttpResquest.Method = "GET";

                    oHttpResquest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                    oHttpResquest.Timeout = _iTimeOut;

                    try
                    {
                        using (HttpWebResponse oHttpWebR = (HttpWebResponse)oHttpResquest.GetResponse())
                        {
                            switch (oHttpWebR.StatusCode)
                            {
                                case HttpStatusCode.OK:
                                    {
                                        for (int iIdx = 0; iIdx < oHttpWebR.Headers.Count; iIdx++)
                                        {
                                            if (oHttpWebR.Headers.Keys[iIdx].Trim() == Constantes.HEADER_CONTENT_TYPE)
                                            {
                                                sValueContentType = (oHttpWebR.Headers[Constantes.HEADER_CONTENT_TYPE]).Replace("\"", string.Empty);
                                                iIdx = oHttpWebR.Headers.Count;
                                            }//fin:if                                         
                                        }//fin:for

                                        switch (sValueContentType)
                                        {
                                            //case Constantes.MIME_TYPE_JSON: break;
                                            //case Constantes.MIME_TYPE_OCTET_STREAM: break;                                        
                                            case Constantes.MIME_TYPE_PDF:
                                                bSuccess = true;
                                                using (var stream = File.Create(sFileNamePDF))
                                                {
                                                    oHttpWebR.GetResponseStream().CopyTo(stream);
                                                }//fin:using
                                                break;
                                            default:
                                                bSuccess = false;
                                                _sErrorMsg = "El servicio no retorno una respuesta correcta.";
                                                break;
                                        }//fin:switch
                                    }//fin:case HttpStatusCode.OK
                                    break;
                                default: bSuccess = false; break;
                            }//fin:switch (oHttpResponse.StatusCode)
                        }//Fin:Using
                    }//fin:try
                    catch (WebException excep)
                    {
                        string responseContent = "";
                        _sErrorMsg = excep.Message;

                    
                        WebResponse errResp = excep.Response;

                        if(errResp != null)
                        {
                            using (Stream respStream = errResp.GetResponseStream())
                            {
                                StreamReader reader = new StreamReader(respStream);
                                responseContent = reader.ReadToEnd();
                            }//fin:using (Stream respStream = errResp.GetResponseStream())
                        }



                        _sErrorMsg += "|" + responseContent;
                    }//Fin:catch                                                                           
                }//fin:try
                catch (Exception oEx)
                {
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    _sErrorMsg = "Ocurrió un error en el método [obtenerCFDIPdf]:";
                    _sErrorMsg += "Message^" + oEx.Message;
                    _sErrorMsg += "~Source^" + oEx.Source;
                    _sErrorMsg += "~Target^" + oEx.TargetSite.ToString();
                    _sErrorMsg += "~StackTrace^" + oEx.StackTrace;
                    bSuccess = false;
                }//fin:catch

                if (bSuccess)
                {
                    oJsonPost.lError = false;
                    oJsonPost.cError = "";
                    oJsonPost.cMensaje = sFileNamePDF;
                }
                else
                {
                    oJsonPost.lError = true;
                    oJsonPost.cError = _sErrorMsg;
                }
                
            //En if comprobar variable de configuración null o vacio
            return oJsonPost;
        }//fin:comprobar recibopdf
        //---------------------------------------------------------------------
        public static TJsonRespuesta UnirRecibosPDF(JArray listFilesPDF, string basePath)
        {
            String sFileNameAllPDFs = string.Empty;
            Document oDocPDFContent = null;
            PdfCopy oPdfCopy = null;
            TJsonRespuesta oResponsePost = new TJsonRespuesta();
            String sErrorMetodo = string.Empty;
            MemoryStream ms = new MemoryStream();

            if(listFilesPDF.Count == 1)
            {
                oResponsePost.lError = false;
                oResponsePost.cError = "";
                oResponsePost.cMensaje = listFilesPDF[0].ToString();
                return oResponsePost;
            }

            try {

                if (listFilesPDF.Count > 0)
                {
                    sFileNameAllPDFs = basePath;
                    sFileNameAllPDFs += "\\all_";
                    sFileNameAllPDFs += DateTime.Now.ToString("yyyyMMddHHmmss.ffff") + "_";
                    sFileNameAllPDFs += ((new Random()).Next(0, 100)).ToString() + ".pdf";

                    oDocPDFContent = new Document();
                    oPdfCopy = new PdfCopy(oDocPDFContent, new FileStream(sFileNameAllPDFs, FileMode.Create));
                


                    oDocPDFContent.AddAuthor("AAFY");
                    oDocPDFContent.AddTitle("RECIBO OFICIAL");

                    oPdfCopy.CompressionLevel = PdfStream.BEST_COMPRESSION;
                    oDocPDFContent.Open();

                
                    foreach (string sFileCurrent in listFilesPDF)
                    {
                        PdfReader oPdfReader = new PdfReader(sFileCurrent);
                        oPdfCopy.AddDocument(oPdfReader);

                        oPdfReader.Close();
                    }//fin:foreach (string sFileCurrent in listFilesPDF)

                    oPdfCopy.Close();
                    oDocPDFContent.Close();

                    oResponsePost.lError = false;
                    oResponsePost.cError = "";
                    oResponsePost.cMensaje = sFileNameAllPDFs;

                    ///Borrar los archivos temporales
                    foreach (string sFilePDFTemp in listFilesPDF)
                    {
                        sErrorMetodo = string.Empty;
                        eliminarArchivoFileSystem(sFilePDFTemp, ref sErrorMetodo);
                        if (!string.IsNullOrEmpty(sErrorMetodo))
                        {

                        }//fin:if 
                    }//fin:foreach (string sFilePDFTemp in listFilesPDF)                                                                                           
                }//fin:if

            }
            catch (Exception oEx)
            {
                oResponsePost.lError = true;
                oResponsePost.cError = oEx.Message;
            }

            return oResponsePost;

        }
        //---------------------------------------------------------------------
        public static bool eliminarArchivoFileSystem(string _sFileName, ref string _sOutError)
        {

            bool lSuccess = true;

            _sOutError = string.Empty;

            if (File.Exists(_sFileName))
            {
                try { File.Delete(_sFileName); lSuccess = true; }
                catch (Exception oExcFile)
                {
                    _sOutError = "Error al eliminar el archivo" + Path.GetFileName(_sFileName) + ":" + oExcFile.Message;
                    lSuccess = false;
                }//fin:catch
            }//fin:if (File.Exists(sFileNameBarCode))

            return lSuccess;

        }//fin:eliminarArchivoFileSystem
        //---------------------------------------------------------------------        
        public static JObject RespReferenciaPrincipal(JArray aJsonTramites, string urlBase, bool bPdf, string sRutaTempPDF, bool bUrlEncode, bool bUnDocumento = false)
        {
            JArray jListAuxiliar = new JArray();
            JObject ObjItem = null;
            string sUrlRecibo = string.Empty;
            TJsonRespuesta jsonItem = null;
            string file = string.Empty;
            string sFileNameConfigMsg = string.Empty;
            TPostResponse oMsgError = null;
            string sBase64HojaPDF = string.Empty;
            JObject oJsonResponse = new JObject();

            oJsonResponse["iError"] = 0;
            oJsonResponse["cError"] = "";

            foreach (JObject oJsonTramite in aJsonTramites.Children<JObject>())
            {
                ObjItem = new JObject();
                if (oJsonTramite.Value<string>("cReferencia") != null && oJsonTramite.Value<string>("cEstatusPago") == "P" && oJsonTramite.Value<int>("iRecibo") > 0 && !string.IsNullOrEmpty(oJsonTramite.Value<string>("cSerie")))
                {
                    
                    sUrlRecibo = urlBase.Replace("@1", oJsonTramite.Value<string>("cReferencia"));

                    switch (bPdf)
                    {
                        case true:
                            jsonItem = new TJsonRespuesta();
                            jsonItem = FuncionesWeb.ComprobarReciboPDF(
                                sUrlRecibo, Constantes.TIMEOUT_DEFAULT, sRutaTempPDF
                            );

                            if (!jsonItem.lError)
                            {

                                if (!bUnDocumento)
                                {
                                    Byte[] bytes = File.ReadAllBytes(jsonItem.cMensaje);
                                    file = Convert.ToBase64String(bytes);
                                    sBase64HojaPDF = string.Empty;
                                    if (bUrlEncode) { sBase64HojaPDF = HttpUtility.UrlEncode(file, Encoding.UTF8); }
                                    else { sBase64HojaPDF = file; }
                                    ObjItem["cReferencia"] = oJsonTramite.Value<string>("cReferencia");
                                    ObjItem["cBase64"] = sBase64HojaPDF;
                                    jListAuxiliar.Add(ObjItem);
                                }
                                else
                                {
                                    jListAuxiliar.Add(jsonItem.cMensaje);
                                }
                            }

                            break;
                        default:
                            ObjItem["cReferencia"] = oJsonTramite.Value<string>("cReferencia");
                            ObjItem["cUrl"] = sUrlRecibo;
                            jListAuxiliar.Add(ObjItem);
                            break;
                    }

                }
            }//fin:foreach

            //Error al no tener valores
            if (jListAuxiliar.Count == 0)
            {
                sFileNameConfigMsg = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");
                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_12);
                oJsonResponse["iError"] = oMsgError.iError;
                oJsonResponse["cError"] = oMsgError.cError;
                oMsgError = null;
            }

            //Formato especial al pedir un documento

            if (bPdf == true && bUnDocumento == true && jListAuxiliar.Count > 0)
            {
                TJsonRespuesta datosTemp = FuncionesWeb.UnirRecibosPDF(jListAuxiliar, sRutaTempPDF);
                ObjItem = new JObject();

                Byte[] bytes = File.ReadAllBytes(datosTemp.cMensaje);
                file = Convert.ToBase64String(bytes);
                sBase64HojaPDF = string.Empty;
                if (bUrlEncode) { sBase64HojaPDF = HttpUtility.UrlEncode(file, Encoding.UTF8); }
                else { sBase64HojaPDF = file; }
                ObjItem["cBase64"] = sBase64HojaPDF;

                oJsonResponse["datos"] = JToken.FromObject(ObjItem);
            }
            else
            {
                oJsonResponse["datos"] = JToken.FromObject(jListAuxiliar);
            }

            return oJsonResponse;

        }
        //---------------------------------------------------------------------
        public static JObject RespReferenciaSecundaria(string cReferencia, string urlBase, bool bPdf, string sRutaTempPDF, bool bUrlEncode)
        {
            JArray jListAuxiliar = new JArray();
            JObject ObjItem = new JObject();
            string sUrlRecibo = string.Empty;
            TJsonRespuesta jsonItem = null;
            string file = string.Empty;
            string sBase64HojaPDF = string.Empty;
            JObject oJsonResponse = new JObject();

            oJsonResponse["iError"] = 0;
            oJsonResponse["cError"] = "";

            sUrlRecibo = urlBase.Replace("@1", cReferencia);

            switch (bPdf)
            {
                case true:
                    jsonItem = new TJsonRespuesta();
                    jsonItem = FuncionesWeb.ComprobarReciboPDF(
                        sUrlRecibo, Constantes.TIMEOUT_DEFAULT, sRutaTempPDF
                    );

                    if (!jsonItem.lError)
                    {
                        Byte[] bytes = File.ReadAllBytes(jsonItem.cMensaje);
                        file = Convert.ToBase64String(bytes);
                        sBase64HojaPDF = string.Empty;
                        if (bUrlEncode) { sBase64HojaPDF = HttpUtility.UrlEncode(file, Encoding.UTF8); }
                        else { sBase64HojaPDF = file; }
                        ObjItem["cReferencia"] = cReferencia;
                        ObjItem["cBase64"] = sBase64HojaPDF;
                        jListAuxiliar.Add(ObjItem);
                    }

                    break;
                default:
                    ObjItem["cReferencia"] = cReferencia;
                    ObjItem["cUrl"] = sUrlRecibo;
                    jListAuxiliar.Add(ObjItem);
                    break;
            }

            oJsonResponse["datos"] = JToken.FromObject(jListAuxiliar);

            return oJsonResponse;

        }
        //---------------------------------------------------------------------
        public static JObject RespRecibos(JArray aJsonTramites, string urlBase, bool bPdf, bool bUrlEncode, bool bUnDocumento = false)
        {
            JArray jListAuxiliar = new JArray();
            JObject ObjItem = null;
            DateTime Inicio;
            DateTime Fin;
            Double Time;
            TJsonRespuesta oJsonRespWS = null;
            string sJsonRecibos = null;
            string sUrlRecibo = string.Empty;
            TJsonRespuesta oJsonRecibo = null;
            string file = string.Empty;
            string sFileNameConfigMsg = string.Empty;
            TPostResponse oMsgError = null;
            string sBase64HojaPDF = string.Empty;
            JObject oJsonResponse = new JObject();

            oJsonResponse["iError"] = 0;
            oJsonResponse["cError"] = "";

            foreach (JObject oJsonTramite in aJsonTramites.Children<JObject>())
            {
                ObjItem = new JObject();
                if (oJsonTramite.Value<string>("cReferencia") != null && oJsonTramite.Value<string>("cEstatusPago") == "P" && oJsonTramite.Value<int>("iRecibo") > 0 && !string.IsNullOrEmpty(oJsonTramite.Value<string>("cSerie")))
                {
                    sUrlRecibo = urlBase.Replace("@1", oJsonTramite.Value<string>("cReferencia"));

                    switch (bPdf)
                    {
                        case true:
                            Inicio = DateTime.Now;
                            oJsonRecibo = FuncionesWeb.reciboJsonPDF(oJsonTramite.Value<string>("cReferencia"));
                            Fin = DateTime.Now;
                            Time = (Fin - Inicio).TotalSeconds;
                            Utilerias.guardarLogEventos("Finalización de POST A Generar reciboJsonPDF " + Time + " {REFERENCIA " + oJsonTramite.Value<string>("cReferencia")+ "}");

                            if (!oJsonRecibo.lError)
                            {
                                if (!bUnDocumento)
                                {
                                    oJsonRespWS = new TJsonRespuesta();
                                    sJsonRecibos = "[" + oJsonRecibo.oJsonData.SelectToken("arecibos")[0] + "]";

                                    Inicio = DateTime.Now;
                                    oJsonRespWS = FuncionesWeb.obtenerPDF(sJsonRecibos);
                                    Fin = DateTime.Now;
                                    Time = (Fin - Inicio).TotalSeconds;
                                    Utilerias.guardarLogEventos("Finalización de POST A Generar obtenerPDF " + Time + " {REFERENCIA " + oJsonTramite.Value<string>("cReferencia") + "}");

                                    if (oJsonRespWS.iError == 0)
                                    {
                                        file = oJsonRespWS.oJsonData.SelectToken("cBase64PDF").ToString();
                                        if (bUrlEncode) { sBase64HojaPDF = HttpUtility.UrlEncode(file, Encoding.UTF8); }
                                        else { sBase64HojaPDF = file; }
                                        ObjItem = new JObject();
                                        ObjItem["cReferencia"] = oJsonTramite.Value<string>("cReferencia");
                                        ObjItem["cBase64"] = sBase64HojaPDF;
                                        jListAuxiliar.Add(ObjItem);
                                    }
                                }
                                else
                                {
                                    jListAuxiliar.Add(oJsonRecibo.oJsonData.SelectToken("arecibos")[0]);
                                }
                            }

                            break;
                        default:
                            ObjItem["cReferencia"] = oJsonTramite.Value<string>("cReferencia");
                            ObjItem["cUrl"] = sUrlRecibo;
                            jListAuxiliar.Add(ObjItem);
                            break;
                    }

                }
            }//fin:foreach

            //Error al no tener valores
            if (jListAuxiliar.Count == 0)
            {
                sFileNameConfigMsg = System.Web.Hosting.HostingEnvironment.MapPath("~/config/mensajes.json");
                oMsgError = Utilerias.obtenerConfigMensaje(sFileNameConfigMsg, Constantes.KEY_MSG_ERROR_12);
                oJsonResponse["iError"] = oMsgError.iError;
                oJsonResponse["cError"] = oMsgError.cError;
                oMsgError = null;
            }

            //Formato especial al pedir un documento

            if (bPdf == true && bUnDocumento == true && jListAuxiliar.Count > 0)
            {
                oJsonRespWS = new TJsonRespuesta();
                sJsonRecibos = jListAuxiliar.ToString(Formatting.None);
                Inicio = DateTime.Now;
                oJsonRespWS = FuncionesWeb.obtenerPDF(sJsonRecibos);
                Fin = DateTime.Now;
                Time = (Fin - Inicio).TotalSeconds;
                Utilerias.guardarLogEventos("Finalización de POST A Generar obtenerPDF " + Time + " ARRAY DE REFERENCIAS");

                if (oJsonRespWS.iError == 0)
                {
                    file = oJsonRespWS.oJsonData.SelectToken("cBase64PDF").ToString();
                    if (bUrlEncode) { sBase64HojaPDF = HttpUtility.UrlEncode(file, Encoding.UTF8); }
                    else { sBase64HojaPDF = file; }
                    ObjItem = new JObject();
                    ObjItem["cBase64"] = sBase64HojaPDF;
                    oJsonResponse["datos"] = JToken.FromObject(ObjItem);
                }
                else
                {
                    oJsonResponse["iError"] = oJsonRespWS.iError;
                    oJsonResponse["cError"] = oJsonRespWS.cError;
                }
            }
            else
            {
                oJsonResponse["datos"] = JToken.FromObject(jListAuxiliar);
            }

            return oJsonResponse;

        }
        //---------------------------------------------------------------------
        public static TJsonRespuesta obtenerDetalleReferencia(string _LineaRef, string _sFolioExecute)
        {

            int _iIdBitacora = 0;
            int iTimeOut = 0;
            string cJsonResponse = string.Empty;
            string stAppAmbiente = string.Empty;
            string stURLWS = string.Empty;
            string stOpcionRecibos = string.Empty;
            string stFechaVigencia = string.Empty;
            string stFormatFloat = string.Empty;
            string sLinealog = string.Empty;


            string _cError = string.Empty;
            string _cData = string.Empty;

            HttpWebRequest oHttpResquest = null;

            NameValueCollection ParamsQueryString = HttpUtility.ParseQueryString(String.Empty);
            DaoConsultas oDaoDatos = null;
            List<string> listKeyConfig = null;
            List<DTOConfiguracion> listConfiguraciones = null;
            DTOBitacora oBitacora = null;
            List<DTODetalleBitacora> listDetalleBit = null;

            TJsonRespuesta oJsonPost = new TJsonRespuesta();

            try
            {
                oDaoDatos = new DaoConsultas();
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Obtener variables de configuracion
                ConfigurationManager.RefreshSection("appSettings");

                listKeyConfig = new List<string>();

                if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null)
                {
                    stAppAmbiente = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                }//fin:if               

                //listKeyConfig.Add(Constantes.WSOPCION_OBTENER_RECIBOS);
                listKeyConfig.Add(Constantes.WTIMEOUT_REQUEST);
                switch (stAppAmbiente)
                {
                    case Constantes._DESARROLLO:
                        listKeyConfig.Add(Constantes.DES_URL_GENERICOPAGOS);
                        break;
                    case Constantes._PRODUCCION:
                    case Constantes._TEST_PRODUCCION:
                        listKeyConfig.Add(Constantes.PROD_URL_GENERICOPAGOS);
                        break;
                }//fin:switch (stAppAmbiente)

                listConfiguraciones = oDaoDatos.ObtenerConfiguraciones(listKeyConfig);
                IEnumerable<DTOConfiguracion> oElements = listConfiguraciones.Where(x => x.lError == false);
                if (oElements.Count() != listKeyConfig.Count)
                {
                    _cError = "[obtenerDetalle-Referencia] No fue posible obtener los parámetros de configuración:";
                    _cError += string.Join(",", listKeyConfig);
                    oElements = null;
                    listKeyConfig = null;
                    oDaoDatos = null;
                    throw new Exception(_cError);
                }//fin:if
                listKeyConfig = null;

                DTOConfiguracion oElement = null;

                oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.WTIMEOUT_REQUEST);
                if (oElement != null) iTimeOut = Convert.ToInt32(oElement.cValor.Trim());

                switch (stAppAmbiente)
                {
                    case Constantes._DESARROLLO:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.DES_URL_GENERICOPAGOS);
                        if (oElement != null) stURLWS = oElement.cValor;
                        break;
                    case Constantes._PRODUCCION:
                    case Constantes._TEST_PRODUCCION:
                        oElement = listConfiguraciones.SingleOrDefault(x => x.cClave == Constantes.PROD_URL_GENERICOPAGOS);
                        if (oElement != null) stURLWS = oElement.cValor;
                        break;
                }//fin:switch (stAmbiente) 
                listConfiguraciones = null;

                stOpcionRecibos = Constantes.WS_OPCION_DETALLEREFERENCIA;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                ParamsQueryString.Clear();
                ParamsQueryString.Add("cOpcion", stOpcionRecibos);
                ParamsQueryString.Add("cLineaReferencia", _LineaRef);
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                //Guardar bitacora de la peticion de Detalla de Refrencia               
                listDetalleBit = new List<DTODetalleBitacora>();
                foreach (string sKey in ParamsQueryString)
                {
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(0, sKey, ParamsQueryString[sKey], Constantes.DET_BIT_INPUT)
                    );
                }//fin:foreach (string key in ParamsQueryString)                           
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                _cData = ParamsQueryString.ToString();

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                oHttpResquest = (HttpWebRequest)WebRequest.Create(stURLWS.Trim());
                oHttpResquest.Method = "POST";
                oHttpResquest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                oHttpResquest.Timeout = iTimeOut;
                byte[] PostData = Encoding.UTF8.GetBytes(_cData);

                oHttpResquest.ContentLength = PostData.Length;

                //Inicio de tiempo en peticion
                DateTime Inicio = DateTime.Now;

                using (Stream requestStream = oHttpResquest.GetRequestStream())
                {
                    requestStream.Write(PostData, 0, PostData.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }//fin:using                

                try
                {
                    
                    using (HttpWebResponse oHttpResp = (HttpWebResponse)oHttpResquest.GetResponse())
                    {
                       
                        //Final de tiempo de tiempo en peticion
                        DateTime Fin = DateTime.Now;

                        Double Time = (Fin - Inicio).TotalSeconds;

                        //+++++++++++++++++++++GUADAR EN BITACORA LE PETICION++++++++++
                        if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                        oBitacora = new DTOBitacora();
                        oBitacora.sFolio = _sFolioExecute;
                        oBitacora.sEvento = "POST^" + stURLWS.Trim();
                        oBitacora.sObservacion = "[obtenerDetalle-Referencia]";
                        oBitacora.sObservacion += "POST al servicio(webspeed)" + stOpcionRecibos + " Time:" + Time;
                        oBitacora.sInputParams = string.Empty;
                        oBitacora.sIPCliente = "0.0.0.0";
                        _iIdBitacora = oDaoDatos.GuardarBitacora(oBitacora, listDetalleBit);
                        listDetalleBit.Clear();

                        sLinealog = oBitacora.sObservacion;

                        Utilerias.guardarLogEventos(sLinealog);

                        oBitacora = null;
                        oDaoDatos = null;
                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


                        switch (oHttpResp.StatusCode)
                        {
                            case HttpStatusCode.OK:
                                {
                                    oJsonPost.lError = false;
                                    using (StreamReader oReader = new StreamReader(oHttpResp.GetResponseStream(), Encoding.GetEncoding("iso-8859-1")))
                                    {
                                        cJsonResponse = oReader.ReadToEnd();

                                        listDetalleBit.Add(
                                            Utilerias.CrearDetalleBitacora(_iIdBitacora, "JSON", cJsonResponse, Constantes.DET_BIT_OUTPUT)
                                        );

                                        try
                                        {
                                            oJsonPost.oJsonData = JObject.Parse(cJsonResponse);
                                            if (oJsonPost.oJsonData["iError"] != null && oJsonPost.oJsonData["cError"] != null)
                                            {
                                                oJsonPost.iError = oJsonPost.oJsonData.Value<int>("iError");
                                                oJsonPost.lError = (oJsonPost.iError > 0 ? true : false);
                                                oJsonPost.cError = oJsonPost.oJsonData.Value<string>("cError");
                                                if (oJsonPost.oJsonData["cMensaje"] != null)
                                                {
                                                    oJsonPost.cMensaje = oJsonPost.oJsonData.Value<string>("cMensaje");
                                                }//fin:if 
                                            }//fin:fin
                                            else
                                            {
                                                _cError = "El servicio no retorno una estructura de datos correcta(JSON inválido).";
                                                oJsonPost.lError = true;
                                                oJsonPost.cError = _cError;
                                            }//fin:else                                                                                        
                                        }//fin:if
                                        catch (JsonReaderException ex)
                                        {
                                            _cError = "El servicio no devolvio una respuesta valida:[" + ex.Message + "]";
                                            oJsonPost.lError = true;
                                            oJsonPost.cError = _cError;
                                        }
                                    }//fin:using
                                }//fin:case
                                break;
                            default:
                                {
                                    _cError = "HttpStatusCode=>" + oHttpResp.StatusCode.ToString();
                                    oJsonPost.lError = true;
                                    oJsonPost.cError = _cError;

                                    listDetalleBit.Add(
                                        Utilerias.CrearDetalleBitacora(
                                            _iIdBitacora, "ERROR", "HttpStatusCode^" + oHttpResp.StatusCode.ToString(), Constantes.DET_BIT_OUTPUT
                                        )
                                    );
                                }//fin:default
                                break;
                        }//fin:switch (oHttpResponse.StatusCode)
                    }//Fin:Using
                }//fin:try
                catch (WebException excep)
                {
                    string responseContent = "";
                    _cError = excep.Message;

                    WebResponse errResp = excep.Response;
                    using (Stream respStream = errResp.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(respStream);
                        responseContent = reader.ReadToEnd();
                    }//fin:using (Stream respStream = errResp.GetResponseStream())

                    _cError += "|" + responseContent;
                    oJsonPost.cError = _cError;
                    oJsonPost.lError = true;

                    listDetalleBit.Add(
                       Utilerias.CrearDetalleBitacora(_iIdBitacora, "ERROR[WEB EXCEPCION]", _cError, Constantes.DET_BIT_OUTPUT)
                    );
                }//Fin:catch

                //Guardar bitacora con la respuesta de la peticion
                if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                listDetalleBit.Clear(); listDetalleBit = null;
                oDaoDatos = null;

            }//fin:try
            catch (Exception ex)
            {
                _cError += " | " + ex.Message;
                oJsonPost.cError = _cError;
                oJsonPost.lError = true;
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
                if (_iIdBitacora > 0)
                {
                    if (listDetalleBit == null) listDetalleBit = new List<DTODetalleBitacora>();
                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Message en [ObtenerDetalle-Referencia]",
                             ex.Message,
                             Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Source en [ObtenerDetalle-Referencia]",
                            ex.Source,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].Target en en [ObtenerDetalle-Referencia]",
                            ex.TargetSite.ToString(),
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    listDetalleBit.Add(
                        Utilerias.CrearDetalleBitacora(
                            _iIdBitacora,
                            "ERROR[EXCEPCION].StackTrace en [ObtenerDetalle-Referencia]",
                            ex.StackTrace,
                            Constantes.DET_BIT_OUTPUT
                        )
                    );

                    if (oDaoDatos == null) oDaoDatos = new DaoConsultas();
                    oDaoDatos.GuardarDetalleBitacora(listDetalleBit);
                    listDetalleBit.Clear(); listDetalleBit = null;
                    oDaoDatos = null;
                }//fin:if (_iIdBitacora > 0)
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                if (oDaoDatos != null) oDaoDatos = null;
            }//fin:catch
            return oJsonPost;
        }//fin:if ObtenerDetalleReferencia
        //-----------------------------------------------------------------------------------------
        public static string convertBase10(string cLineaReferencia)
        {

            string Base10 = string.Empty;
            int valor = 0;
            int residuo = 0;
            int DigVer = 0;
            int count = 1;
            int resultado = 0;

            try
            {

                char[] invertida = cLineaReferencia.ToCharArray();
                Array.Reverse(invertida);
                List<int> suma = new List<int>();
                if (cLineaReferencia.Length > 29 || cLineaReferencia.Length < 29)
                {
                    Base10 = "La linea de referencia esta fuera del rango de caracteres permitidos [29]";
                    return Base10;
                }//fin: if
                foreach (var dato in invertida)
                {
                    valor = Convert.ToInt32(dato.ToString());
                    if (count == 1)
                    {
                        resultado = valor * 2;
                        if (resultado >= 10)
                        {
                            while (resultado != 0)
                            {
                                residuo += resultado % 10;
                                resultado /= 10;
                            }//fin: while
                            resultado = residuo;
                            residuo = 0;
                        }//fin: if
                        suma.Add(resultado);
                        count = 2;
                    }//fin: if

                    else
                    {
                        suma.Add(valor * 1);
                        count = 1;
                    }//fin: else
                }//fin: foreach
                valor = suma.Sum();
                int division = valor % 10;
                if (division == 0)
                {
                    DigVer = division;
                }//fin: if
                else
                {
                    DigVer = 10 - division;
                }//fin: if
                Base10 = cLineaReferencia + DigVer.ToString();
            }//fin: try
            catch { throw; }
            return Base10;
        }//fin:

    }//fin:class
}