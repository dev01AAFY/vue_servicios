﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsVentanillaUnicaREST.Util {
    public class Constantes {

        public const string LOG_FILENAME = "vue_services_rest.log";

        public const int TIMEOUT_DEFAULT = 300000;

        public const string WTIMEOUT_REQUEST = "WTIMEOUT_REQUEST";   
        public const string DES_CONEXION_DB  = "DES_CONEXION_DB";
        public const string PROD_CONEXION_DB = "PROD_CONEXION_DB";

        public const string DB_CONEXION = "DB_CONEXION";

        public const string APPKEY_VALIDA_MODULO = "MOD_WEB_ACCESO_PDP";

        public const string WJSON_CONFIG_SMTP = "WJSON_CONFIG_SMTP";

        public const string DET_BIT_INPUT  = "INPUT";
        public const string DET_BIT_OUTPUT = "OUTPUT";

        public const string _APP_AMBIENTE = "APP_AMBIENTE";

        public const string _DESARROLLO = "DESARROLLO";
        public const string _PRODUCCION = "PRODUCCION";
        public const string _TEST_PRODUCCION = "TEST_PRODUCCION";

        public const string DIRECTORIO_LOG = "DIRECTORIO_LOG";
        public const string REPORTE_VEHICULAR = "REPORTE_VEHICULAR";
        public const string REPORTE_FICHA_PAGO = "REPORTE_FICHA_PAGO";
        public const string RUTA_IMAGEN_AAFY = "RUTA_IMAGEN_AAFY";
        public const string RUTA_IMAGEN_YUC = "RUTA_IMAGEN_YUC";
        public const string RUTA_IMAGEN_CONVENIOS = "RUTA_IMAGEN_CONVENIOS";
        public const string TITULO_REPORTE = "TITULO_REPORTE";
        public const string TITULO_FICHA_PAGO = "TITULO_FICHA_PAGO";

        public const string TOTAL_CEROS_IMPORTE = "TOTAL_CEROS_IMPORTE";
        public const string GUARDAR_PDF = "GUARDAR_PDF";
        public const string RUTA_FILESYSTEM_PDF = "RUTA_FILESYSTEM_PDF";
        public const string MASCARA_LINEAREF = "MASCARA_LINEAREF";

        public const string CONFIG_FORMAT_RECIBO = "{0}^{1}^{2}^{3}^{4}^{5}^{6}^{7}^{8}";
        public const string ATRIBUTO_UNICO_ETIQ  = "IDENTIFICADOR UNICO";

        public const string WS_OP_OBTENER_DET_REFERENCIA = "obtenerDetalleReferencia";
        public const string WS_OPCION_DETALLEREFERENCIA = "getDetalleReferencia";
        public const string WS_OPCION_VALIDAR_APPKEY     = "validarAppKey";
        public const string WS_OPCION_GENERAR_REF_GN     = "generarReferenciaGN";
        public const string WS_OPCION_GENERAR_REF_VU     = "generarReferenciaVU";
        public const string WS_OPCION_VERIFICAR_REF_VU   = "verificarReferenciaVU";

        public const string VUE_APPKEY_HABILITADAS_SERVICIO = "VUE_APPKEY_HABILITADAS_SERVICIO";

        //Constantes Desarrollo
        public const string DES_URL_GENERICOPAGOS   = "DES_URL_GENERICOPAGOS";
        public const string DES_URL_RECIBOSPAGOSWEB = "DES_URL_RECIBOSPAGOSWEB";
        public const string DES_URL_RECIBO = "DES_URL_RECIBO";
        public const string DES_URL_SERVICIOS_VUE   = "DES_URL_SERVICIOS_VUE";

        //Constantes Producción        
        public const string PROD_URL_GENERICOPAGOS   = "PROD_URL_GENERICOPAGOS";
        public const string PROD_URL_RECIBOSPAGOSWEB = "PROD_URL_RECIBOSPAGOSWEB";
        public const string PROD_URL_RECIBO = "PROD_URL_RECIBO";
        public const string PROD_URL_SERVICIOS_VUE   = "PROD_URL_SERVICIOS_VUE";

        //Varaibles para la claves de los Mensajes
        public const string KEY_MSG_ERROR_EXCEPCION = "e00_excepcion_general";

        public const string KEY_MSG_ERROR_99 = "e99_hoja_excepcion";
        public const string KEY_MSG_ERROR_01 = "e01_hoja_refsinnumeros";
        public const string KEY_MSG_ERROR_02 = "e02_hoja_appkeyinvalido";
        public const string KEY_MSG_ERROR_03 = "e03_hoja_configuracion";
        public const string KEY_MSG_ERROR_05 = "e05_hoja_agrupador";
        public const string KEY_MSG_ERROR_06 = "e06_hoja_errorbarcode";
        public const string KEY_MSG_ERROR_07 = "e07_hoja_error_pdf";
        public const string KEY_MSG_ERROR_08 = "e08_hoja_sinconceptos";
        public const string KEY_MSG_ERROR_09 = "e09_hoja_no_appkey";
        public const string KEY_MSG_ERROR_10 = "e01_ws_consultar_detalleref";
        public const string KEY_MSG_ERROR_11 = "e01_ws_consultar_recibos";
        public const string KEY_MSG_ERROR_12 = "e02_ws_consultar_recibos";
        public const string KEY_LR_VENCIDA = "ews_ref_vencida";
        public const string KEY_LR_NOENCONTRADA = "ews_ref_nofound";
        public const string KEY_MSG_ERROR_13 = "e04_ws_consultar_ref";


        public const string KEY_MSG_WSCONSULTAR_REF_01 = "e01_ws_consultar_ref";
        public const string KEY_MSG_WSCONSULTAR_REF_02 = "e02_ws_consultar_ref";
        public const string KEY_MSG_WSCONSULTAR_REF_03 = "e03_ws_consultar_ref";

        public const string KEY_MSG_ERROR_WSGENERARREF_01 = "e01_ws_generar_refgn";
        public const string KEY_MSG_ERROR_WSGENERARREF_02 = "e02_ws_generar_refgn";
        public const string KEY_MSG_ERROR_WSGENERARREF_03 = "e03_ws_generar_refgn";

        public const string HEADER_CONTENT_TYPE = "Content-Type";

        public const string MIME_TYPE_PDF = "application/pdf";
        public const string MIME_TYPE_JSON = "text/json";

        public const string VUE_PREFIJOS_PERMITIDOS = "VUE_PREFIJOS_PERMITIDOS";
        public const string DES_URL_PDF_RECIBO = "DES_URL_PDF_RECIBO";
        public const string PROD_URL_PDF_RECIBO = "PROD_URL_PDF_RECIBO";
        public const string DES_URL_DATOS_RECIBO = "DES_URL_DATOS_RECIBO";
        public const string PROD_URL_DATOS_RECIBO = "PROD_URL_DATOS_RECIBO";

        public enum eTipoDocumentosPDF {
            FICHA_PAGO = 1, HOJA_ATENCION = 2
        };

        public const char SEPARADOR_DIPTONGO = 'æ';

        public const string EMAIL_TO_ERRORES = "EMAIL_TO_ERRORES";

        public const string SP_WSVU_INS_BITACORA = "dbo.sp_wsvu_insert_bitacora";
        public const string SP_WSVU_INS_DET_BITACORA = "dbo.sp_wsvu_insert_detbitacora";

        public const string ERROR_POST_PARAMS_EMPTY = "El servicio no recibo ningún parámetro.";

        public const string ERROR_POST_PARAMS_NO_REQUERIDOS = "El servicio no recibió los parámetros requeridos.";

        public const string VUE_JSON_PAGO_OXXO = "VUE_JSON_PAGO_OXXO";

        public const string VUE_CAJAS_DIGITALES = "VUE_CAJAS_DIGITALES";

    }//fin:class
}