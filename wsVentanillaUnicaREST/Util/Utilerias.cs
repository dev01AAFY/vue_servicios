﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ZXing;
using System.Drawing.Imaging;
using wsVentanillaUnicaREST.Models;
//using System.Web;

namespace wsVentanillaUnicaREST.Util {
    public class Utilerias {
        //---------------------------------------------------------------------
        public static Dictionary<string, string> ObtenerVariablesConfig() {

            string stConfigKey = string.Empty;
            Dictionary<string, string> oDictParams = new Dictionary<string, string>();

            try {
                ConfigurationManager.RefreshSection("appSettings");

                if (ConfigurationManager.AppSettings.Count > 0) {

                    //Parametro para coneccion a base de Datos
                    oDictParams.Add(Constantes.DB_CONEXION, "");

                    //Obtener variables de Entorno para la aplicacion
                    stConfigKey = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE];
                    switch (stConfigKey) {
                        case Constantes._DESARROLLO:
                            //Conecion a la base de datos
                            if (ConfigurationManager.AppSettings[Constantes.DES_CONEXION_DB] != null) {
                                oDictParams[Constantes.DB_CONEXION] =
                                    ConfigurationManager.AppSettings[Constantes.DES_CONEXION_DB].Trim();
                            }//fin:if                            
                            break;
                        case Constantes._TEST_PRODUCCION:
                        case Constantes._PRODUCCION:
                            //Conecion a la base de datos
                            if (ConfigurationManager.AppSettings[Constantes.PROD_CONEXION_DB] != null) {
                                oDictParams[Constantes.DB_CONEXION] =
                                    ConfigurationManager.AppSettings[Constantes.PROD_CONEXION_DB].Trim();
                            }//fin:if                                                        
                            break;
                        default:
                            //Conecion a la base de datos
                            if (ConfigurationManager.AppSettings[Constantes.DES_CONEXION_DB] != null) {
                                oDictParams[Constantes.DB_CONEXION] =
                                    ConfigurationManager.AppSettings[Constantes.DES_CONEXION_DB].Trim();
                            }//fin:if                                                         
                        break;
                    }//fin:switch

                    oDictParams.Add(
                        Constantes._APP_AMBIENTE,
                        ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim()
                    );
                }//fin:if
                return oDictParams;
            }
            catch { throw; }
        }//fin:obtenerConfiguracion
        //---------------------------------------------------------------------        
        public static bool grabarLog(string _path, string _linea) {

            string stFullFileLog = "";
            string stLogMessage = "";
            bool lSuccess = true;

            try {
                if (!string.IsNullOrEmpty(_path)) {
                    //stFullFileLog = System.IO.Path.Combine(_path, LOG_FILENAME);
                    Encoding EncodeLog = Encoding.GetEncoding("ISO-8859-1");
                    //Encoding EncodeLog = Encoding.UTF8;
                    stFullFileLog = Path.Combine(_path, (DateTime.Now.ToString("yyyyMMdd") + "_" + Constantes.LOG_FILENAME));
                    stLogMessage = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] " + _linea + Environment.NewLine;
                    if (!Directory.Exists(_path)) { Directory.CreateDirectory(stFullFileLog); }
                    using (FileStream fs = new FileStream(stFullFileLog, FileMode.Append, FileAccess.Write)) {
                        using (StreamWriter sw = new StreamWriter(fs, EncodeLog)) {
                            sw.Write(stLogMessage);
                            sw.Close();
                        }//Fin:using(StreamWriter sw = new StreamWriter(fs))    
                    }//fin:using
                }//fin:if                
            }//fin:try 
            catch {; }
            return lSuccess;
        }//fin:grabarLog
        //-----------------------------------------------------------------------------------------
        public static void guardarLogEventos(string sLineaLog) {

            string stRutaLog = string.Empty;

            ConfigurationManager.RefreshSection("appSettings");
            if (ConfigurationManager.AppSettings[Constantes.DIRECTORIO_LOG] != null) {
                stRutaLog = ConfigurationManager.AppSettings[Constantes.DIRECTORIO_LOG].Trim();
            }//fin:if

            if (!string.IsNullOrEmpty(stRutaLog)) {
                Utilerias.grabarLog(stRutaLog, sLineaLog);
            }//fin:if                                
        }//fin:GuardarLogEventos
        //-----------------------------------------------------------------------------------------
        public static TPostResponse obtenerConfigMensaje(string _sFileName, string _KeyMensaje) {

            string sKeyName = string.Empty; 
            JArray aJsonValues = null;
            TPostResponse oMensaje = new TPostResponse();

            //{ "key": "", "iError":00, "msg": "" }

            try {
                //Obtener la configuración de un archivo
                oMensaje.Clear();
                using (StreamReader oStreamRead = File.OpenText(_sFileName)) {
                    using (JsonTextReader oJReader = new JsonTextReader(oStreamRead)) {
                        aJsonValues = JArray.Parse(JToken.ReadFrom(oJReader).ToString());

                        foreach (JObject oJsonConfig in aJsonValues.Children<JObject>()) {
                            sKeyName = oJsonConfig.Value<string>("key");

                            if (sKeyName.Equals(_KeyMensaje)) {                               
                                oMensaje.iError = oJsonConfig.Value<int>("ierror");
                                oMensaje.cError = oJsonConfig.Value<string>("msg");
                                break;
                            }//fin:id                            
                        }//fin:foreach
                    }//fin:if
                }//fin: using (StreamReade)                
            }//fin:try
            //catch (Exception oEx)
            catch {
                oMensaje.Clear();
                //_sOutError = "Error en [obtenerConfigReportes] : ";
                //_sOutError += "Message^" + oEx.Message;
                //_sOutError += "~Source^" + oEx.Source;
                //_sOutError += "~Target^" + oEx.TargetSite.ToString();
                //_sOutError += "~StackTrace^" + oEx.StackTrace;                
            }

            return oMensaje;

        }//fin:obtenerConfigMensaje
        //-----------------------------------------------------------------------------------------
        public static Dictionary<string, string> obtenerConfigReporte(string _sFileName, string _sClave, ref string _sOutError) {

            string sKeyName = string.Empty;
            Dictionary<string, string> oDictValues = new Dictionary<string, string>();
            JArray aJsonValues = null;

            //{ "": "total_ceros_importe", "value": "9" },
            try {
                //Obtener la configuración de un archivo
                using (StreamReader oStreamRead = File.OpenText(_sFileName)) {
                    using (JsonTextReader oJReader = new JsonTextReader(oStreamRead)) {
                        aJsonValues = JArray.Parse(JToken.ReadFrom(oJReader).ToString());

                        foreach (JObject oJsonConfig in aJsonValues.Children<JObject>()) {
                            sKeyName = oJsonConfig.Value<string>("key");
                            if (string.IsNullOrEmpty(_sClave)) {
                                oDictValues.Add(sKeyName, oJsonConfig.Value<string>("value"));
                            }//fin:if
                            else {
                                if (sKeyName.Equals(_sClave)) {
                                    oDictValues.Add(sKeyName, oJsonConfig.Value<string>("value"));
                                }//fin:id
                            }//fin:else
                        }//fin:foreach
                        //aJsonValues = JArray.Parse();
                        //JObject o2 = (JObject)JToken.ReadFrom(reader);
                    }//fin:if
                }//fin: using (StreamReade)                
            }//fin:try
            catch (Exception oEx) {
                _sOutError = "Error en [obtenerConfigReportes] : ";
                _sOutError += "Message^" + oEx.Message;
                _sOutError += "~Source^" + oEx.Source;
                _sOutError += "~Target^" + oEx.TargetSite.ToString();
                _sOutError += "~StackTrace^" + oEx.StackTrace;
                oDictValues.Clear();
            }

            return oDictValues;
        }//fin:if 
        //-----------------------------------------------------------------------------------------
        public static string generarImageBarcode(
            int _iTotalCerosImporte, double _dTotal, string _sLineaReferencia, string _sRutaFileSystemTemp, ref string _sOutError) {

            string sTextoAux = string.Empty;
            string stBarCodePrincipal = string.Empty;
            string sFileNameBarCode = string.Empty;

            BarcodeWriter oBarCodeZXing = null;

            try {
                
                if(_iTotalCerosImporte > 0)
                {
                    //Generar el Codigo de Barras para la Referencia Principal
                    sTextoAux = (string.Format("{0:0.00}", _dTotal)).Replace(".", "");
                    stBarCodePrincipal = _sLineaReferencia + sTextoAux.PadLeft(_iTotalCerosImporte, '0');
                }
                else
                {
                    stBarCodePrincipal = _sLineaReferencia.Trim();
                }      

                //Generar la imagen correspondiente al codigo de barras
                oBarCodeZXing = new BarcodeWriter();
                oBarCodeZXing.Options.PureBarcode = true;
                oBarCodeZXing.Format = BarcodeFormat.CODE_128;

                sFileNameBarCode = Path.Combine(
                    _sRutaFileSystemTemp,
                    _sLineaReferencia + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.fff") + ".png"
                //oDataReferencia.cRefPrincipal + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.fff") + ((new Random()).Next(0, 100)).ToString() + ".png"
                );

                oBarCodeZXing.Write(stBarCodePrincipal).Save(sFileNameBarCode, ImageFormat.Png);
                oBarCodeZXing = null;

                if (!File.Exists(sFileNameBarCode)) {
                    sFileNameBarCode = string.Empty;
                    _sOutError = "El archivo correspondiente al código de barras no esta disponible en el servidor.";
                }//fin:if (File.Exists(sFileNameBarCode))              
            }//fin:try
            catch (Exception oExc) {
                _sOutError = "Error en [generarImageBarcode] : ";
                _sOutError += "Message^" + oExc.Message;
                _sOutError += "~Source^" + oExc.Source;
                _sOutError += "~Target^" + oExc.TargetSite.ToString();
                _sOutError += "~StackTrace^" + oExc.StackTrace;
            }

            return sFileNameBarCode;
        }//fin: generarImageBarcode
        //-----------------------------------------------------------------------------------------
        public static bool eliminarArchivoFileSystem(string _sFileName, ref string _sOutError) {

            bool lSuccess = true;

            _sOutError = string.Empty;

            if (File.Exists(_sFileName)) {
                try { File.Delete(_sFileName); lSuccess = true; }
                catch (Exception oExcFile) {
                    _sOutError = "Error al eliminar el archivo" + Path.GetFileName(_sFileName) + ":" + oExcFile.Message;
                    lSuccess = false;
                }//fin:catch
            }//fin:if (File.Exists(sFileNameBarCode))
            return lSuccess;
        }//fin:eliminarArchivoFileSystem              
        //-----------------------------------------------------------------------------------------
        public static DTODetalleBitacora CrearDetalleBitacora(
            int _iIdBitacora, string _sAtributo, string _cValor, string _cTipo) {

            DTODetalleBitacora oDetalleBit = new DTODetalleBitacora();

            oDetalleBit.iIdBitacora = _iIdBitacora;
            oDetalleBit.sAtributo = _sAtributo;
            oDetalleBit.sValor = _cValor;
            oDetalleBit.sOrigen = _cTipo;
            //oDetalleBit.dtFechaAlta = DateTime.Now;

            return oDetalleBit;
        }//fin:CrearDetalleBitacora
        //-----------------------------------------------------------------------------------------
    }//fin:Utilerias
}