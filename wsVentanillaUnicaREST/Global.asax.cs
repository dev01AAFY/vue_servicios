﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace wsVentanillaUnicaREST{

    public class WebApiApplication : System.Web.HttpApplication{
        //---------------------------------------------------------------------
        protected void Application_Start(){

            GlobalConfiguration.Configure(WebApiConfig.Register);

            //Agregamos el soporte para Json
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.MediaTypeMappings.Add(
                new QueryStringMapping("json", "true", "application/json")
            );
        }//fin:Application_Start
        //---------------------------------------------------------------------
    }//fin:class
}
