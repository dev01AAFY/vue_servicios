﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

namespace wsVentanillaUnicaREST.Models {
    public class TJsonRespuesta:TPostResponse {
        //---------------------------------------------------------------------
        public JObject oJsonData { get; set; }
        //---------------------------------------------------------------------
        public TJsonRespuesta() :base() {
            this.oJsonData = null;
        }//fin:constructor
        //---------------------------------------------------------------------
    }//fin:class
}