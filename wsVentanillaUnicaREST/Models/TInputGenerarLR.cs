﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsVentanillaUnicaREST.Models {
    public class TInputGenerarLR {       
        public int iIdTramite { get; set; }
        public int iIdDependencia { get; set; }
        public int iIdSubDependencia { get; set; }
        public string sImporteTramite { get; set; }
        //public string sIdentificadorUnico { get; set; }
        public string sAppKey { get; set; }
        public string sFechaVigenciaLR { get; set; }
        public string sDatosRecibos { get; set; }
        public string sAtributos { get; set; }
        public string sFolioExecute { get; set; }
        //---------------------------------------------------------------------
        public TInputGenerarLR() {
        }//fin:TInputGenerarLR
        //---------------------------------------------------------------------
        public void Clear() {

            this.iIdTramite = 0;
            this.iIdDependencia = 0;
            this.iIdSubDependencia = 0;
            this.sFolioExecute = string.Empty;
            this.sAppKey = string.Empty;
            this.sFechaVigenciaLR = string.Empty;
            this.sDatosRecibos = string.Empty;
            this.sAtributos = string.Empty;
            this.sImporteTramite = string.Empty;
        }//fin:Clear
        //---------------------------------------------------------------------
    }//fin:oRecibo
}