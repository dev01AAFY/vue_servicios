﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsVentanillaUnicaREST.Models {
    public class DTOBitacora {
        //Atributos
        public int iIdBitacora { get; set; }
        public string sFolio { get; set; }
        public string sIPCliente { get; set; }
        public string sHttpUserAgent { get; set; }
        public string sEvento { get; set; }
        public string sInputParams { get; set; }        
        public string sObservacion { get; set; }                
        //---------------------------------------------------------------------       
        public DTOBitacora() {

            this.iIdBitacora = 0;
            this.sFolio = string.Empty;
            this.sIPCliente = string.Empty;                        
            this.sHttpUserAgent = string.Empty;
            this.sEvento = string.Empty;
            this.sInputParams = string.Empty;
            this.sObservacion = string.Empty;

        }//fin:constructor
        //---------------------------------------------------------------------
        /*
        public void Assign(DTOBitacora _oObjeto) {
            this.iIdBitacora = _oObjeto.iIdBitacora;
            this.sFolio = _oObjeto.sFolio;
            this.cIPPrivada = _oObjeto.cIPPrivada;
            this.cIPPublica = _oObjeto.cIPPublica;
            this.cIdSesion = _oObjeto.cIdSesion;
            this.cEvento = _oObjeto.cEvento;
            this.cObservacion = _oObjeto.cObservacion;
            this.cHttpUserAgent = _oObjeto.cHttpUserAgent;
        }//fin:Assign
        */
        //---------------------------------------------------------------------
    }//fin:class
}