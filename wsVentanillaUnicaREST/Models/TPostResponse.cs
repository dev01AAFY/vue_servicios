﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsVentanillaUnicaREST.Models {
    public class TPostResponse {
        public bool lError { get; set; }
        public int iError { get; set; }
        public string cError { get; set; }
        public string cMensaje { get; set; }
        //---------------------------------------------------------------------                       
        public TPostResponse() {
            this.lError = false;
            this.iError = 0;
            this.cError = string.Empty;
            this.cMensaje = string.Empty;
        }//fin:
        //---------------------------------------------------------------------
        public void Clear() {
            this.lError = false;
            this.iError = 0;
            this.cError = string.Empty;
            this.cMensaje = string.Empty;
        }//fin:clear
        //---------------------------------------------------------------------
    }//fin:class
}