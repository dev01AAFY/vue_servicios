﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsVentanillaUnicaREST.Models {
    public class DTODetalleBitacora {
        //---------------------------------------------------------------------       
        //Atributos
        public int iIdBitacora { get; set; }
        public string sAtributo { get; set; }
        public string sValor { get; set; }
        public string sOrigen { get; set; }
        //public DateTime dtFechaAlta { get; set; }
        //---------------------------------------------------------------------
        public DTODetalleBitacora() {
            this.iIdBitacora = 0;
            this.sAtributo = string.Empty;
            this.sValor = string.Empty;
            this.sOrigen = string.Empty;
            //this.dtFechaAlta = DateTime.Now;
        }//fin:constructor
        //---------------------------------------------------------------------
    }//fin:class
}