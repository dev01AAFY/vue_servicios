﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsVentanillaUnicaREST.Models {
    public class DTOConfiguracion {
        
        //Atributos
        public int iIdConfig { get; set; }
        public string cClave { get; set; }
        public string cValor { get; set; }
        public string cDescripcion { get; set; }
        public DateTime dtFechaAlta { get; set; }
        public DateTime? dtFechaModificacion { get; set; }
        public bool lActivo { get; set; }
        public string cTipoDato { get; set; }

        public bool lError { get; set; }
        public string cError { get; set; }
        //---------------------------------------------------------------------
        public DTOConfiguracion() {

            this.iIdConfig = 0;
            this.cClave = "";
            this.cValor = "";
            this.cDescripcion = "";
            this.lActivo = false;
            this.cTipoDato = "";
            this.lError = false;
            this.cError = "";
        }//fin:
        //---------------------------------------------------------------------
    }//fin:class
}