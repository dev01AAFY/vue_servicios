﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wsVentanillaUnicaREST.Util;
using wsVentanillaUnicaREST.Models;
using System.Data.SqlClient;
using System.Data;

namespace wsVentanillaUnicaREST.dao {
    public class DaoConsultas {
        //---------------------------------------------------------------------
        private string Conexion() {

            string stCadenaConnect = string.Empty;

            Dictionary<string, string> dictConfig = Utilerias.ObtenerVariablesConfig();
            if (dictConfig.Count > 0) {
                if (dictConfig.ContainsKey(Constantes.DB_CONEXION)) {
                    stCadenaConnect = dictConfig[Constantes.DB_CONEXION];
                }//fin:if
            }//fin:if                        
            return stCadenaConnect;
        }//fin:Conexion
        //---------------------------------------------------------------------
        private DaoGenerico oDaoGenerico;
        //---------------------------------------------------------------------
        //Constructor        
        public DaoConsultas() {
            this.oDaoGenerico = new DaoGenerico();
        }//fin:DatosMotorPagos
        //---------------------------------------------------------------------
        public DTOConfiguracion ObtenerValorConfig(string _cClave) {

            string stConnection = string.Empty;
            string stQuerySelect = string.Empty;

            DTOConfiguracion oParam = new DTOConfiguracion();

            try {
                stConnection = this.Conexion();
                using (SqlConnection oConn = new SqlConnection(stConnection)) {
                    try {

                        DataSet dtsConfig = new DataSet();
                        SqlCommand cmd = new SqlCommand(ProcedureNames.SP_GETCONFIGURACION, oConn);

                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmd)) {

                            //Parametros de Entrada
                            sqlAdapter.SelectCommand.Parameters.AddWithValue("@cClave", _cClave);
                            sqlAdapter.SelectCommand.Parameters.AddWithValue("@iActivo", 1);

                            sqlAdapter.Fill(dtsConfig);
                            if (dtsConfig.Tables[0].Rows.Count > 0) {
                                DataRow oRow = dtsConfig.Tables[0].Rows[0];
                                oParam.iIdConfig = Int32.Parse(oRow["iIdConfig"].ToString()); //iIdConfig
                                oParam.cClave = oRow["cClave"].ToString();  //cClave
                                oParam.cValor = oRow["cValor"].ToString();//cValor
                                oParam.cDescripcion = oRow["cDescripcion"].ToString();//cDescripcion
                                oParam.dtFechaAlta = oRow.Field<DateTime>("dtFechaAlta");
                                oParam.dtFechaModificacion = (oRow["dtFechaModificacion"] == DBNull.Value) ?
                                    null : oRow.Field<DateTime?>("dtFechaModificacion");
                                oParam.lActivo = Convert.ToBoolean(oRow["lActivo"].ToString());
                                oParam.lError = false;
                                oParam.cError = "";
                            }//fin: if(dtDatos.Tables[0].Rows.Count > 0)          
                            else {
                                oParam.lError = true;
                                oParam.cError = "No fue posible obtener la variable de configuracion : " + _cClave;
                            }//fin:else
                        }//fin:using       
                        cmd.Dispose();
                        cmd = null;
                        dtsConfig = null;
                    }
                    catch { throw; }
                    finally { oConn.Close(); }
                }//fin:using 
            }
            catch (Exception ex) {
                oParam.lError = true;
                oParam.cError = "ocurrio un error en [obtenerValorConfig]:" + ex.Message;
            }
            return oParam;
        }//fin:obtenerValorConfig
        //---------------------------------------------------------------------
        public List<DTOConfiguracion> ObtenerConfiguraciones(List<string> _lstClaves) {

            List<DTOConfiguracion> listConfigs = new List<DTOConfiguracion>();

            DTOConfiguracion oItemConfig = null;

            try {
                foreach (string sKeyConfig in _lstClaves) {
                    oItemConfig = this.ObtenerValorConfig(sKeyConfig);
                    listConfigs.Add(oItemConfig);
                }//fin:foreach
            }
            catch {
                listConfigs = null;
                throw;
            }
            return listConfigs;
        }//fin:if ObtenerConfiguraciones               
        //---------------------------------------------------------------------
        public int GuardarBitacora(DTOBitacora oBitacora, List<DTODetalleBitacora> listDetalles) {

            int iPKBitacora = 0;
            string stConnection = this.Conexion();

            Dictionary<string, object> dictInputs = null;
            Dictionary<string, object> dictOutputs = null;

            using (SqlConnection oConn = new SqlConnection(stConnection)) {
                try {

                    dictInputs = new Dictionary<string, object>();
                    dictOutputs = new Dictionary<string, object>();

                    oConn.Open();

                    using (SqlTransaction oTrans = oConn.BeginTransaction()) {

                        try {                            
                            //Poblar parametros de Entrada
                            dictInputs.Clear();
                            dictInputs.Add("@cfolio", oBitacora.sFolio);
                            dictInputs.Add("@cipcliente", oBitacora.sIPCliente);
                            dictInputs.Add("@chttpuseragent", oBitacora.sHttpUserAgent);
                            dictInputs.Add("@cevento", oBitacora.sEvento);
                            dictInputs.Add("@cinputparams", 
                                string.IsNullOrEmpty(oBitacora.sInputParams) ? (object)DBNull.Value : oBitacora.sInputParams
                            );
                            dictInputs.Add("@cobservacion",
                                string.IsNullOrEmpty(oBitacora.sObservacion) ? (object)DBNull.Value : oBitacora.sObservacion
                            );
                            
                            //Poblar parametros de salida
                            dictOutputs.Clear();
                            dictOutputs.Add("@lError", false);
                            dictOutputs.Add("@cError", "");
                            dictOutputs.Add("@cSqlError", "");
                            dictOutputs.Add("@ifoliocreado", 0);

                            oDaoGenerico.executeSP(Constantes.SP_WSVU_INS_BITACORA, dictInputs, dictOutputs, oConn, oTrans);
                            if (Boolean.Parse(dictOutputs["@lError"].ToString())) {
                                throw new Exception(dictOutputs["@cError"].ToString());
                            }//fin: if (Boolean.Parse(ParamSalida["@cError"].ToString()))
                            else {
                                iPKBitacora = Convert.ToInt32(dictOutputs["@ifoliocreado"].ToString());
                            }//fin:else
                            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                            
                            //Iterar el listado de detalles para guardar en base de datos                            
                            foreach (DTODetalleBitacora oDet in listDetalles) {
                                oDet.iIdBitacora = iPKBitacora;
                                dictInputs.Clear();
                                dictInputs.Add("@iidbitacora", oDet.iIdBitacora);
                                dictInputs.Add("@catributo", oDet.sAtributo);
                                dictInputs.Add("@cvalor", oDet.sValor);
                                dictInputs.Add("@corigen", oDet.sOrigen);

                                //Poblar parametros de salida
                                dictOutputs.Clear();
                                dictOutputs.Add("@lError", false);
                                dictOutputs.Add("@cError", "");
                                dictOutputs.Add("@cSqlError", "");
                                dictOutputs.Add("@ifoliocreado", 0);

                                oDaoGenerico.executeSP(Constantes.SP_WSVU_INS_DET_BITACORA, dictInputs, dictOutputs, oConn, oTrans);
                                if (Boolean.Parse(dictOutputs["@lError"].ToString())) {
                                    throw new Exception(dictOutputs["@cError"].ToString());
                                }//fin: if (Boolean.Parse(ParamSalida["@cError"].ToString()))
                            }//fin:foreach (DTODetalleBitacora oDet in listDetalles)                            
                            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            oTrans.Commit();
                        }
                        catch {
                            oTrans.Rollback();
                            iPKBitacora = 0;
                            throw;
                        }
                    }//fin:using                                       
                    dictInputs.Clear(); dictInputs = null;
                    dictOutputs.Clear(); dictOutputs = null;
                }//fin:try
                catch (Exception ex) {
                    Utilerias.guardarLogEventos("Ocurrio un error al registrar la bitacora:" + ex.Message);
                    iPKBitacora = 0;
                }
                finally { oConn.Close(); }
            }//fin:using

            return iPKBitacora;
        }//fin:GuardarBitacora        
        //---------------------------------------------------------------------                     
        public bool GuardarDetalleBitacora(List<DTODetalleBitacora> listDetalles) {

            bool lSuccess = true;
            string stConnection = this.Conexion();

            Dictionary<string, object> dictInputs = null;
            Dictionary<string, object> dictOutputs = null;

            using (SqlConnection oConn = new SqlConnection(stConnection)) {
                try {

                    dictInputs = new Dictionary<string, object>();
                    dictOutputs = new Dictionary<string, object>();

                    oConn.Open();

                    using (SqlTransaction oTrans = oConn.BeginTransaction()) {

                        try {
                            //Iterar el listado de detalles para guardar en base de datos
                            foreach (DTODetalleBitacora oDet in listDetalles) {
                                if (oDet.iIdBitacora <= 0) continue;
                                dictInputs.Clear();
                                dictInputs.Add("@iidbitacora", oDet.iIdBitacora);
                                dictInputs.Add("@catributo", oDet.sAtributo);
                                dictInputs.Add("@cvalor", oDet.sValor);
                                dictInputs.Add("@corigen", oDet.sOrigen);

                                //Poblar parametros de salida
                                dictOutputs.Clear();
                                dictOutputs.Add("@lError", false);
                                dictOutputs.Add("@cError", "");
                                dictOutputs.Add("@cSqlError", "");
                                dictOutputs.Add("@ifoliocreado", 0);

                                oDaoGenerico.executeSP(Constantes.SP_WSVU_INS_DET_BITACORA, dictInputs, dictOutputs, oConn, oTrans);
                                if (Boolean.Parse(dictOutputs["@lError"].ToString())) {
                                    throw new Exception(dictOutputs["@cError"].ToString());
                                }//fin: if (Boolean.Parse(ParamSalida["@cError"].ToString()))
                            }//fin:foreach (DTODetalleBitacora oDet in listDetalles)
                            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            oTrans.Commit();
                        }
                        catch {
                            oTrans.Rollback();
                            lSuccess = false;
                            throw;
                        }
                    }//fin:using                                       
                    dictInputs.Clear(); dictInputs = null;
                    dictOutputs.Clear(); dictOutputs = null;
                }//fin:try
                catch (Exception ex) {
                    Utilerias.guardarLogEventos("Ocurrio un error al registrar los detalles de bitacora:" + ex.Message);
                    lSuccess = false;
                }
                finally { oConn.Close(); }
            }//fin:using

            return lSuccess;
        }//fin:GuardarDetalleBitacora                
        //---------------------------------------------------------------------
    }//fin:class
}