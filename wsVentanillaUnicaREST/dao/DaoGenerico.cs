﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;

namespace wsVentanillaUnicaREST.dao {
    public class DaoGenerico {
        //---------------------------------------------------------------------
        public DaoGenerico() { }
        //---------------------------------------------------------------------
        public T Load<T>(string SP, Dictionary<string, object> Param, SqlConnection oConn) {

            try {
                T oResultado = Activator.CreateInstance<T>();

                SqlCommand cmd = new SqlCommand();

                cmd.Connection = oConn;
                cmd.CommandText = SP;
                cmd.CommandType = CommandType.StoredProcedure;

                //Agregamos los parametros
                foreach (KeyValuePair<string, object> ParamEnt in Param) {
                    cmd.Parameters.AddWithValue(ParamEnt.Key, ParamEnt.Value);
                }//fin:foreach

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = cmd;

                DataSet dsDatos = new DataSet();
                dataAdapter.Fill(dsDatos);

                if (dsDatos.Tables[0].Rows.Count > 0) {
                    foreach (PropertyInfo oProp in oResultado.GetType().GetProperties()) {
                        if (dsDatos.Tables[0].Columns.Contains(oProp.Name)) {
                            if (dsDatos.Tables[0].Columns.Contains(oProp.Name)) {
                                if (dsDatos.Tables[0].Rows[0][oProp.Name] == DBNull.Value)
                                    oProp.SetValue(oResultado, null, null);
                                else
                                    //p.SetValue(r, value, null);
                                    oProp.SetValue(oResultado, dsDatos.Tables[0].Rows[0][oProp.Name], null);
                            }//Fin:
                        }//Fin:if
                    }//fin:foreach
                }//Fin:if
                return oResultado;
            }
            catch { throw; }
        }//fin:Load
        //-----------------------------------------------------------------------------------------
        public List<T> LoadLista<T>(string SP, Dictionary<string, object> Param, SqlConnection oConn) {

            try {
                List<T> listaResultados = new List<T>();

                SqlCommand cmd = new SqlCommand();

                cmd.Connection = oConn;
                cmd.CommandText = SP;
                cmd.CommandType = CommandType.StoredProcedure;

                //Agregamos los parametros
                foreach (KeyValuePair<string, object> ParamEnt in Param) {
                    cmd.Parameters.AddWithValue(ParamEnt.Key, ParamEnt.Value);
                }//fin:foreach

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = cmd;

                DataSet dsDatos = new DataSet();

                dataAdapter.Fill(dsDatos);

                if (dsDatos.Tables[0].Rows.Count > 0) {
                    //Recorremos el dataset
                    foreach (DataRow drDatos in dsDatos.Tables[0].Rows) {
                        //Creeamos un nuevo objeto T
                        T oResultado = Activator.CreateInstance<T>();

                        foreach (PropertyInfo oProp in oResultado.GetType().GetProperties()) {
                            //Si contiene el nombre de esa columna
                            if (dsDatos.Tables[0].Columns.Contains(oProp.Name)) {
                                if (drDatos[oProp.Name] == DBNull.Value)
                                    oProp.SetValue(oResultado, null, null);
                                else
                                    oProp.SetValue(oResultado, drDatos[oProp.Name], null);
                            }//fin:if (dsDatos.Tables[0].Columns.Contains(oProp.Name))
                        }//Fin:foreach

                        //Agregamos el objetos a la coleccion
                        listaResultados.Add(oResultado);
                    }//fin:foreach (DataRow drDatos in dsDatos.Tables[0].Rows)
                }//fin:if (dsDatos.Tables[0].Rows.Count > 0)
                return listaResultados;
            }
            catch { throw; }
        }//fin:LoadLista
        //-----------------------------------------------------------------------------------------                
        public void executeSP(string cProcedure, Dictionary<string, object> ParamEntrada,
            Dictionary<string, object> ParamSalida, SqlConnection oConn, SqlTransaction oTran) {

            try {
                SqlCommand cmd = new SqlCommand();

                cmd.Connection = oConn;
                cmd.Transaction = oTran;
                cmd.CommandText = cProcedure;
                cmd.CommandType = CommandType.StoredProcedure;

                //Agregamos los parametros de entrada
                foreach (KeyValuePair<string, object> parametersIn in ParamEntrada) {
                    cmd.Parameters.AddWithValue(parametersIn.Key, parametersIn.Value);
                }//fin:foreach

                //Agregamos los parametros de salida
                SqlParameter outParam = null;
                Type oType = null;
                foreach (KeyValuePair<string, object> parametersOut in ParamSalida) {
                    outParam = null;
                    oType = parametersOut.Value.GetType();
                    if (oType == typeof(int)) {
                        outParam = new SqlParameter(parametersOut.Key, SqlDbType.Int) { Direction = ParameterDirection.Output };
                    }//fin:if
                    else {
                        if (oType == typeof(string)) {
                            outParam = new SqlParameter(parametersOut.Key, SqlDbType.NVarChar, -1) { Direction = ParameterDirection.Output };
                        }//fin:else
                        else {
                            outParam = new SqlParameter(parametersOut.Key, parametersOut.Value) { Direction = ParameterDirection.Output };
                        }//fin:else
                    }//fin:else                  
                    if (outParam != null) { cmd.Parameters.Add(outParam); }
                }//fin:foreach

                //Ejecutamos el comando
                cmd.ExecuteNonQuery();

                //Obtenenos los key
                string[] keys = ParamSalida.Keys.ToArray();

                foreach (string NameParameter in keys) {
                    ParamSalida[NameParameter] = cmd.Parameters[NameParameter].Value;
                }//fin:foreach
            }
            catch { throw; }
        }//fin:executeSP
        //-----------------------------------------------------------------------------------------
        public DataSet Load(string SP, Dictionary<string, object> Param, SqlConnection oConn) {

            DataSet dsDatos = new DataSet();
            SqlCommand cmd = new SqlCommand();

            try {

                cmd.Connection = oConn;
                cmd.CommandText = SP;
                cmd.CommandType = CommandType.StoredProcedure;

                //Agregamos los parametros
                foreach (KeyValuePair<string, object> ParamEnt in Param) {
                    cmd.Parameters.AddWithValue(ParamEnt.Key, ParamEnt.Value);
                }
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = cmd;

                dataAdapter.Fill(dsDatos);

                return dsDatos;
            }
            catch { throw; }
        }//fin:Load
        //-----------------------------------------------------------------------------------------
    }//fin:class
}