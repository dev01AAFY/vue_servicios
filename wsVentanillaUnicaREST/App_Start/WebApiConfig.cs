﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;

namespace wsVentanillaUnicaREST{
    public static class WebApiConfig{

        public static void Register(HttpConfiguration config){

            // Configuración de rutas y servicios de API
            config.MapHttpAttributeRoutes();
          
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                //routeTemplate: "services/{controller}/{id}",
                //routeTemplate: "services/{controller}/{action}/{id}",
                //routeTemplate: "vus/services/{controller}/{action}/{id}",// DESARROLLO
                routeTemplate: "vue/services/{controller}/{action}/{id}",   //PRODUCCION
                defaults: new { id = RouteParameter.Optional }
            );

            //config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));

        }//fin:Register
    }
}
