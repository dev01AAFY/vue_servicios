﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using libGenerarPDF.clases;
using Microsoft.Reporting.WinForms;
using System.Text.RegularExpressions;
using System.IO;

namespace libGenerarPDF{
    public class GenerarPDF{
        //---------------------------------------------------------------------
        public GenerarPDF() { }
        //---------------------------------------------------------------------        
        public string crearPDFTramiteVU(
            TDataReporteVU _oDataReferencia,
            TDataTramite _oDatosTramite,
            List<TDataAtributos> _listAtributos,
            List<TDataConceptoRef> _listConceptos,
            TConfiguracion _oConfig,
            ref string _sOutError) {

            int iSizeParam = 0;

            string v_mimetype;
            string v_encoding;
            string v_filename_extension;
            string[] v_streamids;

            string sTextoAux = string.Empty;
            string sTotalTramite = "$ 0.00";
            string sErrorMetodo = string.Empty;
            string sBarCodePrincipal = string.Empty;
            string sContentBase64PDF = string.Empty;
            string sReferenciaConImporte = string.Empty;
            string sReferenciaConFormato = string.Empty;
            string sLRTramiteConFormato = string.Empty;

            string sFileNamePDF = string.Empty;
            string[] aRegexElementos = null;

            Warning[] warnings;
            ReportViewer rptViewer;

            ReportDataSource oRptDtsAtributos = null;
            ReportDataSource oRptDtsConceptos = null;
            ReportParameter[] aParamsReport = null;

            try {

                //Configuracion de los valores para el Reporte                     
                rptViewer = new ReportViewer();
                rptViewer.LocalReport.ReportPath = _oDataReferencia.cRutaReporte;
                rptViewer.LocalReport.DisplayName = "Tramites - PDF";
                rptViewer.LocalReport.EnableExternalImages = true;
                rptViewer.LocalReport.Refresh();

                sBarCodePrincipal = (string.Format("{0:0.00}", _oDataReferencia.dTotal)).Replace(".", "");
                sErrorMetodo = string.Empty;

                //Formatear la referencia principal para separar con guiones medios : 999-9999-9999-9999-9999-9999-9999
                //aRegexElementos = _oConfig.sMascaraLineaRef.Split('æ');
                aRegexElementos = _oConfig.sMascaraLineaRef.Split('~');
                sReferenciaConFormato = Regex.Replace(_oDataReferencia.cRefPrincipal, aRegexElementos[0], aRegexElementos[1]);                
                sLRTramiteConFormato = Regex.Replace(_oDatosTramite.cLineaReferencia, aRegexElementos[0], aRegexElementos[1]);

                if (string.IsNullOrEmpty(sErrorMetodo)) {
              
                    iSizeParam = rptViewer.LocalReport.GetParameters().Count;
                    aParamsReport = new ReportParameter[iSizeParam];
                    oRptDtsAtributos = new ReportDataSource("dtsAtributos", _listAtributos);

                    oRptDtsConceptos = new ReportDataSource("dtsConceptos", _listConceptos);

                    rptViewer.LocalReport.DataSources.Add(oRptDtsAtributos);
                    rptViewer.LocalReport.DataSources.Add(oRptDtsConceptos);
                    
                    sTotalTramite = "$ " + string.Format("{0:#,##0.00}", _oDatosTramite.dTotal);

                    aParamsReport[0] = new ReportParameter("ParamRefPrincipal", sBarCodePrincipal);
                    aParamsReport[1] = new ReportParameter("ParamTotalTramite", sTotalTramite);
                    
                    //Vigencia de la referencia
                    aParamsReport[2] = new ReportParameter("ParamVigenciaPago", _oDataReferencia.dtVigencia);

                    //Referencia principal sin importe
                    aParamsReport[3] = new ReportParameter("ParamRefPrincipalSinImp", sReferenciaConFormato);

                    //Seccion de Avisos
                    aParamsReport[4] = new ReportParameter("ParamAviso", _oDataReferencia.cAviso);

                    //Image Header AAFY
                    aParamsReport[5] = new ReportParameter("ParamImageAAFY", _oDataReferencia.cImageAAFY);

                    //Image Header Gobierno Yucatan
                    aParamsReport[6] = new ReportParameter("ParamImageGob", _oDataReferencia.cImageYUC);

                    //Titulo del reporte : ParamTituloReporte
                    aParamsReport[7] = new ReportParameter("ParamTituloReporte", _oDataReferencia.cTituloReporte);

                    //Folio de Solicitud
                    aParamsReport[8] = new ReportParameter("ParamFolioSolicitud", _oDataReferencia.cFolioSolicitud);

                    //Fecha de Solicitud
                    aParamsReport[9] = new ReportParameter("ParamFechaSolicitud", _oDataReferencia.dtFechaSolicitud);

                    //Nombre del tramite:
                    sTextoAux = String.Format("Trámite {0}: {1}", _oDatosTramite.iOrdenTramite.ToString(), _oDatosTramite.cNombreTramite);
                    aParamsReport[10] = new ReportParameter("ParamNomTramite", sTextoAux);

                    //ParamAnnio , ParamFolioTramite
                    aParamsReport[11] = new ReportParameter("ParamAnnio", _oDatosTramite.iAnioFiscal.ToString());

                    aParamsReport[12] = new ReportParameter("ParamFolioTramite", _oDatosTramite.cFolioTramite);

                    //Importe total en Letras
                    aParamsReport[13] = new ReportParameter("ParamTotalLetras", ".");

                    //Imagen de Convenios
                    aParamsReport[14] = new ReportParameter("ParamImageConvenios", _oConfig.sRutaImagenConvenios);

                    //Mensaje al ciudadano MensajeC1
                    aParamsReport[15] = new ReportParameter("ParamMensajeC1", _oConfig.sMensajeC1);

                    //Mensaje al ciudadano MensajeC1
                    aParamsReport[16] = new ReportParameter("ParamMensajeC2", _oConfig.sMensajeC2);

                    //Se agrega el codigo de barras con la linea de Referencia
                    aParamsReport[17] = new ReportParameter("ParamBarCodeLR", "file:///" + _oConfig.sRutaImagenBarCode);

                    //Fecha de impresion/generacion del documento
                    aParamsReport[18] = new ReportParameter("ParamFechaImpresion", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

                    aParamsReport[19] = new ReportParameter("ParamLRTramite", sLRTramiteConFormato);

                    //Parametro para indicar si tiene atributos                    
                    aParamsReport[20] = new ReportParameter("ParamTieneAtributos", _listAtributos.Count.ToString());

                    //Se agrega el codigo de barras con la linea de Referencia de 30 digitos
                    if (!string.IsNullOrEmpty(_oConfig.sRutaImagenBarCode30))
                    {
                        aParamsReport[21] = new ReportParameter("ParamBarCodeLR30", "file:///" + _oConfig.sRutaImagenBarCode30);
                        aParamsReport[22] = new ReportParameter("ParamRefPrincipal30", _oDataReferencia.cRefPrincipal30);
                    }
                    else
                    {
                        aParamsReport[21] = new ReportParameter("ParamBarCodeLR30", "");
                        aParamsReport[22] = new ReportParameter("ParamRefPrincipal30", "");
                    }

                    //Parametro de concepto Global                   
                    aParamsReport[23] = new ReportParameter("ParamConceptoGlobal", _oDataReferencia.cConceptoGlobal + " " + sTotalTramite);

                    rptViewer.LocalReport.SetParameters(aParamsReport);
                    rptViewer.LocalReport.Refresh();

                    //Generar el PDf
                    byte[] byteViewer = rptViewer.LocalReport.Render("PDF", null, out v_mimetype, out v_encoding,
                                    out v_filename_extension, out v_streamids, out warnings);

                    if (_oConfig.bGuardarPDF) {
                        if (string.IsNullOrEmpty(_oConfig.sFileSystemPDF)) {
                            sFileNamePDF = Path.Combine(
                                Path.GetDirectoryName(_oDataReferencia.cRutaReporte),
                                //_oDataReferencia.cRefPrincipal + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.ffff") + "_" + ((new Random()).Next(0, 100)).ToString() + ".pdf"
                                _oDataReferencia.cRefPrincipal + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.ffff") + ".pdf"
                            );
                        }//fin:if
                        else {
                            sFileNamePDF = Path.Combine(
                                _oConfig.sFileSystemPDF,
                                //_oDataReferencia.cRefPrincipal + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.ffff") + "_" + ((new Random()).Next(0, 100)).ToString() + ".pdf"
                                _oDataReferencia.cRefPrincipal + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.ffff") + ".pdf"
                            );
                        }//fin:else                       

                        using (FileStream fs = new FileStream(sFileNamePDF, FileMode.Create)) {
                            fs.Write(byteViewer, 0, byteViewer.Length);
                            fs.Flush();
                            fs.Close();
                        }//fin:if
                    }//fin:if (_oConfig.bGuardarPDF)

                    sContentBase64PDF = Convert.ToBase64String(byteViewer);

                    _sOutError = "";
                }//fin:if (string.IsNullOrEmpty(sErrorMetodo))
                else {
                    _sOutError = sErrorMetodo;
                }//fin:else              
            }//fin:try
            catch (Exception oEx) {
                _sOutError = "Error en [crearPDFTramiteVU] : ";
                _sOutError += "Message^" + oEx.Message;
                _sOutError += "~Source^" + oEx.Source;
                _sOutError += "~Target^" + oEx.TargetSite.ToString();
                _sOutError += "~StackTrace^" + oEx.StackTrace;
            }
            return sContentBase64PDF;
        }//fin:crearPDFTramiteVehicular
        //-----------------------------------------------------------------------------------------
        public string crearFichaPagoPDF(
            TDataReporteVU _oDataReferencia,
            List<TDataAtributos> _listAtributos,
            List<TDataConceptoRef> _listConceptos,
            TConfiguracion _oConfig,
            ref string _sOutError) {

            int iSizeParam = 0;

            string v_mimetype;
            string v_encoding;
            string v_filename_extension;
            string[] v_streamids;

            string sTextoAux = string.Empty;
            string sTotalFichaPago = "$ 0.00";
            string sErrorMetodo = string.Empty;
            string sBarCodePrincipal = string.Empty;
            string sContentBase64PDF = string.Empty;
            string sReferenciaConImporte = string.Empty;
            string sReferenciaConFormato = string.Empty;
            string sLRTramiteConFormato = string.Empty;

            string sFileNamePDF = string.Empty;
            string[] aRegexElementos = null;

            Warning[] warnings;
            ReportViewer rptViewer;

            ReportDataSource oRptDtsConceptos = null;
            ReportDataSource oRptDtsAtributos = null;
            ReportParameter[] aParamsReport = null;

            //string sBase64FichaPago = string.Empty;

            try {

                //Configuracion de los valores para el Reporte                     
                rptViewer = new ReportViewer();
                rptViewer.LocalReport.ReportPath = _oDataReferencia.cRutaReporte;
                rptViewer.LocalReport.DisplayName = "FichaPago - PDF";
                rptViewer.LocalReport.EnableExternalImages = true;
                rptViewer.LocalReport.Refresh();

                sBarCodePrincipal = (string.Format("{0:0.00}", _oDataReferencia.dTotal)).Replace(".", "");
                sErrorMetodo = string.Empty;

                //Formatear la referencia principal para separar con guiones medios : 999-9999-9999-9999-9999-9999-9999
                //aRegexElementos = _oConfig.sMascaraLineaRef.Split('æ');
                aRegexElementos = _oConfig.sMascaraLineaRef.Split('~');
                sReferenciaConFormato = Regex.Replace(_oDataReferencia.cRefPrincipal, aRegexElementos[0], aRegexElementos[1]);

                sTotalFichaPago = "$ " + string.Format("{0:#,##0.00}", _oDataReferencia.dTotal);

                if (string.IsNullOrEmpty(sErrorMetodo)) {

                    iSizeParam = rptViewer.LocalReport.GetParameters().Count;
                    aParamsReport = new ReportParameter[iSizeParam];
                    oRptDtsAtributos = new ReportDataSource("dtsAtributos", _listAtributos);
                    oRptDtsConceptos = new ReportDataSource("dtsConceptos", _listConceptos);

                    rptViewer.LocalReport.DataSources.Add(oRptDtsAtributos);
                    rptViewer.LocalReport.DataSources.Add(oRptDtsConceptos);


                    aParamsReport[0] = new ReportParameter("ParamRefPrincipal", sBarCodePrincipal);
                    aParamsReport[1] = new ReportParameter("ParamTotalTramite", sTotalFichaPago);

                    //Vigencia de la referencia
                    aParamsReport[2] = new ReportParameter("ParamVigenciaPago", _oDataReferencia.dtVigencia);

                    //Referencia principal sin importe
                    aParamsReport[3] = new ReportParameter("ParamRefPrincipalSinImp", sReferenciaConFormato);

                    //Seccion de Avisos
                    aParamsReport[4] = new ReportParameter("ParamAviso", _oDataReferencia.cAviso);

                    //Image Header AAFY
                    aParamsReport[5] = new ReportParameter("ParamImageAAFY", _oDataReferencia.cImageAAFY);

                    //Image Header Gobierno Yucatan
                    aParamsReport[6] = new ReportParameter("ParamImageGob", _oDataReferencia.cImageYUC);

                    //Titulo del reporte : ParamTituloReporte
                    aParamsReport[7] = new ReportParameter("ParamTituloReporte", _oDataReferencia.cTituloReporte);

                    //Folio de Solicitud
                    aParamsReport[8] = new ReportParameter("ParamFolioSolicitud", _oDataReferencia.cFolioSolicitud);

                    //Fecha de Solicitud
                    aParamsReport[9] = new ReportParameter("ParamFechaSolicitud", _oDataReferencia.dtFechaSolicitud);

                    //Nombre del tramite:
                    aParamsReport[10] = new ReportParameter("ParamNomTramite", ".");

                    //ParamAnnio , ParamFolioTramite
                    aParamsReport[11] = new ReportParameter("ParamAnnio", ".");

                    aParamsReport[12] = new ReportParameter("ParamFolioTramite", ".");

                    //Importe total en Letras
                    aParamsReport[13] = new ReportParameter("ParamTotalLetras", _oDataReferencia.cTotalLetras);

                    //Imagen de Convenios
                    aParamsReport[14] = new ReportParameter("ParamImageConvenios", _oConfig.sRutaImagenConvenios);

                    //Mensaje al ciudadano MensajeC1
                    aParamsReport[15] = new ReportParameter("ParamMensajeC1", _oConfig.sMensajeC1);

                    //Mensaje al ciudadano MensajeC1
                    aParamsReport[16] = new ReportParameter("ParamMensajeC2", _oConfig.sMensajeC2);

                    //Se agrega el codigo de barras con la linea de Referencia
                    aParamsReport[17] = new ReportParameter("ParamBarCodeLR", "file:///" + _oConfig.sRutaImagenBarCode);

                    //Fecha de impresion/generacion del documento
                    aParamsReport[18] = new ReportParameter("ParamFechaImpresion", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

                    //Se agrega el codigo de barras con la linea de Referencia de 30 digitos
                    if ( !string.IsNullOrEmpty(_oConfig.sRutaImagenBarCode30) )
                    {
                        aParamsReport[19] = new ReportParameter("ParamBarCodeLR30", "file:///" + _oConfig.sRutaImagenBarCode30);
                        aParamsReport[20] = new ReportParameter("ParamRefPrincipal30", _oDataReferencia.cRefPrincipal30);
                    }
                    else
                    {
                        aParamsReport[19] = new ReportParameter("ParamBarCodeLR30", "");
                        aParamsReport[20] = new ReportParameter("ParamRefPrincipal30", "");
                    }

                    //Parametro de concepto Global                   
                    aParamsReport[21] = new ReportParameter("ParamConceptoGlobal", _oDataReferencia.cConceptoGlobal + " " + sTotalFichaPago);

                    //Parametro para indicar si tiene atributos                    
                    aParamsReport[22] = new ReportParameter("ParamTieneAtributos", _listAtributos.Count.ToString());

                    rptViewer.LocalReport.SetParameters(aParamsReport);
                    rptViewer.LocalReport.Refresh();

                    //Generar el PDf
                    byte[] byteViewer = rptViewer.LocalReport.Render(
                        "PDF", null, out v_mimetype, out v_encoding, out v_filename_extension, out v_streamids, out warnings
                    );

                    if (_oConfig.bGuardarPDF) {
                        if (string.IsNullOrEmpty(_oConfig.sFileSystemPDF)) {
                            sFileNamePDF = Path.Combine(
                                Path.GetDirectoryName(_oDataReferencia.cRutaReporte),
                                //_oDataReferencia.cRefPrincipal + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.ffff") + "_" + ((new Random()).Next(0, 100)).ToString() + ".pdf"
                                _oDataReferencia.cRefPrincipal + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.ffff") + ".pdf"
                            );
                        }//fin:if
                        else {
                            sFileNamePDF = Path.Combine(
                                _oConfig.sFileSystemPDF,
                                //_oDataReferencia.cRefPrincipal + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.ffff") + "_" + ((new Random()).Next(0, 100)).ToString() + ".pdf"
                                _oDataReferencia.cRefPrincipal + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.ffff") + ".pdf"
                            );
                        }//fin:else                       

                        using (FileStream fs = new FileStream(sFileNamePDF, FileMode.Create)) {
                            fs.Write(byteViewer, 0, byteViewer.Length);
                            fs.Flush();
                            fs.Close();
                        }//fin:if
                    }//fin:if (_oConfig.bGuardarPDF)

                    sContentBase64PDF = Convert.ToBase64String(byteViewer);

                    _sOutError = "";
                }//fin:if (string.IsNullOrEmpty(sErrorMetodo))
                else {
                    _sOutError = sErrorMetodo;
                }//fin:else
            }//fin:try
            catch (Exception oEx) {
                _sOutError = "Error en [crearFichaPagoPDF] : ";
                _sOutError += "Message^" + oEx.Message;
                _sOutError += "~Source^" + oEx.Source;
                _sOutError += "~Target^" + oEx.TargetSite.ToString();
                _sOutError += "~StackTrace^" + oEx.StackTrace;
            }

            return sContentBase64PDF;

        }//fin:crearFichaPagoPDF
        //-----------------------------------------------------------------------------------------
    }//fin:class
}
