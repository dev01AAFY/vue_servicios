﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libGenerarPDF.clases {
    public class TDataConceptoRef {

        #region Atributos
        public int iOrden { get; set; }
        public double iCantidad { get; set; }
        public string cConcepto { get; set; }
        public double dImporteUnitario { get; set; }
        public double dtotal { get; set; }

        #endregion Atributos
        //---------------------------------------------------------------------
        public TDataConceptoRef() {
        }//fin:constructor
        //---------------------------------------------------------------------
        public void Clear() {
            this.iOrden = 0;
            this.iCantidad = 0.00;

            this.cConcepto = string.Empty;

            this.dImporteUnitario = 0.00;
            this.dtotal = 0.00;
        }//fin:Clear
        //---------------------------------------------------------------------
        public void Assign(TDataConceptoRef _Obj) {
            this.iOrden = _Obj.iOrden;
            this.iCantidad = _Obj.iCantidad;

            this.cConcepto = _Obj.cConcepto;

            this.dImporteUnitario = _Obj.dImporteUnitario;
            this.dtotal = _Obj.dtotal;
        }//fin:Assign
        //---------------------------------------------------------------------
    }//fin:class
}
