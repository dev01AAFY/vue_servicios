﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libGenerarPDF.clases {
    public class TDataAtributos {

        #region Propiedades
        public int iOrden { get; set; }
        public string cAtributo { get; set; }
        public string cValor { get; set; }

        #endregion Propiedades
        //---------------------------------------------------------------------
        public TDataAtributos() {
        }//fin:contructor
        //---------------------------------------------------------------------
        public void Clear() {
            this.iOrden = 0;
            this.cAtributo = string.Empty;
            this.cValor = string.Empty;
        }//fin:clear
        //---------------------------------------------------------------------
    }//fin:class
}
