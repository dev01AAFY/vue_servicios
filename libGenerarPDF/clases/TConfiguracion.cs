﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libGenerarPDF.clases {
    public class TConfiguracion {
        public bool bGuardarPDF { get; set; }
        public string sRutaImagenConvenios { get; set; }
        public string sMensajeC1 { get; set; }
        public string sMensajeC2 { get; set; }
        public string sFileSystemPDF { get; set; }
        public string sRutaImagenBarCode { get; set; }
        public string sRutaImagenBarCode30 { get; set; }
        public string sMascaraLineaRef { get; set; }
        //-----------------------------------------------------------------------------------------
        public TConfiguracion() {

            this.bGuardarPDF = false;
            this.sRutaImagenConvenios = string.Empty;
            this.sMensajeC1 = string.Empty;
            this.sMensajeC2 = string.Empty;
            this.sFileSystemPDF = string.Empty;
            this.sRutaImagenBarCode = string.Empty;
            this.sRutaImagenBarCode30 = string.Empty;
            this.sMascaraLineaRef = string.Empty;
        }//fin:constructor
        //-----------------------------------------------------------------------------------------
    }//fin:class
}
