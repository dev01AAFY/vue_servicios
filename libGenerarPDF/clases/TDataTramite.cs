﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libGenerarPDF.clases {
    public class TDataTramite {
        public int iOrdenTramite { get; set; }
        public int iIdTramite { get; set; }
        public int iAnioFiscal { get; set; }
        public double dTotal { get; set; }
        public string cFolioTramite { get; set; }
        public string cNombreTramite { get; set; }
        public string cLineaReferencia { get; set; }
        public string dtVigencia { get; set; }
        public double dImporteUnitTotal { get; set; }
        //---------------------------------------------------------------------
        public TDataTramite() {
            //this.sRutaImgBarCodeLR = string.Empty;
            this.iOrdenTramite = 0;
            this.iIdTramite = 0;
            this.iAnioFiscal = 0;
            this.dTotal = 0.00;
            this.dImporteUnitTotal = 0.00;
            this.cFolioTramite = string.Empty;
            this.cNombreTramite = string.Empty;
            this.cLineaReferencia = string.Empty;
            this.dtVigencia = string.Empty;
        }//fin:TDataTramite
        //---------------------------------------------------------------------
    }//fin:class
}
