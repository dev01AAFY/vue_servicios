﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libGenerarPDF.clases {
    public class TDataReporteVU {

        #region Atributos

        //public int iOrdenTramite { get; set; }
        //public int iAnioFiscal { get; set; }
        //public int iTotalCerosImp { get; set; }
        public double dTotal { get; set; }
        //public double dSubTotal { get; set; }
        //public double dImpuesto { get; set; }
        //public string cNombreTramite { get; set; }
        public string cFolioSolicitud { get; set; }
        public string dtFechaSolicitud { get; set; }
        //public string cFolioTramite { get; set; }
        public string dtVigencia { get; set; }
        public string cRefPrincipal { get; set; }
        //public string cRefSecundaria { get; set; }
        public string cAviso { get; set; }
        public string cTituloReporte { get; set; }
        public string cImageAAFY { get; set; }
        public string cImageYUC { get; set; }
        public string cRutaReporte { get; set; }
        public string cTotalLetras { get; set; }
        public string cRefPrincipal30 { get; set; }
        public string cConceptoGlobal { get; set; }

        #endregion Atributos
        //---------------------------------------------------------------------
        public TDataReporteVU() {
        }//fin:constructor
        //---------------------------------------------------------------------
        public void Clear() {
            //this.iOrdenTramite = 0;
            //this.iAnioFiscal = 0;

            this.dTotal = 0.00;
            //this.dSubTotal = 0.00;
            //this.dImpuesto = 0.00;

            //this.cNombreTramite = string.Empty;
            this.cFolioSolicitud = string.Empty;
            //this.cFolioTramite = string.Empty;
            this.dtVigencia = string.Empty;
            this.cRefPrincipal = string.Empty;
            this.cRefPrincipal30 = string.Empty;
            //this.cRefSecundaria = string.Empty;
            this.cAviso = string.Empty;
            this.cConceptoGlobal = string.Empty;

            this.cTituloReporte = string.Empty;
            this.cImageAAFY = string.Empty;
            this.cImageYUC = string.Empty;

            this.cRutaReporte = string.Empty;
            this.cTotalLetras = string.Empty;
        }//fin:Clear
        //---------------------------------------------------------------------
    }//fin:class
}
